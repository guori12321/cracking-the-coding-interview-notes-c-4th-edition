// Trick here is to be careful about the overflow when computing the middle


// Forward declaration of isBadVersion API.
bool isBadVersion(int version);

class Solution {
public:
    int firstBadVersion(int n) {
        int l = 1, r = n;
        while (l != r)
        {
            // l+r may overflow
            int m = ((long long)l+r)/2;
            if (isBadVersion(m))
                r = m;
            else
                l = m+1;
        }
        return l;
    }
};
