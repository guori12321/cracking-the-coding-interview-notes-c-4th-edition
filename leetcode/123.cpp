class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int f[100000] = {0};

        int buyDay = 0;
        // initial with 0 rather than -1, because perhaps we won't buy anything
        int income = 0;
        int n = prices.size();
        for (int i = 0; i < n; i++)
        {
            if ( prices[i] > prices[buyDay] )
                income = max(income, prices[i] - prices[buyDay] );
            else
                buyDay = i;
            f[i] = income;
        }

        int sellDay = n-1;
        income = 0;
        int ans = 0;
        for (int i = n-1; i >= 0; i--)
        {
            if ( prices[i] < prices[sellDay] )
                income = max(income, prices[sellDay] - prices[i]);
            else
                sellDay = i;
            ans = max(ans, income + f[i]);
        }
        return ans;
    }
};


// If we look for the high price from left and low from right, then we would WA because we need to buy, sell, buy and sell rather than just buy, buy and sell, sell.
class Solution {
public:
    int maxProfit(vector<int>& nums) {
        int n = nums.size();

        if (n < 2)
            return 0;

        int l1 = 99999, l2 = 99999, h1 = -99999, h2 = -99999;

        int l = 0, r = n-1;
        l1 = nums[0];
        h1 = nums[n-1];

        int ans = 0;
        while (l < r)
        {
            while (l < r && nums[l+1] < nums[l] )
            {
                l++;
                if (nums[l] < l1)
                {
                    l2 = l1;
                    l1 = nums[l];
                }
                else
                    if (nums[l] < l2)
                        l2 = nums[l];
            }

            while (l < r && nums[r-1] > nums[r])
            {
                r--;
                if (h1 < nums[r])
                {
                    h2 = h1;
                    h1 = nums[r];
                }
                else
                    if (h2 < nums[r])
                        h2 = nums[r];
            }

            int t1 = h1 - l1, t2 = h2 - l2;
            int t = 0;
            if (t1 > 0)
                t += t1;
            if (t2 > 0)
                t += t2;
            ans = max(ans, t);
            l++;
            r--;
        }

        return ans;
    }
};
