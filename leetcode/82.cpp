// You need to be very, very careful for problems like this.
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        ListNode* vh = new ListNode(-99999);
        vh->next = head;

        ListNode* t = vh;
        while (t->next != NULL)
        {
            ListNode* t2 = t->next->next;
            // Just to check if t2 is NULL
            if (t2 != NULL && t2->val == t->next->val)
            {
                while (t2 != NULL && t2->val == t->next->val)
                    t2 = t2->next;
                t->next = t2;
            }
            else
                t = t->next;
        }
        return vh->next;
    }
};
