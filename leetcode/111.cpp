// DFS
class Solution {
public:
    int ans = 999999;
    void DFS(TreeNode* root, int h)
    {
        if (root == NULL)
            return;
        if (ans < h)
            return;
        if (root->left == NULL && root->right == NULL)
        {
            ans = min(ans, h);
            return;
        }
        DFS(root->left, h+1);
        DFS(root->right, h+1);
    }
    int minDepth(TreeNode* root) {
    if (root == NULL)
        return 0;
    // If the tree has only one node, then the path from the root to the leaf is the root itself, which means the path has a length of 1
    DFS(root, 1);
    return ans;
    }
};

// BFS
class Solution {
public:
    int minDepth(TreeNode* root) {
        if (root == NULL)
            return 0;

        queue<TreeNode*> q;
        queue<int> height;
        q.push(root);
        height.push(1);
        while (!q.empty() )
        {
            TreeNode* t = q.front();
            int h = height.front();
            q.pop();
            height.pop();
            if (t == NULL)
                continue;

            if (t->left == NULL && t->right == NULL)
                return h;
            q.push(t->left);
            q.push(t->right);
            height.push(h+1);
            height.push(h+1);
        }
        return -1;
    }
};
