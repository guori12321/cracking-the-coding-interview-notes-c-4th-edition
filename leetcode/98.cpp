class Solution {
    public:
        bool check(TreeNode* root, long long l, long long r)
        {
            if (root == NULL)
                return true;
            if (root->val <= l || root->val >= r)
                return false;
            // Here is && rather than ||
            return check(root->left, l, root->val) && check(root->right, root->val, r);
        }
        bool isValidBST(TreeNode* root) {
            // The test cases contains INT_MIN and INT_MAX, so we use long long, however INT_MIN - 1 is still int so it wold be overflowed, hence we trans it to be long long first.
            return check(root, (long long)INT_MIN-1, (long long)INT_MAX+1);
        }
};
