class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        int m = obstacleGrid.size();
        if (m == 0)
            return 0;
        int n = obstacleGrid[0].size();
        if (n == 0)
            return 0;
        int f[100][100] = {0};

        // Just remember to check the initial states, because there may be obstacles in the first row and column.
        if (obstacleGrid[0][0] == 0)
            f[0][0] = 1;
        for (int i = 1; i < m; i++)
            if (obstacleGrid[i][0] == 0)
                f[i][0] = f[i-1][0];
        for (int j = 1; j < n; j++)
            if (obstacleGrid[0][j] == 0)
                f[0][j] = f[0][j-1];

        for (int i = 1; i < m; i++)
            for (int j = 1; j < n; j++)
            {
                if (obstacleGrid[i][j] == 0)
                    f[i][j] = f[i-1][j] + f[i][j-1];
            }
         return f[m-1][n-1];
    }
};
