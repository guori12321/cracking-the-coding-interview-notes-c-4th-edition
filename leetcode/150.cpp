class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> st;
        int n = tokens.size();

        for (int i = 0; i < n; i++)
        {
            if (tokens[i] == "+" || tokens[i] == "-" || tokens[i] == "*" || tokens[i] == "/")
            {
                int t1 = st.top();
                st.pop();
                int t2 = st.top();
                st.pop();
                int result = 0;
                if (tokens[i] == "+")
                    result = t2 + t1;
                else if (tokens[i] == "-")
                    result = t2 - t1;
                else if (tokens[i] == "*")
                    result = t2 * t1;
                else if (tokens[i] == "/")
                    result = t2 / t1;
                st.push(result);
            }
            else
                st.push(stoi(tokens[i]));
        }
        return st.top();
    }
};
