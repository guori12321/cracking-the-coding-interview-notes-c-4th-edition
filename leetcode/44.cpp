// Very tricky one.
// The best reference http://yucoding.blogspot.com/2013/02/leetcode-question-123-wildcard-matching.html , and other methods are http://www.cnblogs.com/yuzhangcmu/p/4116153.html

// At the beginning I tried to run a recursive program, but TLE even I tried all kinds of optimization.
// And I find the first reference that is both fast and brief.
// The key idea here is just to memorize location of the last star. Because the recursive program tries to enumerate all of them, we would certainly TLE.
// Why memory one star would work? Assume if the pattern has only one star, then it's okay if we enumerate only this one star.
// However, if we have two stars, then assume the pattern is something like A*B*C, and the string is abc where a matches parts of a and b for parts of B and c for C. If we have trouble when matching C, we can just move the cursor in s forward to find a match, and we don't care about the parts in s which is already matched.
// In other words, as the star could be the sequence of any chars and length, so long as we can match s, we don't care it is matched in the first or second star, and we only care about if the substrings in pattern is matched. If all the substring excluding stars are matched, then we can skip any string in s.
class Solution {
public:
    bool isMatch(string s, string p) {
        int ns = s.size(), np = p.size();
        int ps = 0, pp = 0;
        int ts = -1, tp = -1;
        while (ps < ns)
        {
            if (s[ps] == p[pp] | p[pp]== '?')
            {
                ps++;
                pp++;
                continue;
            }

            if (p[pp]=='*')
            {
                ts = ps;
                tp = pp;
                pp++;
                continue;
            }

            if (tp>=0)
            {
                pp = tp + 1;
                ps = ts + 1;
                ts++;
                continue;
            }

            return false;
        }

        while (pp < np && p[pp] == '*')
            pp++;
        return (pp == np);
    }
};
