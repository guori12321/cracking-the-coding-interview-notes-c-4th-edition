// The code is simple, however, the idea is not.
// The reference is Page 102 of CC
//
// In a cycle, if runner b runs as twice fast as runner a, then, after exactly one cycle, they would meet at the starting point.
// At the beginning, if b is k meters ahead of the starting point (yes he is faster and we still want to give him more privilege), then, a and b would meet k meters before the starting point. At that time, a runs (n-k) meters and is at n-k meters, and b runs 2(n-k) meters and is at k + 2(n-k) = n + (n - k), so b is ahead of one cycle as before.
// The basic idea here is to assume the number of nodes before the cycle starting point is k, and when a is going into the cycle, the b is already k nodes ahead of a. So, they would meet k nodes before the starting points. Let's assume another runner is at the head pointer and start to run and a keeps running as well , then, after c runs k meters, c and a would be at the starting point at the same time, and that will be our answer.
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        if (head == NULL || (head!= NULL && head->next == NULL))
            return NULL;

        ListNode * p1 = head->next, *p2 = head->next->next;
        while (p1 != p2 && p2 != NULL)
        {
            p1 = p1->next;
            if (p2->next == NULL)
                return NULL;
            p2 = p2->next->next;
        }

        if (p2 == NULL)
            return NULL;

        ListNode * p3 = head;
        while (p3 != p2)
        {
            p3 = p3->next;
            p2 = p2->next;
        }
        return p3;
        }
};
