// Very tricky one.
// The initial state is 0 nodes with 0 unique BST and 1 with 1.
// For 2 or more, we enumerate how many nodes on the left children and how many on the right.
// Note that there must be one root, and the remaining nodes are either in the left subtree or right subtree. So just enumerate.
class Solution {
public:
    int numTrees(int n) {
        int f[100000] = {0};
    f[0] = 1;
    f[1] = 1;
    // Note i could equal to n in this case
    for (int i = 2; i <= n; i++)
        for (int j = 0; j <= i - 1; j++)
            f[i] += f[j] * f[i-1-j];
    return f[n];
    }
};

