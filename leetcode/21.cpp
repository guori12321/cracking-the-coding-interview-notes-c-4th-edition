// Brief code
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode* vh = new ListNode(-99999);
        ListNode* t = vh;
        while (l1 != NULL && l2 != NULL)
        {
            if (l1->val < l2->val)
            {
                t->next = l1;
                l1 = l1->next;
            }
            else
            {
                t->next = l2;
                l2 = l2->next;
            }
            t = t->next;
        }
        if (l1 != NULL)
            t->next = l1;
        if (l2 != NULL)
            t->next = l2;

        return vh->next;
    }
};

// Old code
// Check if the head is NULL every time we insert new nodes
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
    if (l1 == NULL && l2 == NULL)
        return NULL;
    ListNode* head = NULL;
    ListNode* t = NULL;
    while ( l1 != NULL && l2 != NULL)
    {
        int v = 0;
        if (l1->val < l2->val)
        {
            v = l1->val;
            l1 = l1->next;
        }
        else
        {
            v = l2->val;
            l2 = l2->next;
        }
        if (t == NULL)
        {
            head = new ListNode(0);
            head->val = v;
            head->next = NULL;
            t = head;
        }
        else
        {
            t->next = new ListNode(0);
            t = t->next;
            t->val = v;
            t->next = NULL;
        }
    }
    while (l1 != NULL)
    {
        // Note if l1 != NULL but l2 == NULL
        if (head == NULL)
        {
            head = new ListNode(0);
            head->val = l1->val;
            head->next = NULL;
            t = head;
            l1 = l1->next;
        }
        else
        {
            t->next = new ListNode(0);
            t = t->next;
            t->val = l1->val;
            t->next = NULL;
            l1 = l1->next;
        }
    }
    while (l2 != NULL)
    {
        if (head == NULL)
        {
            head = new ListNode(0);
            head->val = l2->val;
            head->next = NULL;
            t = head;
            l2 = l2->next;
        }
        else
        {
            t->next = new ListNode(0);
            t = t->next;
            t->val = l2->val;
            t->next = NULL;
            l2 = l2->next;
        }
    }
    return head;
    }
};

// Or we can put the addNode() in a function so that we don't need to check if the head is NULL every time.
class Solution {
public:
    ListNode* head = NULL;
    ListNode* addNode(ListNode* t, int val)
    {
        if (head == NULL)
        {
            head = new ListNode(0);
            head->val = val;
            head->next = NULL;
            return head;
        }

        t->next = new ListNode(0);
        t = t->next;
        t->val = val;
        t->next = NULL;
        return t;
    }

    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        if (l1 == NULL && l2 == NULL)
            return NULL;
        ListNode* t = NULL;
        while ( l1 != NULL && l2 != NULL)
        {
            int v = 0;
            if (l1->val < l2->val)
            {
                t = addNode(t, l1->val);
                l1 = l1->next;
            }
            else
            {
                t = addNode(t, l2->val);
                l2 = l2->next;
            }
        }
        while (l1 != NULL)
        {
                t = addNode(t, l1->val);
                l1 = l1->next;
        }
        while (l2 != NULL)
        {
                t = addNode(t, l2->val);
                l2 = l2->next;
        }
        return head;
    }
};
