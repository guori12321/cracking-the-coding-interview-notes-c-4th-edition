// The formula is very difficult to understand.
// Reference: http://www.cnblogs.com/grandyang/p/4629032.html
// http://constellationmay.blogspot.com/2015/07/leetcodenumber-of-digit-one.html
// http://blog.csdn.net/xudli/article/details/46798619
class Solution {
public:
    int countDigitOne(int n) {
        int ans = 0;
        for (long long k = 1; k <= n; k *= 10)
        {
            long long a = n/k;
            long long b = n%k;

            if (a  >= 2)
                ans += (a+8)/10 * k;
            if (a % 10 == 1)
                ans += b+1;
        }
        return ans;
    }
};
