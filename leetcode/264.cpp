// Convert this problem to merge sort
// Reference: http://www.cnblogs.com/grandyang/p/4743837.html
class Solution {
public:
    int nthUglyNumber(int n) {
        if (n == 0)
            return 0;
        vector<int> ans(1, 1);
        int k2 = 0, k3 = 0, k5 = 0;
        for (int i = 1; i < n; i++)
        {
            int m2 = ans[k2] * 2;
            int m3 = ans[k3] * 3;
            int m5 = ans[k5] * 5;
            int minm = min(m2, min(m3, m5) );

            // Here we don't use if else, because the ugly numbers may be duplicated, for instance 6 can be both 2*3 and 3*2, so we need to skip the same number here.
            if (minm == m2)
                k2++;
            if (minm == m3)
                k3++;
            if (minm == m5)
                k5++;
            ans.push_back(minm);
        }
        return ans[n-1];
    }
};
