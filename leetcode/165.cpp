class Solution {
public:
    int compareVersion(string version1, string version2) {
        int n1 = version1.size(), n2 = version2.size();

        int l1 = 0, l2 = 0;
        while (true)
        {
            int t1 = 0, t2 = 0;
            if (l1 == n1 && l2 == n2)
                return 0;
            // In case of 0 and 0.0.0, we think they are the same
            /*
               if (l1 == n1)
               return 1;
               if (l2 == n2)
               return -1;
               */

            while (l1 < n1 && version1[l1] != '.')
            {
                t1 = t1*10 + (version1[l1] - '0');
                l1++;
            }
            // Skip '.'
            if (l1 < n1)
                l1++;
            while (l2 < n2 && version2[l2] != '.')
            {
                t2 = t2*10 + (version2[l2] -'0');
                l2++;
            }
            if (l2 < n2)
                l2++;

            if (t1 > t2)
                return 1;
            if (t1 < t2)
                return -1;
        }
        return 0;
    }
};
