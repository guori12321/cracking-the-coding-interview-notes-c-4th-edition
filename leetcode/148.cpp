class Solution {
public:
    ListNode* mergeSort(ListNode* head, ListNode* tail)
    {
        if (head == NULL || head == tail)
            return NULL;
        if (head->next == tail)
        {
            head->next = NULL;
            return head;
        }

        int n = 0;
        ListNode* t = head;
        while (t != tail)
        {
            n++;
            t = t->next;
        }
        t = head;
        for (int i = 0; i < n/2; i++)
            t = t->next;

        ListNode* a = mergeSort(head, t);
        ListNode* b = mergeSort(t, tail);
        if (a == NULL)
            return b;
        if (b == NULL)
            return a;

        ListNode *hans = NULL, *ans = NULL;
        if (a->val < b->val)
        {
ans = a;
            a = a->next;
        }
        else
        {
            ans = b;
            b = b->next;
        }
        hans = ans;

        while (a != NULL && b != NULL)
        {
            if (a->val < b->val)
            {
                ans->next = a;
                a = a->next;
                ans = ans->next;
            }
            else
            {
                ans->next = b;
                b = b->next;
                ans = ans->next;
            }
        }
        while (a != NULL)
        {
            ans->next = a;
            a = a->next;
            ans = ans->next;
        }
        while (b!= NULL)
        {
            ans->next = b;
            b = b->next;
            ans = ans->next;
        }
        ans->next = NULL;

        // We need to return the hans rather than ans!
        return hans;
    }
    ListNode* sortList(ListNode* head) {
        return mergeSort(head, NULL);
    }
};
