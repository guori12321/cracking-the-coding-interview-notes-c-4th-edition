// This algorithm can be extended to n/k.
// In case of n/k, there may be at most k-1 answer integers.
// If there are k-1 answer, as there would be at most n/k other numbers, all the other numbers cannot reduce the count of the answer numbers to less than 1. So this algorithm works.
// And if there are less than k-1 answer, (I don't know how to proof this...)
//
// Anyway this algorithm works for this example.
// Reference: http://www.geeksforgeeks.org/given-an-array-of-of-size-n-finds-all-the-elements-that-appear-more-than-nk-times/
// No proof is given in the reference.
class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        int c1 = 0, c2 = 0;
        int d1 = 0, d2 = 0;

        for (int i = 0; i < nums.size(); i++)
        {
            if (c1 == 0)
            {
                c1 = 1;
                d1 = nums[i];
            }
            else if (c1 > 0 && d1 == nums[i])
            {
                c1++;
            }
            else if (c2 == 0)
            {
                c2++;
                d2 = nums[i];
            }
            else if (c2 > 0 && d2 == nums[i])
                c2++;
            else
            {
                c1--;
                c2--;
            }
        }
        int t1 = 0, t2 = 0;
        if (c1 > 0)
            for (int i = 0; i < nums.size(); i++)
                if (nums[i] == d1)
                    t1++;
        if (c2 > 0)
            for (int i = 0; i < nums.size(); i++)
                if (nums[i] == d2)
                    t2++;
        vector<int> ans;
        int n = nums.size();
        if (t1 > n/3)
            ans.push_back(d1);
        if (t2 > n/3)
            ans.push_back(d2);
        return ans;
    }
};
