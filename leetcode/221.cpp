// This is a traditional DP question. But still need to be careful when familiar with such a problem.
class Solution {
public:
    // The input is char matrix rather than int or bool matrix
    int maximalSquare(vector<vector<char> >& matrix) {
        int m = matrix.size();
        if (m == 0)
            return 0;
        int n = matrix[0].size();
        if (n == 0)
            return 0;

        vector<vector<int> > f(m, vector<int>(n, 0) );
        int ans = 0;

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (matrix[i][j] == '1')
                {
                    if (i >= 1 && j >= 1)
                        // min() can only take two parameters.
                        f[i][j] = min(f[i-1][j-1], min(f[i-1][j], f[i][j-1]) ) + 1;
                    else
                        f[i][j] = 1;
                    // f[m-1][n-1] is NOT the answer.
                    ans = max(ans, f[i][j]);

                }
        // ans is the length of edge of the matrix. The area would be ans square.
        return ans*ans;

    }

};
