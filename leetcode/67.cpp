class Solution {
public:
    string addBinary(string a, string b) {

        if (a.size() < b.size() )
        {
            string t = a;
            a = b;
            b = t;
        }

        int la = a.size();
        int lb = b.size();
        int carry = 0;
        string ans = "";

        for (int i = lb-1; i >= 0; i--)
        {
            // We cannot add int into string
            ans = to_string( ( (a[ la-lb + i]-'0') + (b[i]-'0') +carry ) % 2) + ans;
            // We need to convert char to int, otherwise they would be the ascii code
            carry = ( (a[ la-lb + i]-'0') + (b[i]-'0') +carry ) / 2;
        }

        for (int i = la-lb-1; i >= 0; i--)
        {
            ans = to_string( ( (a[i]-'0') + carry) % 2 ) + ans;
            carry = ( (a[i]-'0') + carry) / 2;
        }
        if (carry == 1)
            ans = "1" + ans;
        return ans;
        }
};
