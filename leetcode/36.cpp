class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        for (int i = 0; i < 9; i++)
        {
            bool hash[10] = {0};
            for (int j = 0; j < 9; j++)
                if (board[i][j] != '.')
                    if (hash[board[i][j] - '0'] == false)
                        hash[ board[i][j] - '0' ] = true;
                    else
                        return false;
        }

        for (int i = 0; i < 9; i++)
        {
            bool hash[10] = {0};
            for (int j = 0; j < 9; j++)
                if (board[j][i] != '.')
                    if (hash[board[j][i] - '0'] == false)
                        hash[ board[j][i] - '0' ] = true;
                    else
                        return false;
        }

        for (int i = 0; i <= 6; i += 3)
            for (int j = 0; j <= 6; j +=3)
            {
                bool hash[10] = {0};
                for (int ii = i; ii < i + 3; ii++)
                    for (int jj = j; jj < j + 3; jj++)
                        if (board[ii][jj] != '.')
                            if (hash[ board[ii][jj] - '0' ] == false)
                                hash[ board[ii][jj] - '0' ] = true;
                            else return false;
            }
        return true;
    }
};
