// The basic idea is to use a stack to store numbers and + or - operations, but for * and / we do it as soon as possible.
// For the - , we push the opposite number of the current int and + to the stack, because - requires the computing order to be from left to right.
class Solution {
public:
    int calculate(string s) {
        int n = s.size();
        stack<string> st;
        long long t = 0;
        bool flag = true;

        for (int i = 0; i < n; i++)
        {
            if (s[i] == ' ')
                continue;

            if (s[i] >= '0' && s[i] <= '9')
            {
                t = t*10 + (s[i]-'0');
                continue;
            }

            if (s[i] == '+' || s[i] == '-')
            {
                if (flag)
                    st.push(to_string(t) );
                else
                    st.push(to_string(0-t) );

                t = 0;
                if (s[i] == '-')
                    flag = false;
                else
                    flag = true;
                st.push( "+" );

                continue;
            }

            if (s[i] == '*' || s[i] == '/')
            {
                int t2 = 0;
                int j = i+1;
                while (j < n && (s[j] == ' ' || ( s[j] >= '0' && s[j] <= '9')) )
                {
                    // There may be space after the * or /, just skip them
                    if (s[j] == ' ')
                    {
                        j++;
                        continue;
                    }
                    t2 = t2 * 10 + (s[j] - '0');
                    j++;
                }

                if (s[i] == '*')
                    t = t*t2;
                else
                    t = t/t2;
                // Deal with -
                if (!flag)
                    t = -t;
                // Here as i would be increased in the for loop, we set i to j-1 because s[i] is NOT number.
                i = j-1;

                //And as here we replace the t with t*t2 or t/t2, we don't push the result because we would push it later when we get + or -.
            }
        }

        // Deal with the last t, or the formula may only contain one number
        if (flag)
            st.push( to_string(t) );
        else
            st.push( to_string(0-t) );

        while (st.size() > 1)
        {
            int t2 = stoi(st.top());
            st.pop();
            string op = st.top();
            st.pop();
            int t = stoi(st.top());
            st.pop();

            int r = 0;
            if (op == "+")
                r = t + t2;
            else if (op == "-")
                r = t - t2;
            // We've dealt with all the * and /
            /* else if (op == "*")
               r = t * t2;
               else r = t / t2; */
            st.push(to_string(r));
        }

        if (st.empty())
            return t;
        else return stoi(st.top());
    }
};
