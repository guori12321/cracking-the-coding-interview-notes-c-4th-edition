// Such a problem involves map and list (we cannot visit the middle elements in the queue).
class LRUCache{
public:
    int n;
    map<int, list<pair<int, int> >::iterator> m;
    list<pair<int, int> > l;

    LRUCache(int capacity) {
        n = capacity;
    }

    int get(int key) {
        if (m.find(key) != m.end())
        {
            pair<int, int> t = *(m[key]);
            l.erase(m[key]);
            l.push_front(make_pair(t.first, t.second) );
            // Use begin() here rather than front() here.
            // l.front() is the element at front, l.begin() is the iterator.
            m[key] = l.begin();
            return (*m[key]).second;
        }
        return -1;
    }

    void set(int key, int value) {
        if (m.find(key) != m.end())
        {
            l.erase(m[key]);
            l.push_front(make_pair(key, value));
            m[key] = l.begin();
        }
        else
        {
            if (l.size() >= n)
            {
                pair<int, int> t = l.back();
                m.erase(t.first);
                l.pop_back();
            }

            l.push_front( make_pair(key, value) );
            m[key] = l.begin();
        }
        return;
    }
};
