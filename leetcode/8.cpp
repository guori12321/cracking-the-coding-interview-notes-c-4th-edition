class Solution {
public:
    int myAtoi(string str) {
        int n = str.size();
        long long ans = 0;
        // Use int rather than boolean can make the code more brief
        int positive = 1;

        int i = 0;
        // Skip the space at the beginning of the string
        while (i < n && str[i] == ' ')
            i++;

        // Skip the optional flag. Note there may be +.
        if (str[i] == '-')
        {
            positive = -1;
            i++;
        }
        else
            if (str[i] == '+')
            {
                positive = 1;
                i++;
            }

        while (i < n)
        {
            if (str[i] >= '0' && str[i] <= '9')
            {
                ans = ans*10 + (str[i] - '0');
                // Don't compare ans with INT_MAX directly, because abs(INT_MAX) != abs(INT_MIN)
                if (ans*positive > INT_MAX)
                        return INT_MAX;
                if (ans*positive < INT_MIN)
                        return INT_MIN;
            }
            else
                return ans * positive;
            i++;
        }
        return ans * positive;
    }
};
