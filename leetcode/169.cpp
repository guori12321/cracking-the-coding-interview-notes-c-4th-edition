// Very similar solution to No. 137
// Be careful about the variable naming: I made mistakes when I used i and j, so later I use the bitNum and numNum instead.
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        unsigned int ans = 0;
        int n = nums.size();
        for (int bitNum = 0; bitNum < sizeof(int) * 8; bitNum++)
        {
            int count = 0;
            for (int numNum = 0; numNum < n; numNum++)
                if ( (nums[numNum] >> bitNum & 1) == 1)
                    count++;
        if (count > n/2)
            ans |= 1 << bitNum;
        }

        return ans;
    }
};
