// We have to allocate a new vector rather than delete the duplicated items in the old one, because the erase() in vector takes a long time.
class Solution {
public:
    static bool mySort(Interval a, Interval b)
    {
        if (a.start < b.start)
            return true;
        if (a.start > b.start)
            return false;
        if (a.end < b.end)
            return true;
        return false;
    }
    vector<Interval> merge(vector<Interval>& intervals) {
        if (intervals.size() == 0)
            return intervals;
        sort(intervals.begin(), intervals.end(), mySort);

        int n = intervals.size();
        vector<Interval> ans;
        ans.push_back(intervals[0]);
        int last = 0;
        for (int i = 1; i < n; i++)
        {
            if (ans[last].end < intervals[i].start)
            {
                last++;
                ans.push_back(intervals[i]);
            }
            else
                if (ans[last].end < intervals[i].end)
                    ans[last].end = intervals[i].end;
        }
        return ans;
    }
};
