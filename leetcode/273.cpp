// So many edge cases.
class Solution {
public:
    map<long long, string> m;
    Solution()
    {
        //m[0] = "Zero";
        m[1] = "One";
        m[2] = "Two";
        m[3] = "Three";
        m[4] = "Four";
        m[5] = "Five";
        m[6] = "Six";
        m[7] = "Seven";
        m[8] = "Eight";
        m[9] = "Nine";
        m[10] = "Ten";
        m[11] = "Eleven";
        m[12] = "Twelve";
        m[13] = "Thirteen";
        m[14] = "Fourteen";
        m[15] = "Fifteen";
        m[16] = "Sixteen";
        m[17] = "Seventeen";
        m[18] = "Eighteen";
        m[19] = "Nineteen";
        m[20] = "Twenty";
        m[30] = "Thirty";
        m[40] = "Forty";
        m[50] = "Fifty";
        m[60] = "Sixty";
        m[70] = "Seventy";
        m[80] = "Eighty";
        m[90] = "Ninety";
        m[100] = "Hundred";
        m[1000] = "Thousand";
        m[1000000] = "Million";
        m[1000000000] = "Billion";
        m[1000000000000] = "Trillion";
        m[1000000000000000] = "Quadrillion";
        m[1000000000000000000] = "Quintillion";
    }
    string lessThanThousand(int n)
    {
        string ans = "";
        if (n == 0)
            return ans;
        if (n / 100 != 0)
            ans += m[n/100] + " " + m[100] + " ";

        // 100 should return "one hundred" rather than "one hundred and zero"
        if (n%100 == 0)
            return ans;

        if (m.find(n % 100) != m.end())
            ans += m[ n% 100] + " ";
        else
        {
            ans += m[ n % 100 /10 * 10] + " ";
            ans += m[ n % 10] + " ";
        }
        return ans;
    }
    string numberToWords(int num) {
        // Zero would show up only when num is 0
        if (num == 0)
            return "Zero";

        string ans = "";
        long long base = 1;
        while (base <= num)
        {
            string t =  lessThanThousand(num / base % 1000);
            // If t is zero, then we should not add zero.
            if (base > 1 && t.size() > 0)
                t += m[base] + " ";
            ans = t + ans;
            base *= 1000;
        }
        return ans.substr(0, ans.size() - 1);
    }
};
