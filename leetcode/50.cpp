// Watch out the precision in extreme cases
class Solution {
public:
    double myPow(double x, int n) {
        if (n == 0)
            return 1.0;

        bool negative = false;
        // Because n can be INT_MIN, and then, -n would be INT_MAX +1, that would lead to overflow error. So we need a long long to do this.
        long long nn = n;
        // Yes, the n might be negative.
        if (n < 0)
        {
            nn = -n;
            negative = true;
        }

        double hash[1000];

        int i = 0;
        hash[0] = x;
        // As we use p*2 later, p need to be unsigned
        long long p = 1;

        while (p * 2 <= nn)
        {
            x = x * x;
            i++;
            p *= 2;
            hash[i] = x;
        }
        nn -= p;
        for (int i = 0; i < 32; i++)
            {
                if ( (nn >> i & 1) == 1)
                    x *= hash[i];
            }
            if (negative)
                x = 1 / x;
            return x;
        }
};
