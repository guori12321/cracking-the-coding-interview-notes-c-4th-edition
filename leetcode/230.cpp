// Always comes with a simple method first, because the interview is only 45 minutes!
// As the inorder traverse would give an increasing order of all the numbers in the BST, so we do it.
//
// If we can modify the tree, for a better time complexity, we can record the number of nodes in the left subtree (i.e. the number of numbers smaller than the current node). One problem is that for the nodes in the right subtree of the root, the number of nodes whose value is smaller than the current node is the number of the nodes in the left subtree plus the number of nodes in its ancestor and ancestor's left subtree.
// However, there is one hint that I missed: assume the number of nodes in the left subtree of the root is X, then, if K < X-1, we go to the left subtree, if K = X-1, then root is our answer. Else, we go to the right subtree and look for (K-X) and set the right child of root as the new root.
// Thus we don't record the number of ancestor in the tree.
// And the insert and delete modification of the tree would be O(height of tree) rather than O(number of nodes in the tree), because we can just update the number of nodes in the left subtree from the leaf to the root and don't need to check the right subtree of the root.
//
// So, just talk every possible idea with the interviewer.
class Solution {
public:
    int num = 0;
    int ans = -99999;
    void inorderSearch(TreeNode* root, int k)
    {
        if (root == NULL || ans != -99999)
            return;
        inorderSearch(root->left, k);
        num++;

        if (num == k)
        {
            ans = root->val;
            return;
        }
        inorderSearch(root->right, k);
        return;
    }

    int kthSmallest(TreeNode* root, int k) {
        inorderSearch(root, k);
        return ans;
    }
};
