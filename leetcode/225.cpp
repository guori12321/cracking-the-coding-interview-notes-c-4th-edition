// Note that queue use front() rather than top()
class Stack {
public:
    queue<int> q;
    // Push element x onto stack.
    void push(int x) {
        q.push(x);
        return;
    }

    // Removes the element on top of the stack.
    void pop() {
        queue<int> emptyQ;
        while (q.size() > 1)
        {
            emptyQ.push(q.front());
            q.pop();
        }
        q = emptyQ;
        return;
    }

    // Get the top element.
    int top() {
        queue<int> emptyQ;
        while (q.size() > 1)
        {
            emptyQ.push(q.front());
            q.pop();
        }
        int ans = q.front();
        emptyQ.push(q.front());
        q = emptyQ;
        return ans;
    }

    // Return whether the stack is empty.
    bool empty() {
        if (q.size() == 0)
            return true;
        return false;
    }
};

