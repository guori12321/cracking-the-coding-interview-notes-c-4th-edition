class Solution {
public:
    bool visited[10000] = {};
    vector<vector<int>> ans;
    vector<int> s;

    void DFS(vector<int> & nums, int h)
    {
        if (h == nums.size())
        {
            ans.push_back(s);
            return;
        }
        for (int i = 0; i < nums.size(); i++)
        {
            if (! visited[i])
            {
                visited[i] = true;
                // s[h], rather than s[i]
                s[h] = nums[i];
                DFS(nums, h+1);
                visited[i] = false;
            }
        }
        return;
    }

    vector<vector<int>> permute(vector<int>& nums) {
        for (int i = 0; i < nums.size(); i++)
        s.push_back( nums[i]);
        DFS( nums, 0);
        return ans;
    }
};
