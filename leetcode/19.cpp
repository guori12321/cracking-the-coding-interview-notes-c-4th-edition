class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
    if (head == NULL)
        return NULL;
        ListNode* vh = new ListNode(-99999);
        vh->next = head;

        ListNode* p1 = vh, *p2 = vh;
        for (int i = 0; i < n; i++)
            p1 = p1->next;
        while (p1->next != NULL)
        {
            p1 = p1->next;
            p2 = p2->next;
        }
        p2->next = p2->next->next;
        return vh->next;
    }
};
