class Solution {
public:
    int countPrimes(int n) {
	vector<bool> f(n, 1);
	int ans = 0;

	for (int i = 2; i < n; i++)
	{
		if (f[i])
		{
			ans++;
            // j can be as large as n/i, because n/i is already a floor.
            // The initial value for j can be i, as described in the hint, because if j < i then j * i is already conducted in the previous steps.
			for (int j = i; j <= n/i; j++)
				f[i*j] = false;
		}
	}
	return ans;
    }
};
