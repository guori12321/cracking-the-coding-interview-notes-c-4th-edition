/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        ListNode * vh = new ListNode(-99999);
        vh->next = head;

        ListNode* p1 = vh;
        ListNode* p2 = NULL;
        while (p1 != NULL && p1->next != NULL)
        {
            while (p1->next != NULL && p1->next->val < x)
                p1 = p1->next;
            if (p1->next == NULL)
                return vh->next;

            if (p2 == NULL)
                p2 = p1->next;
            while (p2->next != NULL && p2->next->val >= x)
                p2 = p2->next;
            if (p2->next == NULL)
                return vh->next;
            ListNode * t2 = p2->next;
            p2->next = p2->next->next;
            ListNode * t1 = p1->next;
            p1->next = t2;
            t2->next = t1;
            p1 = p1->next;
        }

        return vh->next;
    }
};
