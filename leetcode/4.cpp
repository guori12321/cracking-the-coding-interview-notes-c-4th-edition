// The question find the median is normalized to find the Kth largest number in the two arrays, so we reduce k by at most k/2, and the algorithm will be O(log(n)).

class Solution {
public:
    double findKth(vector<int>& nums1, vector<int>& nums2, int l1, int l2, int k)
    {
        int n1 = nums1.size();
        int n2 = nums2.size();

        // We should check if the boundary is reached before we check if k is 1. Otherwise we would go beyond the boundary.
        if (l1 == n1)
            return nums2[l2+k-1];
        if (l2 == n2)
            return nums1[l1+k-1];

        if (k == 1)
            return min(nums1[l1], nums2[l2]);

        // Just to keep inside the array boundary. Note that d start from 1 rather than 0 just as k does.
        int d = min( min(n1-l1, n2-l2), k/2);

        // So here as d start from 1 we need to minus 1 in the index
        // And this equal just to save time in the worst case.
        if (nums1[l1+d-1] == nums2[l2+d-1])
            if (n1-l1 < n2-l2)
                return findKth(nums1, nums2, l1+d, l2, k-d);
            else
                return findKth(nums1, nums2, l1, l2+d, k-d);

        if (nums1[l1+d-1] < nums2[l2+d-1])
            return findKth(nums1, nums2, l1+d, l2, k-d);
        else
            return findKth(nums1, nums2, l1, l2+d, k-d);

        return -9999999;
    }
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int m = nums1.size();
        int n = nums2.size();
        return (findKth(nums1, nums2, 0, 0, (m+n+1) / 2) + findKth(nums1, nums2, 0, 0, (m+n) / 2 + 1) ) /2.0;
    }
};
