// My first idea is if a greedy algorithm works? The answer is NO, because the rightest node may be on the right subtree or the left one, and until we search the entire tree, we cannot make sure which one is the rightest.
//
// So, DFS or BFS? DFS will be more brief.
class Solution {
public:
    vector<int> ans;
void DFS(TreeNode* root, int h)
{
    if (root == NULL)
        return;
    // Here, as we assume the height of the root is 0, so, here is ans.size() <= h rather than <
    if (ans.size() <= h)
        ans.push_back(root->val);
    DFS(root->right, h+1);
    DFS(root->left, h+1);
    return;
}
    vector<int> rightSideView(TreeNode* root) {
        DFS(root, 0);
    return ans;
    }
};
