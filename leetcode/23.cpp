/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
    ListNode* merge(ListNode* l1, ListNode* l2)
    {
        if (l1 == NULL)
            return l2;
        if (l2 == NULL)
            return l1;

        ListNode* h = new ListNode(-99999);
        ListNode* p = h;
        while (l1 != NULL && l2 != NULL)
        {
            if (l1->val <= l2->val)
            {
                p->next = l1;
                p = p->next;
                l1 = l1->next;
            }
            else
            {
                p->next = l2;
                p = p->next;
                l2 = l2->next;
            }
        }
        while (l1 != NULL)
        {
            p->next = l1;
            p = p->next;
            l1 = l1->next;
        }
        while (l2 != NULL)
        {
            p->next = l2;
            p = p->next;
            l2 = l2->next;
        }
        return h->next;
    }

    ListNode* mergeKLists(vector<ListNode*>& lists) {
        int n = lists.size();
        // Be careful about the empty input!
        if (n == 0)
            return NULL;
        if (n == 1)
            return lists[0];

        vector<ListNode*> vt;
        for (int i = 0; i < n; i+=2)
        {
            if (i+1 < n)
                vt.push_back(merge(lists[i], lists[i+1]));
            else
                vt.push_back(lists[i]);
        }
        return mergeKLists(vt);
    }
};
