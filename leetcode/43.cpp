// A basic high precision algorithm, though the number is shown in string so need to be careful.
// The string times string function is divided into a string add string and a char times string function.
class Solution {
public:
    string add(string s1, string s2)
    {
        if (s1.size() < s2.size())
        {
            string t = s1;
            s1 = s2;
            s2 = t;
        }
        int n1 = s1.size(), n2 = s2.size();

        int carry = 0;
        for (int i = s2.size() - 1; i >= 0; i--)
        {
            int t = (s1[n1-n2+i]-'0') + (s2[i]-'0') + carry;
            carry = t / 10;
            s1[n1-n2+i] = t%10 +'0';
        }
        for (int i = n1-n2-1; i >= 0; i--)
        {
            int t = (s1[i]-'0') + carry;
            carry = t/10;
            s1[i] = t%10+'0';
        }

        if (carry > 0)
        s1 = "1" + s1;
            return s1;
    }

    string charTimeS(char c, string s)
    {
        if (c == '0')
            return "0";
        int carry = 0;
        for (int i = s.size()-1; i >= 0; i--)
        {
            int t = (s[i]-'0')*(c-'0') + carry;;
            s[i] = '0' + t%10;
            carry = t/10;
        }
        if (carry > 0)
                s = to_string(carry) + s;
        return s;
    }

    string multiply(string num1, string num2) {
        if (num1 == "0" || num2 == "0")
            return "0";
        if (num1.size() > num2.size())
        {
            string t = num1;
            num1 = num2;
            num2 = t;
        }

        string ans = "0";
        int n1 = num1.size(), n2 = num2.size();

        string suffix = "";
        for (int i = n1-1; i >= 0; i--)
        {
            ans = add(ans, charTimeS(num1[i], num2)+suffix );
            suffix += "0";
        }

        return ans;
    }

};
