class Solution {
public:
    int minDistance(string word1, string word2) {
        int f[100000] = {0};
        int m = word1.size(), n = word2.size();

        // If using two dimensions, the boundary is f[0][j] = j, f[i][0] = i
        for (int i = 0; i <= n; i++)
            f[i] = i;
        for (int i = 1; i <= m; i++)
        {
            // Put last inside the for loop
            // And the initial value of f[0], or f[i][0] is i rather than 0.
            int last = f[0];
            f[0] = i;
            for (int j = 1; j <= n; j++)
            {
                int tlast = f[j];
                f[j] = min(f[j], min(f[j-1], last) ) + 1;
                // NOTE!!! As i is the length, so, here we compare word1[i-1] and word2[j-1]
                if (word1[i-1] == word2[j-1])
                    f[j] = min(f[j], last);
                last = tlast;
            }
        }

        return f[n];
    }
};
