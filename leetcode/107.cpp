// Don't be afraid of the reverse result. Just do it in the simple way.

// BFS
class Solution {
public:
    vector< vector<int> > ans;
    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        queue<TreeNode*> q;
        queue<int> level;
        q.push(root);
        level.push(0);
        while (! q.empty() )
        {
            TreeNode* t = q.front();
            q.pop();
            int l = level.front();
            level.pop();
    if (t == NULL)
                continue;
            if (ans.size() < l + 1)
            {
                vector<int> vt;
                ans.push_back(vt);
            }
            ans[l].push_back(t->val);

            q.push(t->left);
            q.push(t->right);
            level.push(l + 1);
            level.push(l + 1);
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
};

//DFS
//class Solution {
public:
    vector<vector<int>> ans;

    void DFS(TreeNode* root, int h)
    {
        if (root == NULL)
            return;
        if (ans.size() < h + 1)
        {
            vector<int> vt;
            ans.push_back(vt);
        }
        ans[h].push_back(root->val);
        DFS(root->left, h+1);
        DFS(root->right, h+1);
        return;
    }

    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        DFS(root, 0);
        // Remember to reverse
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
