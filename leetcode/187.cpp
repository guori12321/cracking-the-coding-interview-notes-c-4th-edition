// My first thought is to use hash table. However this would certainly got Memory Limit Error.
// Is there a better way rather than direct hash map? Noticed that the alphabets are just "ACGT", a table of 4 chars which is much smaller than the general strings. And I thought to hash the string to int because the repeated patterns are just as large as 10 chars. However as I always use the map as hash table, I thought it was already hashed when storing the key and values. But the map is not hash table! So, we manually hash the string to int (encode), and then hash back (decode).
//
// Reference: http://www.programcreek.com/2014/03/leetcode-repeated-dna-sequences-java/
// Another point is:  << has a lower priority than +, that means + would be conducted first
class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        unordered_map<int, short> m;
        int n = s.size();
        unordered_map<char, int> hash;
        hash['A'] = 0;
        hash['C'] = 1;
        hash['G'] = 2;
        hash['T'] = 3;
        // Here i can be n-10
        for (int i = 0; i <= n-10; i++)
        {
            string ts = s.substr(i, 10);
            int ti = 0;

            for (int j = 0; j < 10; j++)
            // << has a lower priority than +, that means + would be conducted first
                ti = (ti << 2) + hash[ ts[j] ];
            if (m.find(ti) == m.end())
                m[ti] = 1;
            else if (m[ti] == 1)
                m[ti]++;
        }
        vector<string> ans;
        char remap[4] = {'A', 'C', 'G', 'T'};
        for (unordered_map<int, short>::iterator it = m.begin(); it != m.end(); it++)
        {
            if (it->second > 1)
            {
                string ts = "";
                int ti = it->first;
                for (int i = 0; i < 10; i++)
                {
                    // 2 is 0b10, 3 is 0b11, here we just use 0b
                    ts = remap[ti & 0b11] + ts;
                    ti = ti >> 2;
                }
                ans.push_back(ts);
            }
        }
        return ans;
    }
};
