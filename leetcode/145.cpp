// Recursively:
class Solution {
public:
    vector<int> ans;
    void DFS(TreeNode* root)
    {
        if (root == NULL)
            return;
        DFS(root->left);
        DFS(root->right);
        ans.push_back(root->val);
        return;
    }
    vector<int> postorderTraversal(TreeNode* root) {
        DFS(root);
    return ans;
    }
};

// Iteratively:
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> ans;
    stack<TreeNode*> s;
    s.push(root);
    while (! s.empty() )
    {
        TreeNode *t = s.top();
        s.pop();
        // Don't put if (t== NULL || t->val == -99999), because the compiler may check the t->val first
        if (t == NULL || (t != NULL && t->val == -99999) )
            continue;
        if ( (t->left == NULL || (t->left != NULL && t->left->val == -99999) )  && (t->right == NULL || (t->right != NULL && t->right->val == -99999)) )
        {
            ans.push_back(t->val);
            t->val = -99999;
            continue;
        }
        s.push(t);
        s.push(t->right);
        s.push(t->left);
    }
    return ans;
    }
};
