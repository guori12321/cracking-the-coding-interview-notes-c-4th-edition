// Use a extra head pointer would help a lot.
// Draw it on a paper, and then everything will be very clear
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        ListNode* h = new ListNode(-999);
        h->next = head;

        ListNode* t = h;
        while (t != NULL && t->next != NULL && t->next->next != NULL)
        {
            ListNode * first = t->next;
            ListNode * second = t->next->next;
            ListNode * third = t->next->next->next;
            t->next =  second;
            second->next = first;
            first->next = third;
            t = first;
        }

        return h->next;
    }
};
