class Solution {
public:
    TreeNode* DFS(vector<int>& preorder, vector<int>& inorder, int pl, int pr, int il, int ir)
    {
        if (pl > pr)
            return NULL;
        // Here the first node in preorder is preorder[pl] rather than preorder[0]
        TreeNode* root = new TreeNode( preorder[pl]);
        int m = 0;
        for (m = il; m <= ir; m++)
        {
            if (inorder[m] == preorder[pl])
                break;
        }
        root->left = DFS(preorder, inorder, pl+1, pl+1 + m-1-il , il, m-1);
        root->right = DFS(preorder, inorder, pr - (ir- (m+1)), pr, m+1, ir);
        return root;
    }

    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        return DFS(preorder, inorder, 0, preorder.size() - 1, 0, preorder.size() - 1);
    }
};
