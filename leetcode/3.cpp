class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int n = s.size();
        int l = 0, r = 0;
        int ans = 0;
        int hash[256] = {0};
        for (int i = 0; i < 256; i++)
            hash[i] = -1; // -1 means this char never shows up

        while (r < n)
        {
            //Scan the char at s[r] and record its position if it doesn't show at the right side of l
        	while (r < n && hash[ s[r] ] < l)
        	{
        		hash[ s[r] ] = r;
        		r++;
        	}

        	ans = max(ans, r-l);

        	if (r < n)
        	{
        	    l = hash[s[r]]+1;
        	    hash[ s[r] ] = r;
        	    r++; // Remember to move the pointer!
        	}
        }
        return ans;
    }
};
