// To avoid the extra space, we can use the first column and first row for the hash table.
// However, we need to distinguish weather the zeros in the row or column are there at the beginning or they are brought in the later operations.
// A basic idea would be use two flags to show weather there are zeros at the first row or column.

class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
    int m = matrix.size();
    if (m == 0)
        return;
    int n = matrix[0].size();

    bool rowZero = false;
    bool columnZero = false;
    for (int i = 0; i < m; i++)
        if (matrix[i][0] == 0)
            columnZero = true;
    for (int j = 0; j < n; j++)
        if (matrix[0][j] == 0)
            rowZero = true;

    for (int i = 1; i < m; i++)
        for (int j = 1; j < n; j++)
            if (matrix[i][j] == 0)
            {
                matrix[i][0] = 0;
                matrix[0][j] = 0;
            }
    for (int i = 1; i < m; i++)
        if (matrix[i][0] == 0)
            for (int j = 1; j < n; j++)
                matrix[i][j] = 0;
    for (int j = 1; j < n; j++)
        if (matrix[0][j] == 0)
            for (int i = 1; i < m; i++)
                matrix[i][j] = 0;
    if (columnZero == true)
        for (int i = 0; i < m; i++)
            matrix[i][0] = 0;
    if (rowZero == true)
        for (int j = 0; j < n; j++)
            matrix[0][j] = 0;

    return;
    }
};
