// At the beginning I thought a greedy algorithm would work. We pick up the largest square number that is less than n, and then reduce n by this number, and repeat this process until n is 0. It works on half of the test cases, however not on the other.
//
// The correct solution is DP. The track here is f[i+j*j] = min(f[i+j*j], f[i]+1). It is unique because we usually use f[i], f[j] and so on but not f[i+j*j].
// Reference: http://bookshadow.com/weblog/2015/09/09/leetcode-perfect-squares/
class Solution {
public:
    int numSquares(int n) {
	vector<int> f(n+1, 999999);

    f[0] = 0;
	for (int i = 1; i*i <= n; i++)
		f[i*i] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 1; i + j*j <= n; j++)
			f[i+j*j] = min(f[i+j*j], f[i] + 1);
	return f[n];
    }
};
