// Reference: http://articles.leetcode.com/2011/01/sliding-window-maximum.html
// We use a double-entry queue.
// From the queue front to end, we store the index of large value to small value while the index of the values increase, because it is meaningless to store the increasing value with the index increases.
// Then we need to be careful when adding new elements. As before, we still maintain the value decreasing while the index increasing. Then how many old elements should we pop? Certainly we should pop from the front (because the oldest element is in the front) until all the elements are in the window.
// Then the largest element in the window would be the one at the front.
class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        vector<int> ans;
        if (nums.size() == 0)
            return ans;

        deque<int> d;
        for (int i = 0; i < k; i++)
        {
            while (!d.empty() && nums[d.back()] <= nums[i])
                d.pop_back();
            d.push_back(i);
        }
        ans.push_back(nums[d.front()]);

        for (int i = k; i < nums.size(); i++)
        {
            while (!d.empty() && nums[d.back()] <= nums[i])
                d.pop_back();
            d.push_back(i);
            while (!d.empty() && d.front() <= i-k)
                d.pop_front();
            ans.push_back(nums[d.front()]);
        }
        return ans;
    }
};
