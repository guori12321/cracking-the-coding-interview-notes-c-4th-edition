class Solution {
public:
    vector< vector<int> > ans;

    void DFS(TreeNode* root, int h)
    {
        if (root == NULL)
            return;
        if (ans.size() == h)
        {
            vector<int> t;
            ans.push_back(t);
        }
        ans[h].push_back(root->val);
        DFS(root->left, h+1);
        DFS(root->right, h+1);
        return;
    }

    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        DFS(root, 0);
        int n = ans.size();
        // Note that here the initial value is 1 rather than 0, because we would leave the root along and reverse the next layer.
        for (int i = 1; i < n; i += 2)
            reverse(ans[i].begin(), ans[i].end() );
        return ans;
    }
};
