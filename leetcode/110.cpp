// In this problem the definition of "balance" is different from most cases
// So we just check if the tree is balanced during the traverse
class Solution {
public:
    bool ans = true;
    int getHeight( TreeNode* root, int h)
    {
        if (root == NULL)
            return h;
        if (ans == false)
            return h;
        int l = getHeight(root->left, h+1);
        int r = getHeight(root->right, h+1);
        if (l > r+1 || r > l+1)
            ans = false;
        return max(l, r);
    }
    bool isBalanced(TreeNode* root) {
        getHeight(root, 0);
        return ans;
    }
};
