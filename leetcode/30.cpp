// Reference: http://blog.csdn.net/linhuanmars/article/details/20342851
// A good practice for the sliding window algorithm.
// The basic idea would build two hash tables, one for the original words list and one for the current words we meet.
// And we set a count variable to check if we've found all the strings.

// If the out-most loop enumerates the offset, then the words could just be treated as chars.
// And then, we go through the string, and if we find a word that is not in the original hash table, then we clear the current hash table and reset the count variable.
// If we find a word that is in the original table and the number of such a string in the current string is smaller or equal to the one in the original table, then we add it and slide our window to the next string, otherwise if it is larger, then we need to cut all the string before its first apparency in the current string, and then continue to slide the window.
class Solution {
public:
    vector<int> findSubstring(string s, vector<string>& words) {
        vector<int> ans;
        if (s == "" || words.size() == 0)
            return ans;

        unordered_map<string, int> om;
        for (vector<string>::iterator it = words.begin(); it != words.end(); it++)
        {
            if (om.find(*it) == om.end())
                om[*it] = 1;
            else
                om[*it]++;
        }

        int wordLen = words[0].size();
        int n = s.size();
        for (int off = 0; off < wordLen; off++)
        {
            unordered_map<string, int> m;
            int count = 0;
            for (int i = off; i+wordLen <= n; i += wordLen)
            {
                string sub = s.substr(i, wordLen);
                if (om.find(sub) != om.end())
                {
                    count++;

                    if (m.find(sub) == m.end())
                        m[sub] = 1;
                    else
                    {
                        if (m[sub] == om[sub])
                        {
                            int j = i-(count-1)*wordLen;
                            while (s.substr(j, wordLen) != sub)
                            {
                                m[s.substr(j, wordLen)]--;
                                j += wordLen;
                                count--;
                            }
                            m[s.substr(j, wordLen)]--;
                            count--;
                        }
                        m[sub]++;
                    }

                    if (count == words.size())
                        ans.push_back(i - (words.size()-1)*wordLen);
                }
                else
                {
                    m.clear();
                    count = 0;
                }

            }
        }
        return ans;
    }
};
