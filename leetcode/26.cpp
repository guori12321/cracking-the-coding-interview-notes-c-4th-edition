class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int n = nums.size();
        if (n <= 1)
            return n;

        int l = 0, r = 1;
        while (r < n)
        {
            while (r < n && nums[l] == nums[r])
                r++;
            if (r < n)
            {
                l++;
                nums[l] = nums[r];
                r++;
            }
        }

        return l+1;
    }
};
