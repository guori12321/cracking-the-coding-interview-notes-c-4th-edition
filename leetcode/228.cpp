class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums) {
        vector<string> ans;
        int n = nums.size();

        int l = 0;
        while (l < n)
        {
            int r = l+1;
            while (r < n && nums[r] == nums[r-1]+1)
                r++;
            r--;
            if (nums[l] == nums[r])
                ans.push_back( to_string(nums[l]) );
            else
                ans.push_back( to_string(nums[l]) + "->" + to_string(nums[r]) );
            l = r + 1;
        }
        return ans;
    }
};
