// Talk with the interviewer can the subarray be empty, that means, if all the numbers are negative, then should we return 0 or a negative number.
// Then, we need to set the initial value of ans accordingly.
// If the ans should be at least 0, we set it to 0, otherwise we set it to a very small negative number.
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
        int ans = -99999999, t = -99999;
        for (int i = 0; i < n; i++)
        {
            if (t > 0)
                t = nums[i] + t;
            else
                t = nums[i];
            if (t > ans)
                ans = t;
        }
        return ans;
    }
};
