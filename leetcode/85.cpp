// Reference: http://n00tc0d3r.blogspot.com/2013/04/maximum-rectangle.html
// If we want to find the SQUARE, then it is a simple DP problem: f[i][j] = min(f[i-1][j-1], f[i-1][j], f[i][j-1]) if (matrix[i][j] == 1) where f[i][j] means the length of square.
// But, this problem asks for RECTANGLE, so the DP is not working. The valid method is to convert it to the #84. We convert the matrix to rows of histogram, and then scan row by row to find the best solution.

class Solution {
public:
    int maximalRectangle(vector<vector<char> >& matrix) {
        int m = matrix.size();
        if (m == 0)
            return 0;
        int n = matrix[0].size();
        if (n == 0)
            return 0;

        vector< vector<int> > h;
        vector<int> t;
        for (int i = 0; i < n; i++)
        {
            if (matrix[0][i] == '1')
                t.push_back(1);
            else
                t.push_back(0);
        }
        h.push_back(t);

        for (int i = 1; i < m; i++)
        {
            vector<int> t;
            for (int j = 0 ; j < n; j++)
            {
                if (matrix[i][j] == '1')
                    t.push_back(h[i-1][j] + 1);
                else
                    t.push_back(0);
            }
            h.push_back(t);
        }

        int ans = 0;
        for (int i = 0; i < m; i++)
        {
            stack<int> s;
            h[i].push_back(0);

            s.push(0);
            for (int j = 1; j < h[i].size(); j++)
            {
                int r = j-1;
                while (!s.empty() && h[i][s.top()] >= h[i][j])
                {
                    int height = h[i][s.top()];
                    int l = 0;
                    s.pop();
                    if (! s.empty() )
                        // s.top() is a coordinate, but h[s.top ()] is a height!
                        l = s.top() + 1;
                    ans = max(ans, height*(r-l+1) );
                }
                s.push(j);
            }
        }
        return ans;
    }
};
