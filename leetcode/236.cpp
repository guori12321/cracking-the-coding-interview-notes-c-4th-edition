class Solution {
public:
    map<TreeNode*, int> count;
    TreeNode* ans = NULL;

    void find(TreeNode* root, TreeNode* p, TreeNode* q)
    {
        if (root == NULL)
            return;
        if (root == p || root == q)
            count[root] = 1;
        else
            count[root] = 0;
        if (count.find(root->left) == count.end())
            find(root->left, p, q);
        if (count.find(root->right) == count.end())
            find(root->right, p, q);
        count[root] += count[root->left] + count[root->right];
        if (count[root] == 2 && count[root->left] < 2 && count[root->right] < 2)
            ans = root;
        return;
    }

    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        // Remember to deal with the NULL case
        count[NULL] = 0;
        find(root, p, q);
        return ans;
    }
};
