class Solution {
public:
    int ans = 0;
    void DFS(TreeNode *root, int sum)
    {
        if (root == NULL)
            return;
        // A tiny mistake: update sum here rather than after the leaf node judgement, otherwise the sum in the leaf judgement won't be updated
        sum = sum*10 + root->val;

        if (root->left == NULL && root->right == NULL)
        {
            ans += sum;
            return;
        }

        DFS(root->left, sum);
        DFS(root->right, sum);
        return;
    }
    int sumNumbers(TreeNode* root) {
        DFS(root, 0);
        return ans;
    }
};
