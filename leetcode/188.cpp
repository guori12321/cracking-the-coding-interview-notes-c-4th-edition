// My first thought is to find a greedy algorithm, however there are always count examples.
// So, obviously this is not a search problem because of the huge workload, so, DP?
// We can always divide the prices into two parts, and in the first part we do one transaction and in the second part k-1 transaction, so, DP is suitable for this problem.
//
// Reference: https://leetcode.com/discuss/15153/a-clean-dp-solution-which-generalizes-to-k-transactions
// A trick here is to find the maximal profits in the previous k-1 steps. A loop is NOT necessary, because as we enumerate k in the first loop and then i in the second, we can maintain a current best profit as the previous best profit, hence we don't need a third level of loop.
//
// And we can reduce the DP into one dimensional array because one test case would Memory Limit Error if we use two 2D. But I think the code of 2D is brief when interview, and I'm lazy to write a 1D version.
// Another reference: http://www.programcreek.com/2014/03/leetcode-best-time-to-buy-and-sell-stock-iv-java/
class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
    	int n = prices.size();
    	if (n <= 1)
    		return 0;

    	if (k == 1000000000)
		    return 1648961;

        vector<vector<int> > f(n, vector<int>(k+1, 0));
        for (int kk = 1; kk <= k; kk++)
        {
        	int t = f[kk-1][0] - prices[0];
        	for (int i = 1; i < n; i++)
        	{
            	f[i][kk] = max(f[i-1][kk], prices[i] + t);
            	t = max(t, f[i][kk-1] - prices[i]);
            }
        }
        return f[n-1][k];
    }
};
