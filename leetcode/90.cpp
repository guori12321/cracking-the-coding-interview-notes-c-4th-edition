// A little change to the classical DFS.
class Solution {
public:
    vector< vector<int> > ans;
    bool hash[1000] = {0};

    void DFS(vector<int> & nums, int last)
    {
        int n = nums.size();
        if (last == n)
        {
            vector<int> t;
            for (int i = 0; i < n; i++)
                if (hash[i])
                    t.push_back( nums[i] );
            ans.push_back(t);
            return;
        }
        int l = last;
        int r = l + 1;
        while ( r < n && nums[r] == nums[l])
            r++;

        // Note that we need to set flags manually because when go back tracing those flags matter.
        for (int i = l; i < r; i++)
            hash[i] = false;
        DFS(nums, r);
        for (int i = l; i < r; i++)
        {
            hash[i] = true;
            DFS(nums, r);
        }
        for (int i = l; i < r; i++)
            hash[i] = false;
        return;
    }
    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        sort(nums.begin(), nums.end() );
        DFS(nums, 0);
        return ans;
    }
};
