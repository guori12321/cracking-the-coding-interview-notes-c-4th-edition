// The O(n^3) method is obvious: we can enumerate every two nodes and then enumerate all the possible third nodes to test if all the three nodes are on the same line.
//
// The O(n^2) method requires a little trick on enumerate.
// At the beginning I just enumerate every two nodes and then record their slops and intercepts, and then we can decide how many points are on the line given the slop and intercept.
// But there is two big issues. One is the vertical lines don't have slop, for this case we can just assume one unreal double value as the slop and pray there is no conflict on this slop. Another is there may be points that are same. For this problem I don't know how to handle it. The last trouble is to decide how many points are on the line. As we enumerate every two points on the line, the total number will be C(n 2), and I don't know to get the floor or ceil value of the sqrt(ans*2).
//
// A better method is to enumerate the first point on the line, and then enumerate the second points and maintain a hash table of the slop of the line of the two nodes. We still need to deal with vertical line and same nodes, but things are much easier here.

/**
 * Definition for a point.
 * struct Point {
 *     int x;
 *     int y;
 *     Point() : x(0), y(0) {}
 *     Point(int a, int b) : x(a), y(b) {}
 * };
 */
class Solution {
public:
    int maxPoints(vector<Point>& points) {
        int n = points.size();
        map< pair<double, double>, int> m;
        // Note that 0 or 1 point cannot be described by the combination
        if (n == 0 || n == 1)
            return n;

        int ans = 2;
        for (int i = 0; i < n; i++)
        {
            int same = 1; // The points[i] is same as itself
            int vertical = 0;
            int numOnLine = 0;
            // Remember to clear the m here.
            m.clear();

            for (int j = i+1; j < n; j++)
            {
                double k;
                if (points[i].x == points[j].x && points[i].y == points[j].y)
                    same++;
                else if (points[i].x == points[j].x)
                    vertical++;
                else
                {
                    // Be careful about int to double!!! Otherwise the right side would be divided as int and then assigned to a double
                    k = double(points[i].y - points[j].y) / (points[i].x - points[j].x);
                    // In this case we don't need to record intercept as we've enumerated the first point on the line
                    int d = points[i].y - points[i].x*k;
                    pair<double, double> t = make_pair(k, d);
                    if (m.find(t) != m.end() )
                        m[t]++;
                    else
                        m[t] = 1;
                    numOnLine = max(numOnLine, m[t]);
                }
            }
            ans = max(ans, max(vertical, numOnLine) + same);
        }
        return ans;
    }
};
