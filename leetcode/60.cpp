class Solution {
public:
    int getFactorial(int k)
    {
        int ans = 1;
        for (int i = 1; i <= k; i++)
            ans *= i;
        return ans;
    }

    string getPermutation(int n, int k) {
        vector<int> nums;
        for (int i = 0; i < n; i++)
            nums.push_back(i+1);

        string ans = "";
        // the index of the first permutation is 1, rather than 0, so we shift k to be the index.
        // Otherwise k is from 1..n, and we would later mod it, and then it would be from 0..n-1, and we would have bugs.
        k--;
        while (n > 0)
        {
            int t = k/getFactorial(n-1);
            k %= getFactorial(n-1);
            ans += (nums[t] + '0' ) ;
            nums.erase(nums.begin() + t);
            n--;
        }
        return ans;
    }
};
