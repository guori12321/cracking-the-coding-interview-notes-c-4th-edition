// Use the Trier (prefix tree) to prune the tree.
// At the beginning I thought the prefix may overlap, and the prefix and the middle part may overlap as well. Here to group the same prefix Trier is the best data structure.
// Reference: http://www.cnblogs.com/easonliu/p/4514110.html

class Solution {
public:
    unordered_set<string> ans;
    vector<string> finAns;
    struct Trier{
        bool end = false;
        Trier* next[26] = {0};
    };

    bool visited[500][500];
    // The current string that has been searched. We can also pass the searched string in the DFS parameters, however this would involve in string copy which would lead to TLE.
    string st;

    void DFS(vector<vector<char> > board, Trier* root, int x, int y)
    {
        int m = board.size(), n = board[0].size();
        if (x < 0 || x >= m || y < 0 || y >= n)
            return;
        if (visited[x][y] || root == NULL)
            return;
        if (root->next[ board[x][y] - 'a' ] == NULL)
            return;

        st.push_back(board[x][y]);
        if (root->next[ board[x][y] - 'a']->end)
        {
            // It is not convenient to erase one word in the prefix tree, however we can at least erase the end flag.
            root->next[ board[x][y] - 'a']->end = false;
            finAns.push_back(st);
        }

        visited[x][y] = true;
        int dir[4][2] = { {0, 1}, {0, -1}, {1, 0}, {-1, 0} };
        for (int i  = 0; i < 4; i++)
            DFS(board, root->next[ board[x][y] - 'a'], x+dir[i][0], y+dir[i][1]);
        visited[x][y] = false;
        st.pop_back();
        return;
    }

    vector<string> findWords(vector<vector<char> >& board, vector<string>& words) {
        int numWords = words.size();
        int m = board.size();
        if (m == 0 || numWords == 0)
            return finAns;
        int n = board[0].size();

        memset(visited, sizeof(visited), 0);

        Trier* root = new Trier();
        for (int i = 0; i < numWords; i++)
        {
            Trier* t = root;
            for (int j = 0; j < words[i].size(); j++)
            {
                if (t->next[ words[i][j] - 'a' ] == NULL)
                    t->next[ words[i][j] - 'a' ] = new Trier();
                t = t->next[ words[i][j] - 'a' ];
                if (j == words[i].size() - 1)
                    t->end = true;
            }
        }

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                DFS(board, root, i, j);
        return finAns;
    }
};
