// DFS
class Solution {
public:
    vector< vector<int> > ans;
    void DFS(TreeNode* root, int h)
    {
        if (root == NULL)
            return;
        if (ans.size() <= h)
        {
            vector<int> t;
            ans.push_back(t);
        }
        ans[h].push_back(root->val);
        DFS(root->left, h+1);
        DFS(root->right, h+1);
        return;
    }

    vector<vector<int>> levelOrder(TreeNode* root) {
        DFS(root, 0);
        return ans;
    }
};

// BFS
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        queue<TreeNode*> q;
        queue<int> height;
        vector< vector<int> > ans;
        q.push(root);
        height.push(0);

        while (! q.empty() )
        {
            while( !q.empty() && q.front() == NULL)
            {
                q.pop();
                height.pop();
            }
            if (q.empty())
                return ans;

            TreeNode* t = q.front();
            int h = height.front();
            q.pop();
            height.pop();

            if (ans.size() <= h)
            {
                vector<int> v;
                ans.push_back(v);
            }
            ans[h].push_back(t->val);
            q.push(t->left);
            q.push(t->right);
            height.push(h+1);
            height.push(h+1);
        }
        return ans;
    }
};
