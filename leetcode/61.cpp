// Be careful about the cases where k > the length of the list (in this case we mod it), empty input (head == NULL)
class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        if (head == NULL)
            return NULL;

        int n = 0;
        ListNode* t = head;
        while (t != NULL)
        {
            n++;
            t = t->next;
        }

        k %= n;

        if (head == NULL || k == 0)
            return head;

        ListNode* p1 = head, *p2;

        for (int i = 0; i < k; i++)
        {
            if (p1 == NULL)
                return head;
            p1 = p1->next;
        }
        p2 = head;

        if (p1 == NULL)
            return head;

        while (p1->next != NULL)
        {
            p1 = p1->next;
            p2 = p2->next;
        }
        ListNode* ans = p2->next;
        p1->next = head;
        p2->next = NULL;
        return ans;
    }
};
