// To speed up, we need to double divisor every time, and then, for the remaining part of dividend, we still need to speed up it, so we call the function recursively.
//
// Another point is the precision. As we double them, we need the long long rather than int.
// And note that 0-INT_MIN != INT_MAX, in fact it would still be INT_MIN because we overflow.
// So here use many long long.
class Solution {
public:
    int divide(int dividend, int divisor) {
        long long ldividend = dividend, ldivisor = divisor;
        if (ldivisor == 0)
            return INT_MAX;
        int positive = 1;
        if ( (ldividend < 0 && ldivisor > 0) || (ldividend > 0 && ldivisor < 0) )
            positive = -1;

        ldividend = abs(ldividend);
        ldivisor = abs(ldivisor);

        //cout << "ldividend " << ldividend << endl;
        //cout << "ldivisor " << ldivisor << endl;

        if (ldividend < ldivisor)
            return 0;

        ldividend -= ldivisor;
        long long ans = 1;
        long long d = ldivisor;

        while (ldividend - d > 0)
        {
            ldividend -= d;
            ans *= 2;
            d *= 2;
            //cout << "d " << d << endl;
        }

        ans *= positive;
        ans += positive * divide(ldividend, ldivisor);
        if (ans > INT_MAX || ans < INT_MIN)
            return INT_MAX;
        else
            return ans;
    }
};
