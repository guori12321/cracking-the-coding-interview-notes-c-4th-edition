class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.size();

        bool f[1001][1001] = {0};
        int ansi = 0, ansj = 0;
        for (int i = 0; i < n; i++)
            f[i][i] = 1;

        for (int i = 1; i < n; i++) // ending point - starting point, or length - 1
            for (int j = 0; j < n-i; j++) // starting point
                if ( s[j] == s[j+i])
                    if (j+1 > j+i-1 || f[j+1][j+i-1]) // j+1 > j+i-1 means the palindromic has a length of 2
                    {
                        f[j][j+i] = true;
                        // ansi is not i!!!
                        ansi = j;
                        ansj = j+i;
                    }
        return s.substr(ansi, ansj-ansi+1);
    }
};
