// It's simple, but be clear in your mind and try different examples!
//
class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        ListNode* vh = new ListNode(-99999);
        vh->next = head;

        ListNode* p1 = vh;
        // On the linked list the nodes at the left side of p1 (INCLUDE p1) are ordered.
        while (p1->next != NULL)
        {
            // There we want to sort the node p1->next, so the initial value of p1 will be p1 and we will judge p1->next
            ListNode* t = vh;
            while ( t != p1 && t->next->val < p1->next->val)
                t = t->next;
            // It is needless to insert p1->next at the end of p1: it's already here, however, we need to judge if we need to forward p1 manually
            if (t != p1)
            {
                ListNode* t1 = t->next;
                ListNode* t2 = p1->next->next;
                t->next = p1->next;
                p1->next->next = t1;
                p1->next = t2;
            }
            // We need this else because if we move p1 in front of p1 then p1 is already moved forward (p1->next has changed or not).
            else
                p1 = p1->next;
        }
        return vh->next;
    }
};
