// At the beginning I believed this problem was easy despite of the AC rate of 18.1%. I thought I could do it in-place: just to shift the array. However I could not write a perfect solution because there are always counter examples.
// Shift works only when n % d == 0. And in other cases we need to replace the elements like a linked-list: when we jump back we might arrive at the nums[1] rather than nums[0].
// So, how to judge if we replace all the elements? I chose to use a hash table which lead to O(n) space complexity. Or we can judge if n % d == 0 and then implement both the swift or jump-around code.
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        // k might be much larger than n
        k %= n;

        int d = 0, last = nums[0];
        while (true)
        {
            d = (d+k) % n;
            int t = nums[d];
            nums[d] = last;
            last = t;
            if (d == 0)
                break;
        }

        return;
    }
};

// Another method is to reverse three times. Reference: http://www.programcreek.com/2015/03/rotate-array-in-java/
