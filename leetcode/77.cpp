// A very basic and easy one, but I still submitted 6 times to AC.
// Like 8 years ago, I still treated this one as permutation at the beginning...
//
// It's very similar to permutation, just a little change to the classical DFS algorithm.
class Solution {
public:
    vector< vector<int> > ans;
    vector<int> s;
    void DFS(int n, int k, int d, int last)
    {
        if (d == k)
        {
            ans.push_back(s);
            return;
        }
        if (n - last <= k - d)
            return;

        // Here we cannot get the last, so in the main() function, we initial last to be -1
        for (int i = last + 1; i < n; i++)
        {
            s[d] =i+1;
            DFS(n, k, d+1, i);
        }
        return;
    }

    vector<vector<int>> combine(int n, int k) {
    // Note that here we have k numbers rather than n
    for (int i = 0; i < k; i++)
        s.push_back(-1);
        DFS(n, k, 0, -1);
        return ans;
    }
};
