// This one is in the CC.
// As 10 = 2 * 5 and 2*5 only, we just want to count how many 2*5 are possible in the n! . Because 2s are more than 5s, so we just count how many factor of 5s are in the n! .
// So, first we count how many 5s are in the n! : it is n! / 5. And then, as 25 would contribute two 5s in the n!, so we count how many 25 : n!/25 . And as the 25 contribute two 5s and it is already counted once in the previous count, here we add result into ans again. And then 25 * 5 until no more 5 are founded.

class Solution {
public:
    int trailingZeroes(int n) {
        int ans = 0;
        // n might be very large, almost INT_MAX. So, the t here might be overflowed. So a unsigned long long would help.
        // Note that unsigned int or unsigned long is not long enough.
        unsigned long long t = 5;
        while (t > 0 && n / t > 0)
        {
            ans += n / t;
            t *= 5;
        }
        return ans;
    }
};
