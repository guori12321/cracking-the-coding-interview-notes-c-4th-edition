// Obvious it is DP.
class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        vector<vector<int> > f;
        int m = dungeon.size();
        if (m == 0)
            return 0;
        int n = dungeon[0].size();
        if (n == 0)
            return 0;

        vector<int> t;
        for (int j = 0; j < n; j++)
            t.push_back(1);
        for (int i = 0; i < m; i++)
        {
            f.push_back(t);
        }

        f[m-1][n-1] = max(1 - dungeon[m-1][n-1], 1);
        for (int j = n-2; j >= 0; j--)
            f[m-1][j] = max( f[m-1][j+1]-dungeon[m-1][j], 1);
        for (int i = m-2; i >= 0; i--)
            f[i][n-1] = max(f[i+1][n-1]-dungeon[i][n-1], 1);

        for (int i = m-2; i >= 0; i--)
            for (int j = n-2; j>=0; j--)
                f[i][j] = max(1, min(f[i+1][j], f[i][j+1])-dungeon[i][j] );
        return f[0][0];
    }
};
