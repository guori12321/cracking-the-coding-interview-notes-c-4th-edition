// If we want to run at O(n) time, then we either need O(n) buffer space or we change the linked list.
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        if (head == NULL)
            return true;

        int n = 0;
        ListNode* t = head;
        while (t != NULL)
        {
            t = t->next;
            n++;
        }

        ListNode* vh = new ListNode(-99999);
        //remember to link the next of vh!
        vh->next = head;

        t = vh->next;
        for (int i = 0; i < n/2 - 1; i++)
        {
            ListNode* t1 = vh->next;
            vh->next =  t->next;
            t->next = t->next->next;
            vh->next->next = t1;
        }

        ListNode* l = vh->next;
        if (n % 2 == 1)
            t = t->next;

        // Just to check if the linked list has only one node
        if (t == NULL)
            return true;
        t = t->next;
        while (t != NULL)
        {
            if (l->val != t->val)
                return false;
            l = l->next;
            t = t->next;
        }
        return true;
    }
};

// If we don't change the linked list, then we need O(n^2) time
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        if (head == NULL)
            return true;
        int n = 0;
        ListNode* t = head;
        while (t != NULL)
        {
            n++;
            t = t->next;
        }

        ListNode* l = head;
        ListNode* r = l;
        for (int i = 0; i < n/2; i++)
        {
            r = l;
            for (int j = 0; j < n-1-i - i; j++)
                r = r->next;

            if (l->val != r->val)
                return false;

            l = l->next;
        }
        return true;
    }
};
