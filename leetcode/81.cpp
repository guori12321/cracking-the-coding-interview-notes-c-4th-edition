// Related to 33
// This is more complicated than 33. 5 conditions in total, however if clear in mind then it could be very easy

class Solution {
public:
    bool search(vector<int>& nums, int target) {
        int n = nums.size();
        if (n == 0)
            return -1;

        int l = 0, r = n-1;
        while (l <= r)
        {
            int m = (l+r) / 2;
            if (nums[m] == target)
                return true;

            if (nums[l] < nums[m] && target >= nums[l] && target < nums[m])
                r = m - 1;
            else
                if (nums[m] < nums[r] && target > nums[m] && target <= nums[r] )
                    l = m + 1;
            else
                if (nums[l] > nums[m] && (target >= nums[l] || target < nums[m]) )
                   r = m - 1;
             else
                if (nums[m] > nums[r] && (target > nums[m] || target <= nums[r] ))
                   l = m + 1;
            // If nums[l] == nums[m], then, all the numbers in the left may be the equal, or they might be (-INT_MAX, nums[l]], [nums[l], +INT_MAX), in other words, any possible numbers. In this case we can only do a linear search
             else
                l++;
        }
        return false;
    }
};
