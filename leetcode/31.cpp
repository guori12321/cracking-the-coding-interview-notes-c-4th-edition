// Next Permutation, image the vector as a number, then, the next permutation means we need to increase the number, and then, increase as less as possible.
// First we need to guarantee that we can increase, otherwise it would be cases like 3, 2, 1 and we should return 1, 2, 3.
// The idea is to scan from right to left, and when we find nums[l] < nums[l+1], then we can get a larger number by swap nums[l] and nums[l+1].
// However, such a swap would increase the number, but not increase to the next smallest one. As the numbers at the right side of nums[l] are in decreasing order, we scan from right to left to find the first one that is smaller than nums[l], and swap it with nums[l].
// Is that enough? Not yet, because now nums[l] is fixed and its right side is in decreasing order, and we can make it even smaller by reverse it to increasing order. Then, it's done.
class Solution {
public:
    void nextPermutation(vector<int>& nums) {
        int n = nums.size();
        if (n == 1)
            return;

        // We need to claim l out of the for loop
        int l;
        for (l = n-2; l >= 0; l--)
            if (nums[l] < nums[l+1])
                break;

        if (l == -1)
        {
            reverse(nums.begin(), nums.end() );
            return;
        }

        int r;
        for (r = n-1; r > l; r--)
            if (nums[l] < nums[r])
                break;

        int t = nums[l];
        nums[l] = nums[r];
        nums[r] = t;

        //reverse till the end, rather than just till the r
        reverse(nums.begin() + l + 1, nums.end());
        return;
    }
};
