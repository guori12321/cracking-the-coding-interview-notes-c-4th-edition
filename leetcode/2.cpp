class Solution {
public:
    ListNode* insert(ListNode* t, int x)
    {
        t->next = new ListNode(-99999);
        t = t->next;
        t->val = x;
        return t;
    }

    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* head = new ListNode(-99999);
        ListNode* t = head;
        int carry = 0;

        while (l1 != NULL || l2 != NULL || carry != 0)
        {
            int a = 0, b = 0;
            if (l1 != NULL)
            {
                a = l1->val;
                l1 = l1->next;
            }
            if (l2 != NULL)
            {
                b = l2->val;
                l2 = l2->next;
            }
            int sum = a + b + carry;
            t = insert(t, sum%10);
            carry = sum/10;
        }
        return head->next;
    }
};
