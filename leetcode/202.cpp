class Solution {
public:
    bool isHappy(int n) {
        set<int> hash;

        // Usually find() is with != end(), but here is ==
        while (n != 1 && hash.find(n) == hash.end() )
        {
            hash.insert(n);
            int t = 0;
            while (n > 0)
            {
                t += (n%10) * (n % 10);
                n /= 10;
            }
            n = t;
        }
        if (n == 1)
            return true;
        return false;
    }
};
