class TrieNode {
public:
    TrieNode* next[26];
    bool end;
    // Initialize your data structure here.
    TrieNode() {
             for (int i = 0; i < 26; i++)
                next[i] = NULL;
            end = false;
    }
};

class Trie {
public:
    Trie() {
            root = new TrieNode();
    }

    // Inserts a word into the trie.
    void insert(string word) {
        int n = word.size();
        TrieNode* t = root;
        for (int i = 0; i < n; i++)
        {
            if (t->next[ word[i] - 'a' ] == NULL)
                t->next[ word[i] - 'a' ] = new TrieNode();
            t = t->next[ word[i] - 'a' ];
        }
        t->end = true;
        return;
    }

    // Returns if the word is in the trie.
    bool search(string word) {
        int n = word.size();
        TrieNode* t = root;
        for (int i = 0; i < n; i++)
        {
            if (t->next[ word[i] - 'a' ] == NULL)
                return false;
            t = t->next[ word[i] - 'a' ];
        }
        if (t->end)
            return true;
        return false;
    }

    // Returns if there is any word in the trie
    // that starts with the given prefix.
    bool startsWith(string prefix) {
        int n = prefix.size();
        TrieNode *t = root;
        for (int i = 0; i < n; i++)
        {
            if (t->next[prefix[i] - 'a'] == NULL)
                return false;
            t = t->next[prefix[i] - 'a'];
        }
        return true;
    }

private:
    TrieNode* root;
};
