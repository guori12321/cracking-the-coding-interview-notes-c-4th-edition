 class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) {
        vector<int> last, now;

        int n = triangle.size();
        if (n == 0)
            return 0;
        for (int i = 0; i < n; i++)
        {
            last.push_back(99999);
            now.push_back(99999);
        }

        now[0] = triangle[0][0];
        last = now;
        for (int i = 1; i < n; i++)
        {
            now[0] = triangle[i][0] + last[0];
            for (int j = 1; j <= i-1; j++)
                now[j] = triangle[i][j] + min(last[j], last[j-1]);
            now[i] = triangle[i][i] + last[i-1];
            last = now;
        }
        int ans = 9999999;
        for (int i = 0; i < now.size(); i++)
            ans = min(ans, now[i]);
        return ans;
    }
};
