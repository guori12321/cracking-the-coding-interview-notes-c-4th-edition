// My first idea is to search, but it would certainly be TLE. My second idea is to do a memorized search, but I just thought to use the substring, rather than the starting and ending index of the string as the memorized array index, so the memorized method won’t help a lot. Next thought is DP, but my intuition told me that 3D or more would be TEL and Memory Error (but it is the right way!). And a few greedy methods are obviously not working.
// It is in fact a clear and simple 3D DP. So, don’t be afraid of using more memory than expected. It’s free to new or malloc after all!
// For such a problem, it’s either searching, DP, or greedy. Just try them!

class Solution {
public:
   bool isScramble(string s1, string s2) {
        // f[i][j][k] = 0 means if s1[j : j+i-1] and s2[ k: k+i-1] is scrambled. i is length of the string, j is the starting location in s1, and j is in s2.
        bool f[100][100][100] = {0};

        if (s1.size() != s2.size() )
            return false;
        if (s1 == s2)
            return true;

        int n = s1.size();

        // as i is length, i should be able to be n
        for (int i = 1; i <= n; i++)
        {
            for (int j = 0; j < n-i+1; j++)
                for (int k = 0; k < n-i+1; k++)
                {
                    if (i == 1)
                    {
                        if (s1[j] == s2[k])
                            f[i][j][k] = 1;
                    }
                    else
                    // l is the length of the first part in s1
                    for (int l = 1; l < i; l++)
                    {
                        // Note that the last item is f[i-l][j+l][k] rather than f[i-l][j+(i-l)][k]
                        f[i][j][k] |= (f[l][j][k] && f[i-l][j+l][k+l]) || (f[l][j][k+(i-l)] && f[i-l][j+l][k]);
                    }
                }
        }

        return f[n][0][0];
    }

};
