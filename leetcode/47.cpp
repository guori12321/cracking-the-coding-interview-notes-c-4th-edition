// The reason that there are duplicated permutations is the permutation of the same number.
// In the given example
//
// For example,
// [1,1,2] have the following unique permutations:
// [1,1,2], [1,2,1], and [2,1,1].
//
// If we mark the two 1 with characters 1a and 1b, then 1a 1b 2 and 1b 1a 2 would be two different permutation, however, here they are treated as the same permutation.
//
// So the solution would be to mark the same numbers and let the first one always be before the second one, otherwise there would be something like 1b 1a and then duplicated.
// The question is, how to mark the same numbers? Well, we can maintain a table containing how many times a number is used. At the beginning all numbers are used 0 times, and to use a new number, we have to select the 1st one in the original nums variable (e.g. we can only select 1a rather than 1b or 1c because 1a is the first one).
// To figure out the ranking of the current nums[i] (the current 1 is 1a or 1b), we maintain another table containing the index of the first number (i.e. 1a in the previous example).
class Solution {
public:
    vector< vector<int> > ans;
    bool visited[10000] ={};
    vector<int> s;
    map<int, int> first;
    int show[10000] = {};


void DFS(vector<int>& nums, int d, int last)
{
    int n = nums.size();
    if (n == d)
    {
        ans.push_back(s);
        return;
    }
    for (int i = 0; i < n; i++)
        if ( !visited[i] && (i - first.find(nums[i])->second == show[nums[i]] ) )
        {
            s[d] = nums[i];
            visited[i] = true;
            show[nums[i]]++;
            DFS(nums, d+1, i);
            visited[i] = false;
            show[nums[i]]--;
        }
    return;
}


    vector<vector<int>> permuteUnique(vector<int>& nums) {
        sort(nums.begin(), nums.end() );

    for (int i = 0; i < nums.size(); i++)
        if (first.find(nums[i]) == first.end())
            first[nums[i]] = i;

             s = nums;
        DFS(nums, 0, -1);
        return ans;
    }
};

// Another way to avoid the duplications is to count how many times a number is showed in the nums. And then, do the DFS and recursive if there are still numbers.
// In this way the first two ones in [1, 1, 2] are treated the same so there would be no duplication.
//
class Solution {
public:
    map<int, int> m;
    vector< vector<int> > ans;
    vector<int> s;

    void DFS(vector<int>& nums, int d)
    {
        int n = nums.size();
        if (n == d)
        {
            ans.push_back(s);
            return;
        }
        for (map<int, int>::iterator it = m.begin(); it != m.end(); it++)
        {
            if (it->second > 0)
            {
                it->second--;
                s[d] = it->first;
                DFS(nums, d+1);
                it->second++;
            }
        }
        return;
    }

    vector<vector<int>> permuteUnique(vector<int>& nums) {
        int n = nums.size();
        for (int i = 0; i < n; i++)
            if (m.find(nums[i]) == m.end())
                m[ nums[i] ] = 1;
            else
                m[ nums[i] ] = m.find(nums[i])->second + 1;
        s = nums;

             DFS(nums, 0);
        return ans;
    }
};
