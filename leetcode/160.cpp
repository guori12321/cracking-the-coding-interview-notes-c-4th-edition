// Don't panic!
// The idea is very simple. First we count the length of the two linked lists, and then, align the longer one with the shorter one at the end by moving the header of the longer one. Next, we move t1 and t2 together until they are the same or NULL.

class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int n1 = 0, n2 = 0;
        // In such a sentence just remember the start note in front of t2.
        ListNode* t1 = headA, *t2 = headB;
        while (t1 != NULL)
        {
            t1 = t1->next;
            n1++;
        }
        while (t2 != NULL)
        {
            t2 = t2->next;
            n2++;
        }

        t1 = headA;
        t2 = headB;
        for (int i = 0; i < n1 - n2; i++)
            t1 = t1->next;
        for (int i = 0; i < n2 - n1; i++)
            t2 = t2->next;
        while (t1 != NULL && t2 != NULL)
        {
            if (t1 == t2)
                return t1;
            t1 = t1->next;
            t2 = t2->next;
        }
        return NULL;
    }
};
