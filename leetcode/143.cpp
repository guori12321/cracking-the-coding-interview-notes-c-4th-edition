// The key to solve such a linked list problem is to write things down on paper and then learn how to link those elements.
class Solution {
public:
    void reorderList(ListNode* head) {
        if (head == NULL)
            return;

        int n = 0;
        ListNode* t = head;
        stack<ListNode*> st;
        while (t != NULL)
        {
            n++;
            st.push(t);
            t = t->next;
        }

        // We want to get the node in front of the tail node, so here we pop one tail node
        st.pop();

        // Add * in front of t2!!!
        ListNode* t1 = head, *t2;
        for (int i = n-1; i > 1; i -= 2)
        {
            // Instead of the following visiting codes, we use the stack to speed up our codes
            //t2 = t1;
            //for (int j = 0; j < i-1; j++)
            //	t2 = t2->next;
            t2 = st.top();
            st.pop();

            ListNode* t1Next = t1->next;
            ListNode* t2Next = t2->next;
            t2->next = t2->next->next;
            t1->next = t2Next;
            t2Next->next = t1Next;
            t1 = t1Next;
        }
        return;
    }
};
