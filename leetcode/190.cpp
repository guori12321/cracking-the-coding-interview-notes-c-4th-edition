// An obvious method:
class Solution {
public:
    uint32_t reverseBits(uint32_t n) {
        uint32_t ans = 0;
        for (int i = 0; i < 32; i++)
        {
            if ( (n >> i) & 1 == 1)
                ans |= 1 << (31-i);
        }
        return ans;
    }
};

// And one obvious method to optimize is to use a loop up table as the buffer. As it is not practical to prepare a loop up table for all the unsigned int in advance, at least we can manage a loop up table of 4 bits and reverse 4 bits once to speed up our algorithm.
// In fact, both methods run at 4 ms in Leetcode.
class Solution {
public:
    uint32_t reverseBits(uint32_t n) {
        // 0b0000, 0b1000, 0b0100 ...
        char table[16] = {0,8,4,12,2,10,6,14,1,9,5,13,3,11,7,15};
        uint32_t ans = 0;
        for (int i = 0; i < 32 / 4; i++)
        {
            int t = (n >> (i*4) ) & 0b1111;
            ans |= table[t] << ( (32-4)-i*4);
        }
        return ans;
    }
};

