// This one seems to be a greedy algorithm, however I cannot figure out the greedy way, so, another method seems to be binary search (i.e. divided and conquer): if we choose the middle one and if we don't.
// So, very brief idea and code~~
class Solution {
public:
    int ans = 0;
    int DFS(vector<int> & nums, int l, int r)
    {
        if (l > r)
            return 0;
        if (l == r)
            return nums[l];

        int m = (l+r) / 2;
        return max( DFS(nums, l, m-1) + DFS(nums, m+1, r),
        nums[m] + DFS(nums, l, m-2) + DFS(nums, m+2, r) );
    }

    int rob(vector<int>& nums) {
        int n = nums.size();
        if (n == 0)
            return 0;
        int m = n / 2;
        ans = DFS(nums, 0, n-1);
        return ans;
    }
};
