// A binary search.
// Note that if the middle element is smaller or larger than the target, then we only know the left-up corner or right-down corner is not satisfied.

class Solution {
public:
bool DFS(vector<vector<int> >& matrix, int target, int ax, int ay, int bx, int by)
{
    // Not necessary to judge if ax >= matrix.size() as bx >= ax && bx <= matrix.size()
    if (ax < 0 || bx >= matrix.size() || bx < ax || ay < 0 || by >= matrix[0].size() || by < ay)
        return false;

    int mx = (ax+bx) / 2, my = (ay+by) / 2;
    if (matrix[mx][my] == target)
        return true;
    if (matrix[mx][my] > target)
        return DFS(matrix, target, ax, ay, bx, my-1) || DFS(matrix, target, ax, my, mx-1, by);

    return DFS(matrix, target, mx+1, ay, bx, by) || DFS(matrix, target, ax, my+1, mx, by);
}

bool searchMatrix(vector<vector<int> >& matrix, int target) {
    int m = matrix.size();
    if (m == 0)
        return false;
    int n = matrix[0].size();
    if (n == 0)
        return false;
    return DFS(matrix, target, 0, 0, m-1, n-1);
}
};
