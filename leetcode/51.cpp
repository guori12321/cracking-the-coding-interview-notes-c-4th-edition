class Solution {
public:
    vector< vector<string> > ans;
    int s[1000];

    bool check(int k, int x)
    {
        for (int i = 0; i < k; i++)
        {
            if (s[i] == x || abs(i - k) == abs(s[i] - x) )
                return false;
        }
        return true;
    }

    void DFS(int n, int k)
    {
        if (n == k)
        {
            vector<string> tans;
            for (int i = 0; i < n; i++)
            {
                string t = "";
                for (int j = 0; j < n; j++)
                    t += ".";
                // Here is char, the last sentence is string.
                t[ s[i] ] = 'Q';
                tans.push_back(t);
            }
            ans.push_back(tans);
            return;
        }

        for (int i = 0; i < n; i++)
            if (check(k, i))
            {
                s[k] = i;
                DFS(n, k+1);
            }
        return;
    }

    vector<vector<string>> solveNQueens(int n) {
        DFS(n, 0);
        return ans;
    }
};
