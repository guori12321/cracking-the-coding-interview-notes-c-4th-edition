// Recursively
class Solution {
public:
    ListNode* ans = NULL;
    void DFS(ListNode* now, ListNode *prev)
    {
        if (now->next == NULL)
        {
            now->next = prev;
            ans = now;
            return;
        }

        DFS(now->next, now);
        now->next = prev;
        return;
    }

    ListNode* reverseList(ListNode* head) {
        // Empty input is terrible, and here we must deal with it specially
        if (head == NULL)
            return NULL;
        DFS(head, NULL);
        return ans;
    }
};

//Iteratively
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        if (head == NULL)
            return NULL;

        ListNode * now = head, *prev = NULL;
        while (now != NULL)
        {
            ListNode* t = now->next;
            now->next = prev;
            prev = now;
            now = t;
        }
        return prev;
    }
};
