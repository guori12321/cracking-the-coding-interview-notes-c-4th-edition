// This is obvious a searching problem.
// The key is how to prune the searching tree.
// For this test case, the simple search would TLE
// "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab", ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
// So, we just need one visited boolean array, and visited[i] means we have split s at s[i] and search if it works.
// Otherwise as the two parts s[0:i] and s[i:n] has many way to be split, and we would take a much longer time to search.
class Solution {
public:
    bool visited[1000000] = {0};
    bool ans = false;

    void DFS(int done, string s, unordered_set<string>& wordDict)
    {
        if (s == "" || ans == true)
        {
            ans = true;
            return;
        }

        int n = s.size();
        // Find the longest substr first to speedup
        for (int i = n; i >= 1; i--)
            if (wordDict.find( s.substr(0, i) ) != wordDict.end() && visited[done+i] == false )
            {
                visited[done+i] = true;
                DFS( done + i,  s.substr(i, n-i), wordDict);
            }

        return;
    }
    bool wordBreak(string s, unordered_set<string>& wordDict) {
        DFS(0, s, wordDict);
        return ans;
    }
};
