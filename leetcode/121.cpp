// We enumerate the sell day, and keep a track of the lowest price before the sell day (i.e. the buy day)
class Solution {
public:
    int maxProfit(vector<int>& prices) {
    int ans = 0;
    int n = prices.size();
    int lowDay = 0;
    for (int i = 1; i < n; i++)
    {
        if (prices[i] < prices[lowDay])
            lowDay = i;
        ans = max(ans, prices[i] - prices[lowDay] );
    }
    return ans;
    }
};
