class Solution {
public:
    int missingNumber(vector<int>& nums) {
        unsigned long long tot = 0;
        int n = nums.size();

        for (int i = 0; i < n; i++)
            tot += nums[i];

        return int(((unsigned long long)n*(unsigned long long)(1+n)/2) - tot);
    }
};
