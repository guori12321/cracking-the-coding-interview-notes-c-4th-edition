class Solution {
public:
    string convert(string s, int numRows) {
        if (numRows == 1)
            return s;

        string ans = "";
        int n = s.size();

        for (int i = 0; i < numRows; i++)
            // As here we add j by 2*numRows-2, so if numRows is 1 then j would never be increased. As a result the judge of weather numRows is 1 is needed.
            for (int j = i; j < n; j += 2*numRows-2)
            {
                ans += s[j];
                if (i != 0 && i != numRows -1 && j+2*numRows-2-2*i < n)
                    ans += s[j+2*numRows-2-2*i];
            }
        return ans;
    }
};
