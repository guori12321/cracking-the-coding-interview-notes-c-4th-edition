// Lots of details to care about!
// A good reference: http://www.cnblogs.com/tenosdoit/p/3649607.html
// Another possible solution is to get the hash table of the addition of every pair, and then enumerate the pairs rather than the beginning two elements.
// Because the pairs should be less than the number of elements, our algorithm can run faster, however after a long time I still have trouble to make the result unique, so I give up.
class Solution {
public:
    vector<vector<int> > fourSum(vector<int>& nums, int target) {
        vector< vector<int> > ans;
        int n = nums.size();
        if (n < 4)
            return ans;

        // We would TLE if we use map here.
        unordered_map<int, vector<pair<int, int> > > m;
        sort(nums.begin(), nums.end());

        for (int i = 0; i < n; i++)
        {
            for (int j = i+1; j < n; j++)
                m[ nums[i] + nums[j] ].push_back(make_pair(i, j));
        }

        // <= n-4, rather than n-3
        for (int i = 0; i <= n-4; i++)
        {
            if (i > 0 && nums[i] == nums[i-1])
                continue;
            for (int j = i+1; j <= n-3; j++)
            {
                if (j > i+1 && nums[j] == nums[j-1])
                    continue;

                if (m.find(target-nums[i]-nums[j]) != m.end())
                {
                    int t = target-nums[i]-nums[j];
                    for (vector<pair<int, int> >::iterator it = m[t].begin(); it != m[t].end(); it++)
                    {
                        if ((*it).first <= j)
                            continue;

                        int nt = ans.size();
                        if (nt > 0 && (ans.back())[0] == nums[i] && (ans.back())[1] == nums[j] && (ans.back())[2] == nums[(*it).first] && (ans.back())[3] == nums[(*it).second])
                            continue;

                        vector<int> vt;
                        vt.push_back(nums[i]);
                        vt.push_back(nums[j]);
                        vt.push_back(nums[(*it).first]);
                        vt.push_back(nums[(*it).second]);
                        ans.push_back(vt);
                    }
                }
            }
        }
        return ans;
    }
};
