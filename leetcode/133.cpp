// Be careful about the grammar, because we don't have the compiler when writing the code in Google Doc or a white board.
// Just be careful where to use '.' and where '->', especially when we use a map and pointers together.

// DFS
class Solution {
public:
    map<UndirectedGraphNode*, UndirectedGraphNode*> m;

    void DFS(UndirectedGraphNode *newNode, UndirectedGraphNode *oldNode)
    {
        if (oldNode == NULL)
            return;
        for (int i = 0; i < oldNode->neighbors.size(); i++)
        {
            bool visited = true;
            if (m.find( oldNode->neighbors[i] ) == m.end() )
            {
                UndirectedGraphNode* t = new UndirectedGraphNode( oldNode->neighbors[i]->label);
                m[ oldNode->neighbors[i] ] = t;
                visited = false;
            }
            newNode->neighbors.push_back(m[ oldNode->neighbors[i] ]);
            if (! visited)
                DFS( m[ oldNode->neighbors[i] ], oldNode->neighbors[i]);
        }
        return;
    }

    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
        if (node == NULL)
            return NULL;
        m[node] = new UndirectedGraphNode( node->label );
        DFS( m[node], node);
        return m[node];
    }
};

// BFS
/**
 * Definition for undirected graph.
 * struct UndirectedGraphNode {
 *     int label;
 *     vector<UndirectedGraphNode *> neighbors;
 *     UndirectedGraphNode(int x) : label(x) {};
 * };
 */
class Solution {
public:
    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
        if (node == NULL)
            return NULL;

        map< UndirectedGraphNode*, UndirectedGraphNode*> m;
        queue<UndirectedGraphNode*> q;
        queue<UndirectedGraphNode*> newQ;

        q.push(node);
        m[node] = new UndirectedGraphNode( node->label );
        newQ.push( m[node] );

        while (! q.empty() )
        {
            UndirectedGraphNode *oldNode = q.front();
            q.pop();
            UndirectedGraphNode *newNode = newQ.front();
            newQ.pop();

            for (int i = 0; i < oldNode->neighbors.size(); i++)
            {
                bool visited = true;
                if (m.find( oldNode->neighbors[i] ) == m.end() )
                {
                    visited = false;
                    m[ oldNode->neighbors[i] ] = new UndirectedGraphNode( oldNode->neighbors[i]->label );
                }
                newNode->neighbors.push_back( m[ oldNode->neighbors[i] ] );
                if (! visited)
                {
                    q.push( oldNode->neighbors[i] );
                    newQ.push( m[ oldNode->neighbors[i] ] );
                }
            }
        }

        return m[node];
    }
};
