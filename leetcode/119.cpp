// Note that [1, 3, 3, 1] is the 3rd row, that means [1] is the 0th row.

class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> ans;
        ans.push_back(1);
        if (rowIndex == 0)
            return ans;

        ans.push_back(1);
        for (int i = 2; i <= rowIndex; i++)
        {
            int oldt = 1, newt = 1;
            for (int j = 1; j < i; j++)
            {
                // Here we need two temp variable, one for the ans[j-1] of the last row, one for the ans[j] of the last row
                newt = ans[j];
                ans[j] = oldt + ans[j];
                oldt = newt;
            }
            ans.push_back(1);
        }
        return ans;
    }
};
