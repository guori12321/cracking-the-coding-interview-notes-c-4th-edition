class Solution {
public:
    // The string variable is s, so here we cannot use s anymore.
    vector<string> st;
    vector<vector<string>> ans;
    char hash[1000][1000] = {};

    bool check(string s, int l, int r)
    {
        if (l >= r)
            return true;
        if (hash[l][r] != 0)
            if (hash[l][r] == 1)
                return true;
            else
                return false;
        if (s[l] == s[r] && check(s, l+1, r-1))
        {
            hash[l][r] = 1;
            return true;
        }
        else
        {
            hash[l][r] = 2;
            return false;
        }
    }

    void DFS(string s, int l)
    {
        int n = s.size();
        if (l > n)
            return;
        if (l == n)
        {
            ans.push_back(st);
            return;
        }
        for (int i = l; i < n; i++)
            if ( check(s, l, i) )
            {
                st.push_back(s.substr(l, i-l+1) );
                DFS(s, i+1);
                // The parameter for erase() is a itor rather than a int index, so just call st.begin().
                st.erase(st.begin() + st.size() - 1);
            }
        return;
    }

    vector<vector<string>> partition(string s) {
        for (int i = 0; i < s.size(); i++)
            hash[i][i] = 1;
            DFS(s, 0);
        return ans;
    }
};
