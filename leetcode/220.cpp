// Reference: http://www.cnblogs.com/easonliu/p/4544073.html
class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
        // k == 0 is the special corner case
        if (k == 0 || t < 0)
            return false;

        multiset<long long> m;
    	int n = nums.size();

        for (int i = 0; i < n; i++)
        {
            if (m.size() == k+1)
                // erase only one element here. m.erase(nums[i-k-1]) would erase all the elements of the same value
                m.erase( m.find(nums[i-k-1]) );
            // lower_bound(t) returns the iterator that is equal or larger to t
            // up_bound(t) return larger.
            auto it = m.lower_bound(nums[i] - t);
            if (it != m.end() && *it - nums[i] <= t)
                return true;
            m.insert(nums[i]);
        }

        return false;
    }
};
