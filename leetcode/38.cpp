class Solution {
public:
    string countAndSay(int n) {
        string s = "1";
        for (int i = 1; i < n; i++)
        {
            string t = "";
            int j = 0;
            int n = s.size();
            while (j < n )
            {
                int k = j+1;
                while (k < n && s[k] == s[j])
                    k++;
                int count = k - j;
                // int to string, just use to_string(int)
                t += to_string(count) + s[j];
                j = k;
            }
            s = t;
        }

        return s;
    }
};
