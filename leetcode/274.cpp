// As the hint says, we can use more memory to speedup our algorithm.
class Solution {
public:
    int hIndex(vector<int>& citations) {
        int n = citations.size();
        vector<int> count(n+1, 0);

        for (int i = 0; i < n; i++)
            count[ min(n, citations[i]) ]++;

        int tot = n, ans = 0;
        for (int i = 0; i <= n; i++)
        {
            if (tot >= i)
                ans = i;
            tot -= count[i];
        }

        return ans;
    }
};
