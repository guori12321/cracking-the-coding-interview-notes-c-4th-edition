// The description is not clear, so I need to guess lots of times.
// * means the preceding (not proceeding) or previous one char can show 0 time, once or more times.
// But .* means empty string, .*, .*.* ... Which means any string. In the .*.* example the first and second . can be different chars.
//
// So we need to detect if the next char in the pattern string is *.
// A brief and clear solution is given in http://www.cnblogs.com/grandyang/p/4461713.html
class Solution {
public:
    int f[1000][1000];

    bool myisMatch(string& s, string& p, int ls, int lp) {
        int np = p.size();
        int ns = s.size();

        if (f[ls][lp] != 0)
        {
             if (f[ls][lp] == 1)
                return true;
             return false;
        }

        if (ns == ls && np == lp)
        {
            f[ls][lp] = 1;
            return true;
        }

        if (lp+1 < np && p[lp+1] == '*')
            if (myisMatch(s, p, ls, lp+2) )
            {
                f[ls][lp] = 1;
                return true;
            }


        int i = lp;
            if (p[i] != '*' && p[i] != '.' && p[i] == s[ls] && myisMatch(s, p, ls+1, i+1) )
            {
                f[ls][lp] = 1;
                return true;
            }
            else if (p[i] == '*')
                {
                    if ( myisMatch(s, p, ls, i+1) )
                    {
                        f[ls][lp] = 1;
                        return true;
                    }
                    if ( s[ls]==p[lp-1] || p[lp-1] == '.')
                    {
                        if ( myisMatch(s, p, ls+1, i+1) )
                        {
                            f[ls][lp] = 1;
                            return true;
                        }
                        for ( int t = ls+1; t < ns && (s[t] == s[t-1] || p[lp-1] == '.'); t++ )
                            if ( myisMatch(s, p, t+1, i+1) )
                            {
                                f[ls][lp] = 1;
                                return true;
                            }
                    }
                }
            else if (p[i] == '.' && myisMatch( s, p, ls+1, i+1))
            {
                f[ls][lp] = 1;
                return true;
            }

        f[ls][lp] = -1;
        return false;
    }

    bool isMatch(string s, string p) {
        int ns = s.size();
        int np = p.size();
        for (int i = 0; i < ns; i++)
            for (int j = 0; j < np; j++)
                f[i][j] = 0;
        return myisMatch(s, p, 0, 0);
    }


};
