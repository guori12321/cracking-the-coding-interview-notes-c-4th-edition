class Solution(object):
    def simplifyPath(self, path):
        dirs = path.split('/')
        s = ['/']
        for d in dirs:
            if d == '':
                continue
            elif d == '.':
                continue
            elif d == '..':
                if len(s) == 0:
                    s.append('/')
                elif len(s) == 1:
                    continue
                else:
                    s = s[:-1]
            else:
                s.append(d)
        # The first char is always '/'
        ans = "/"
        for d in s[1:]:
            ans += d+'/'
        # If we have at least one d added to s, then we would have '/' at the end, otherwise there would be just '/'
        if len(ans) > 1:
            return ans[:-1]
        return ans
