class Solution {
public:
    bool isPowerOfTwo(int n) {
        // Note that 0 is NOT a power of zero
        // In this problem we assume negative numbers are not a power of 2
        if (n <= 0)
            return false;
        // == is conducted before &, so, here we need braces.
        while ( (n & 1) ==  0)
            n = n >> 1;
        if (n == 1)
            return true;
        return false;
    }
};
