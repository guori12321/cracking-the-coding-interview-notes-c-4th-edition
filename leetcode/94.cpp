// Recursively
class Solution {
public:
	vector<int> ans;
	void inorderTra(TreeNode* root)
	{
		if (root == NULL)
			return;
		inorderTra(root->left);
		ans.push_back(root->val);
		inorderTra(root->right);
		return;
	}
    vector<int> inorderTraversal(TreeNode* root) {
        	inorderTra(root);
	return ans;
    }
};

// Iteratively. Just to check if we've visited the left child
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
    vector<int> ans;
    stack<TreeNode*> s;
    s.push(root);
    while (s.size() > 0)
    {
        TreeNode *t = s.top();
        s.pop();

        if (t == NULL)
            continue;
        if (t->left == NULL || t->left->val == -99999)
        {
            ans.push_back(t->val);
            t->val = -99999;
            s.push(t->right);
        }
        else
        {
            s.push(t);
            s.push(t->left);
        }
    }
    return ans;
    }
};

