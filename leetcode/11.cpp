// Tricky one.
//
// First, let's prove that the two lines are on the two sides of the highest line. It's simple: if the two lines are on one side, then we can replace one with the highest one and get a better result
// Next, with x grows, we can assume that height of the lines at the left side of the highest line grows. If there is one that is shorter than its left neighbor, then the left one is better and we can discard the shorter one. On the right side, the height decreases.
// So, we can set two pointers, one at the first line, one at the last. As in the water container only the shorter line matters, so we move the short line pointer closer to the highest line to find a higher line.
// And then update our answer if necessary.
// The program ends when the two pointers meet.
class Solution {
public:
    int maxArea(vector<int>& height) {
        int leftH = 0, rightH = 0;
        int n = height.size();
        int l = 0, r = n - 1;
        int ans = 0;
        while (l < r)
        {
            // Note l cannot be n, so here is l < n-1
            while (l < n - 1 && height[l] < leftH)
                l++;
            while (r > 0 && height[r] < rightH)
                r--;
            leftH = height[l];
            rightH = height[r];
            if (l < r)
                ans = max(ans, min(leftH, rightH) * (r - l) );
            if (leftH < rightH)
                l++;
            else
                r--;
        }
        return ans;
    }
};
