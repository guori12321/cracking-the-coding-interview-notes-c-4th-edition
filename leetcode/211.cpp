// A practice on trie.
class Trie{
public:
    Trie *next[26];
    bool end;
    Trie()
    {
        for (int i = 0; i < 26; i++)
            next[i] = NULL;
        end = false;
    }
};

class WordDictionary {
public:
    WordDictionary()
    {
        root = new Trie();
    }

    Trie *root;
    // Adds a word into the data structure.
    void addWord(string word) {
        Trie *t = root;
        for (int i = 0; i < word.size(); i++)
        {
            if (t->next[ word[i] - 'a'] == NULL)
            {
                t->next[ word[i] - 'a'] = new Trie();
                if (i == word.size() - 1)
                    t->next[ word[i] - 'a']->end = true;
            }
            t = t->next[ word[i] - 'a'];
        }
    }

    bool DFS(string word, Trie *root)
    {
        if (word.size() == 0 && root == NULL)
            return true;
        if (word.size() == 0 || root == NULL)
            return false;

        if (word.size() == 1)
        {
            if (word[0] != '.')
                return root->next[word[0] - 'a'] != NULL && root->next[word[0] - 'a']->end;
            else
                for (int i = 0; i < 26; i++)
                    if (root->next[i] != NULL && root->next[i]->end)
                        return true;
            return false;
        }

        if (word[0] != '.')
            return DFS(word.substr(1, word.size() - 1), root->next[ word[0] - 'a']);
        else
        {
            for (int i = 0; i < 26; i++)
                if (DFS(word.substr(1, word.size() - 1), root->next[i]) )
                    return true;
            return false;
        }
        return false;
    }

    // Returns if the word is in the data structure. A word could
    // contain the dot character '.' to represent any one letter.
    bool search(string word) {
        Trie *t = root;
        return DFS(word, root);
    }
};
