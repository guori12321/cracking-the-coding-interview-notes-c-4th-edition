class Solution {
public:
    void connect(TreeLinkNode *root) {
        // Remember to check root->left
        while (root != NULL && root->left != NULL)
        {
            TreeLinkNode *t = root;
            root = root->left;

            while (t != NULL)
            {
                t->left->next = t->right;
                if (t->next != NULL)
                    t->right->next = t->next->left;
                t = t->next;
            }
        }
        return;
    }
};

