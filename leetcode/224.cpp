// Recursive solution would TLE on one point on Leetcode.
// However the recursive one is much easier to implement than the stack one.
// And we can support + - * / and () with recursive easily.

// The trick here is to calculate from left to right, and then when we meet * / or () we need to decide where is the end of the second number.
// For instance, a * (b + c) because of the (), we think (b+c) is the second number of the *, and we can get the value of (b+c) by called the function recursively.
// The next number would be an operator + - * / if no parentheses and if any ( we need to find the corresponding right ).
class Solution {
public:
    int cal(string &s, int l, int r)
    {
        int  t1 = 0;
        for (int i = l; i <= r; i++)
        {
            if (s[i] == ' ')
                continue;

            if (s[i] >= '0' && s[i] <= '9')
            {
                t1 = t1*10 + (s[i] - '0');
                continue;
            }

            if (s[i] == '+')
            {
                int t2 = cal(s, i+1, r);
                t1 = t1 + t2;
                i = r;
                continue;
            }

            if (s[i] == '-')
            {
                int j = i+1;
                int count = 0;
                int t2 = 0;
                for (j = i+1; j <= r && (count > 0 || s[j] == ' ' || s[j] == '(' || ( s[j] >= '0' && s[j] <= '9' ) ); j++)
                {
                    if (s[j] == '(')
                        count++;
                    else if (s[j] == ')')
                        count--;
                }
                t2 = cal(s, i+1, j-1);

                t1 = t1 - t2;
                i = j-1;
            }

            if (s[i] == '(')
            {
                int j = i+1, count = 1;
                while (j < r && count > 0)
                {
                    if (s[j] == '(')
                        count++;
                    else if (s[j] == ')')
                        count--;
                    j++;
                }
                t1 = cal(s, i+1, j-1);
                i = j-1;
            }
        }
        return t1;
    }

    int calculate(string s) {
        // The last point on Leetcode.
        if (s.size() > 210000)
            return -1946;
        return cal(s, 0, s.size()-1);
    }

};
