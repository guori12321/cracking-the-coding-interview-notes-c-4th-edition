class Solution {
public:
    int DFS(vector<int>& nums, int l, int r)
    {
        int n = nums.size();
        if (r < 0 || l >= n || l > r)
            return 99999999;
        // if l == r, then nums[l] == nums[r]
        if (nums[l] <= nums[r])
            return nums[l];
        int m = (l + r) / 2;
        return min(  DFS(nums, l, m), DFS(nums, m+1, r) );
    }
    int findMin(vector<int>& nums) {
        return DFS(nums, 0, nums.size() - 1);
    }
};

