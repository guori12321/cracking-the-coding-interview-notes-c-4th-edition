// It is not as complicated as I once thought
// Just be clear in your mind
//
// We enumerate the root at the root level and the child $t$ in the child level of the root
// Via the next pointer, we find the first root at the root level that has a child, and then, if this child is not $t$ (at the beginning this child would be $t$ exactly, otherwise it is not), then it's $t$'s next, and we update t->next and move t to t->next.
// And we move root to root->next until root is NULL, then we move root to the first child of the next level.
class Solution {
public:
    TreeLinkNode* getFirstChild(TreeLinkNode *root)
    {
        while (root != NULL)
        {
            if (root->left != NULL)
                return root->left;
            if (root->right != NULL)
                return root->right;
            root = root->next;
        }
        return NULL;
    }
    void connect(TreeLinkNode *root) {
        while (root != NULL && getFirstChild(root) != NULL)
        {
            TreeLinkNode* firstChild = getFirstChild(root);
            while (root != NULL && root->left == NULL && root->right == NULL)
                root = root->next;
            TreeLinkNode* t = firstChild;

            while (root != NULL)
            {
                if (root->left != NULL && root->left != t)
                {
                    t->next = root->left;
                    t = t->next;
                }
                if (root->right != NULL && root->right != t)
                {
                    t->next = root->right;
                    t = t->next;
                }
                root = root->next;
            }

            root = firstChild;
        }
   }
};
