// It is obviously a searching problem. Instead of DFS, we can do it much faster with BFS.
// However there are lots of details to care about.
// At the beginning I maintained a hash table to maintain the minimal steps to go from the beginWord to the words in wordDict, however I got TLE.
// Later instead of the hash table, in this problem the first time we find a word in wordDict we get the minimal steps to this word, so we can remove the hash table and maintain the minimal steps in the queue using a pair <string, int>. And still TLE. So I have no idea to optimize it.
// I found one reference http://www.programcreek.com/2012/12/leetcode-word-ladder/ and the only difference here is it enumerates all the possible string that has one char different from the current word, and then query if such a new word is in the wordDict or not.
// In this problem the size of wordDict is very large but the length of words is small, so such a enumerate method works.
class Solution {
    public:
        int ladderLength(string beginWord, string endWord, unordered_set<string>& wordDict) {
            if ( beginWord.size() == 0)
                return 0;

            queue<pair<string, int> > q;
            q.push(make_pair(beginWord, 1));

            while (! q.empty() )
            {
                pair<string, int> t = q.front();
                q.pop();

                if (t.first == endWord)
                    return t.second;

                for (int i = 0; i < t.first.size(); i++)
                {
                    string newT = t.first;
                    for (char c = 'a'; c <= 'z'; c++)
                    {
                        newT[i] = c;
                        if (wordDict.find(newT) != wordDict.end())
                        {
                            q.push( make_pair(newT, t.second+1) );
                            wordDict.erase(newT);
                        }
                    }
                }
            }
            return 0;
        }
};
