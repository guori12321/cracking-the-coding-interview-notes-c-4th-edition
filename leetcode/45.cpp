// Be extremely careful about the special cases, just like only one element.

class Solution {
public:
    int jump(vector<int>& nums) {
        int f[1000000] = {0};

        int n = nums.size();
        // We are already on the first element
        f[0] = 0;
        // We need to use an end pointer to speedup our algorithm.
        int end = 0;
        // end < n-1 that means if n is 1 then we return 0 directly
        for (int i = 0; i < n && end < n-1; i++)
            {
                // Note that j can be nums[i] + i so <= rather than <
                for (int j = end; j < n && j <= nums[i] + i; j++)
                    if (j > 0 && f[j] == 0)
                        f[j] = f[i] + 1;
                    else
                        f[j] = min(f[j], f[i] + 1);
                end = max(end, nums[i] + i);

                if (f[n-1] != 0)
                    return f[n-1];
            }
        return f[n-1];
    }
};

