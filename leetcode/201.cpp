// The idea is simple: if m != n, then we shift them 1 bit to the right side, until they are the same, and then we shift them left with the right part filled with 0.
class Solution {
public:
    int rangeBitwiseAnd(int m, int n) {
        int k = 0;
        while (m != n)
        {
            m = m >> 1;
            n = n >> 1;
            k++;
        }
        return m << k;
    }
};

// If we only detect the range of n-m to get how many bits would flip, then we would get WA in the following case.
// Input:
// 2, 4
// Output:
// 2
// Expected:
// 0
class Solution {
public:
    int rangeBitwiseAnd(int m, int n) {
        if (m == n)
            return m;

        // int would overflow.
        long long t = 2;
        int k = 1;
        while (t < n-m)
        {
            t = t << 1;
            k++;
        }

        int ans = m;
        ans = (ans >> k) << k;

        return ans;
    }
};
