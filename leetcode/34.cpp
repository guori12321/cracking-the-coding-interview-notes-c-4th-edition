class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        int n = nums.size();
        vector<int> ans;
        ans.push_back(-1);
        ans.push_back(-1);
        if (n == 0)
            return ans;

        int l = 0, r = n - 1;
        while (l <= r)
        {
            int m = (l+r) / 2;
            if (nums[m] == target)
                break;
            if (nums[m] < target)
                l = m + 1;
            else
                r = m - 1;
        }
        if (l > r)
            return ans;

        // As we may only have one target in the nums, here we should not let lr = (l+r) / 2 - 1, otherwise there may be no target in the left area
        //int ll = l, lr = (l+r) / 2 - 1;
        int ll = l, lr = (l+r) / 2;
        while (ll <= lr)
        {
            int m = (ll + lr) / 2;
            if (nums[m] == target && nums[m-1] < target)
            {
                ans[0] = m;
                break;
            }
            if (nums[m] == target)
                lr = m - 1;
            else
                ll = m + 1;
        }
        if (ans[0] == -1)
            ans[0] = 0;

        int rl = (l+r) / 2;
        int rr = r;
        while (rl <= rr)
        {
            int m = (rl + rr) / 2;
            if (nums[m] == target && nums[m+1] > target)
            {
                ans[1] = m;
                break;
            }
            if (nums[m] == target)
                rl = m + 1;
            else
                rr = m - 1;
        }
        if (ans[1] == -1)
            ans[1] = n-1;
        return ans;
    }
};
