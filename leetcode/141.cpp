class Solution {
public:
    bool hasCycle(ListNode *head) {
        if (head == NULL)
            return false;
        ListNode *p = head, *q = head;
        // We cannot judge if p == q here because their initial value is the same
        while (p != NULL && q != NULL)
        {
            p = p->next;
            if (q->next == NULL)
                return false;
            q = q->next->next;
            if (p == q)
                return true;
        }
        return false;
    }
};
