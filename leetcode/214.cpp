// KMP: reference https://github.com/haoel/leetcode/blob/master/algorithms/shortestPalindrome/ShortestPalindrome.cpp
// http://pastebin.com/dk2xjL50
//
//
// I still don't understand KMP.
class Solution {
public:
    string shortestPalindrome(string s) {
        int n = s.size();
        if (n <= 1)
            return s;

        string mirror = s + "#" + string(s.rbegin(), s.rend());

        vector<int> f(mirror.size(), 0);

        for (int i = 1; i < mirror.size(); i++)
        {
            int j = f[i-1];
            while (j > 0 && mirror[j] != mirror[i])
                j = f[j-1];
            if (mirror[i] == mirror[j])
                j++;
            f[i] = j;
        }
        string prefix = s.substr(f.back(), n-f.back());
        return string(prefix.rbegin(), prefix.rend()) + s;
    }
};

// Brute-force one. TLE
class Solution {
public:
    string shortestPalindrome(string s) {
        int n = s.size();
        if (n <= 1)
            return s;

        for (int preLen = 0; preLen <= n-1; preLen++)
        {
            int l = 0, r = n-1 - preLen;
            bool flag = true;
            while (l < r)
            {
                if (s[l] != s[r])
                {
                    flag = false;
                    break;
                }
                l++;
                r--;
            }

            if (flag)
            {
                string pre = s.substr( n - preLen, preLen);
                reverse(pre.begin(), pre.end() );
                return pre+s;
            }
        }

        return "-----";
    }
};



