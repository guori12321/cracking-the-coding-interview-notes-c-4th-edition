class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> ans;
        int n = numRows;
        // I don't think n is a legal input, however here in Leetcode it is.
        if (n == 0)
            return ans;
        for (int i = 0; i < numRows; i++)
        {
            vector<int> vt;
            vt.push_back(1);
            ans.push_back(vt);
        }

        if (numRows == 1)
            return ans;

        ans[1].push_back(1);
        for (int i = 2; i < n; i++)
        {
            // The last j++ is always inputed as i++...
            for (int j = 1; j < i; j++)
                ans[i].push_back(ans[i-1][j-1] + ans[i-1][j]);
            ans[i].push_back(1);
        }
        return ans;
    }
};
