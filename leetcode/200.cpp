class Solution {
public:
    bool visited[500][500] = {{0}};
    int m = 0, n = 0;
    int ans = 0;

    void DFS (vector< vector<char> >& grid, int x, int y)
    {
        // Note that x >= m rather than x > m!!!
        if (x < 0 || x >= m || y < 0 || y >= n)
            return;
        if (visited[x][y] || grid[x][y] != '1')
            return;
        visited[x][y] = true;
        DFS(grid, x-1, y);
        DFS(grid, x+1, y);
        DFS(grid, x, y+1);
        DFS(grid, x, y-1);
        return;
    }

    int numIslands(vector<vector<char> >& grid) {
        m = grid.size();
        if (m == 0)
            return 0;
        // Don't use int n here because we want to modify the global one
        n = grid[0].size();
        if (n == 0)
            return 0;
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
            {
                if (!visited[i][j] && grid[i][j] == '1')
                {
                    ans++;
                    DFS(grid, i, j);
                }
            }
        return ans;
    }
};
