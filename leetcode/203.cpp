class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        ListNode* vh = new ListNode(-99999);
        vh->next = head;
        ListNode* t = vh;

        while (t->next != NULL)
            if (t->next->val == val)
                t->next = t->next->next;
            else
                t = t->next;

        return vh->next;
    }
};
