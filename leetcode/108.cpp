class Solution {
public:
    TreeNode* DFS(vector<int> & nums, int l, int r)
    {
        if ((l > r) || (l < 0) || (r >= nums.size()) )
            return NULL;
        int m = (l+r) / 2;
        TreeNode* root = new TreeNode(nums[m]);
        root->left = DFS( nums, l, m-1);
        root->right = DFS( nums, m+1, r);
        return root;
    }
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        return DFS(nums, 0, nums.size() - 1);
    }
};
