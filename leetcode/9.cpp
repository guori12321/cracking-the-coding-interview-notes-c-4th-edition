class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0)
            return false;
        if (x < 9)
            return true;

        long long t = 1;
        while (x / t > 0)
        t *= 10;
        t /= 10;

        while (t > 1)
        {
            if (x % 10 != x / t)
                return false;
            // erase the first and last digit of x
            x = x % t;
            x /= 10;
            // As we erase two digits at the same time, so t should be divided by 100 rather than 10
            t /= 100;
        }
        return true;
    }
};
