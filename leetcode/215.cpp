// The find-kth-largest is a well known O(n) algorithm.
// It is a highly related to quick sort.
// Unlike quick sort, here it is not necessary to sort recursively.
class Solution {
public:
int findKthLargest(vector<int>& nums, int k) {
    k = nums.size() - k;
    int left = 0, right = nums.size() - 1;
    while (left <= k && right >= k)
    {
        int l = left, r = right;
        int m = nums[(l + r) / 2];
        while (l <= r)
        {
            while (nums[l] < m)
                l++;
            while (nums[r] > m)
                r--;
            if (l <= r)
            {
                int t = nums[l];
                nums[l] = nums[r];
                nums[r] = t;
                l++;
                r--;
            }
        }

        if (k <= r)
            right = r;
            else if (k >= l)
                left = l;
            else
            {
                return nums[k];
                break;
            }
    }
    return nums[k];
}
};

// The following is the quick sort
#include <iostream>
#include <vector>
using namespace std;

void qsort(vector<int> & v, int left, int right)
{
    if (left >= right)
        return;

    int l = left, r = right;
    int m = v[(l+r)/ 2];
    // Why we don't stop at l < r: Because we need to push the boundary of the sorting area every time (we increase left or reduce right).
    // Otherwise the boundary will be the same when we call the quick sort recursively, and there will be infinite loop.
    while (l <= r)
    {
        // Why we don't stop when v[l] == m: If we swap m to the right side, then, when we meet m at the right side we will stop, otherwise would not stop until the end of this array.
        // Because of this, quick sort is unstable sorting algorithm.
        while (v[l] < m)
            l++;
        while (v[r] > m)
            r--;
        // Here, if l == r, though it is needless to swap v[l] and v[r], we still need to l++ and r--.
        if (l <= r)
        {
            int t = v[l];
            v[l] = v[r];
            v[r] = t;
            l++;
            r--;
        }
    }
    // Here we don't use r-1 because the v[r] is NOT sorted.
    qsort(v, left, r);
    qsort(v, l, right);
    return;
}

int main()
{
    vector<int> v;
    for (int i = 0; i < 100; i++)
        v.push_back(rand() % 100);
    qsort(v, 0, v.size()-1);

    for (int i = 0; i < v.size(); i++)
        cout << v[i] << "\t";
    cout << endl;
    return 0;
}
