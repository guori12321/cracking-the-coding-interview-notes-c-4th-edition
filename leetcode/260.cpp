// There are two basic methods to deal with such questions, one is to use XOR, another is to count how many numbers have 1 value in a specific bit.
// In this question, if we run XOR, then we would get a xor b where a and b are the final answer. So, let's get a xor b first and then consider the bit count method. As a != b, so a xor b != 0, and we know a and b are different in at least one bit.
// So, we find one bit that a and b are different in and put all the numbers in two groups whose this bit is 1 and 0.
// And then, a and b are in different group and so we get the question that find the number that shows only once while all the others show twice.
class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int ans0 = 0, ans1 = 0, ansx = 0;
        int n = nums.size();
        for (int i = 0; i < n; i++)
            ansx ^= nums[i];
        unsigned int mask = 1;
        // Note that & has a higher priority than ==, that means we need () here.
        while ( (mask & ansx) == 0)
            mask <<= 1;
        for (int i = 0; i < n; i++)
            if ( (nums[i] & mask) == 0)
                ans0 ^= nums[i];
            else
                ans1 ^= nums[i];
        vector<int> ans;
        ans.push_back(ans0);
        ans.push_back(ans1);
        return ans;
    }
};
