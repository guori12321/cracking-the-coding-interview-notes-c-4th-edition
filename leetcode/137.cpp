// Very tricky.
// Count the number of 1 in every bit, and then mod 3 and we will get if this bit is 1 in the answer
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int n = nums.size();
    unsigned int ans = 0;
    for (int i = 0; i < sizeof(int) * 8; i++)
    {
        int count = 0;
        for (int j = 0; j < n; j++)
            if ( (nums[j] >> i) & 1 == 1)
                count++;
        if (count % 3 ==1)
            ans |= ( 1 << i);
    }
    return ans;
    }
};
