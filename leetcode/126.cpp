// I spent 2 days on this problem...

// We can do a DFS with limited depth from 0 to where we find the answer. However we would TLE.
// Another method would be BFS to get the depth and then DFS, but I highly doubt that we would TLE as well.
// So the last method is BFS while recording the previous node in the searching tree, and then generate the entire answer with the record of previous nodes. Note that the answer generation is just a recursive program rather than sorting: it is just find all the paths given the tree rather than generate the tree itself.

// A trick here is to use two queue (in fact, two sets) one by one, so we don't need to record the number of steps in the queue. And the total depth of the tree is not necessary when we generate the answer: as the leaf nodes are all the end word, every path would be one of the answers.
// And we delete nodes when a level is searched rather than delete it when we reach it, because there are many shortest paths.

// My two days were spent on a little mistake commented in the code
class Solution {
    public:
        vector<vector<string> > ans;
        vector<string> st;
        int level = 0, maxLevel = 0;
        unordered_set<string> ansPre;
        unordered_set<string> visited;
        unordered_map<string, unordered_set<string> > pre;

        // Generate the answers given the previous nodes
        void search(string& start)
        {
            string t = st.back();

            if (t == start)
            {
                vector<string> tans;
                for (int i = st.size() -1; i >= 0; i--)
                    tans.push_back(st[i]);
                ans.push_back(tans);
                // We shouldn't erase here, instead we do it in the follosing code
                //st.erase(st.end() - 1);
                return;
            }

            for (unordered_set<string>::iterator it = pre[t].begin(); it != pre[t].end(); it++)
            {
                if (visited.find(*it) == visited.end())
                {
                    st.push_back(*it);
                    visited.insert(*it);
                    search(start);
                    visited.erase(*it);
                    st.erase(st.end() - 1);
                }
            }
            return;
        }

        vector<vector<string> > findLadders(string start, string end, unordered_set<string> &dict)
        {
            if (start == "")
                return ans;

            vector<unordered_set<string> > q(2);
            bool current = 0;
            bool previous = 1;
            bool findAns = false;
            q[current].insert(start);

            while (!findAns)
            {
                current = !current;
                previous = !previous;

                // Those nodes are to be searched in the future. So here we remove them from the dict before the search, rather than after, otherwise two nodes in the same level may point to each other and we would spend much more time on generating the result because we need to record the visited and unvisited nodes and the depth of the current DFS tree, and the tree may be exponentially larger.
                for (unordered_set<string>::iterator it = q[previous].begin(); it != q[previous].end(); it++)
                    dict.erase(*it);
                q[current].clear();

                maxLevel++;
                for (unordered_set<string>::iterator it = q[previous].begin(); it != q[previous].end(); it++)
                {
                    string t = *it;

                    for (int i = 0; i < t.size(); i++)
                    {
                        string newT = t;
                        for (int c = 'a'; c <= 'z'; c++)
                        {
                            newT[i] = c;
                            if (newT == t)
                                continue;

                            if (newT == end)
                            {
                                ansPre.insert(t);
                                findAns = true;
                                break;
                            }
                            if (!findAns && dict.find(newT) != dict.end())
                            {
                                pre[newT].insert(t);
                                q[current].insert(newT);
                            }
                        }
                    }
                }
                if (findAns || q[current].size() == 0)
                    break;
            }

            visited.clear();
            st.push_back(end);
            for (unordered_set<string>::iterator it = ansPre.begin(); it != ansPre.end(); it++)
            {
                st.push_back(*it);
                visited.insert(*it);
                search(start);
                st.erase(st.begin() + 1, st.end());
                visited.erase(*it);
            }
            return ans;
        }
};
