// Recursively
class Solution {
public:
    vector<int> ans;
    void preTravel(TreeNode* root)
    {
        if (root == NULL)
            return;
        ans.push_back(root->val);
        preTravel(root->left);
        preTravel(root->right);
    }
    vector<int> preorderTraversal(TreeNode* root) {
        preTravel(root);
    return ans;
    }
};

// For the iteration version, we need to claim a stack manually.
class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
    stack<TreeNode*> s;
    vector<int> ans;
    s.push(root);
    while (s.size() > 0)
    {
        TreeNode* t = s.top();
        s.pop();

        if (t != NULL)
        {
            ans.push_back(t->val);
            s.push(t->right);
            s.push(t->left);
        }
    }
    return ans;
    }
};
