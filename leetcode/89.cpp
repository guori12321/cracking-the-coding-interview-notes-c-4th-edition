class Solution {
public:
    vector<int> grayCode(int n) {
        vector<int> ans;
        // Discuss with the interviewer what should we return if n == 0. Here we return [0]
        ans.push_back(0);
        if ( n == 0)
            return ans;
        ans.push_back(1);
        for (int i = 1; i < n; i++)
        {
            // the size is changing when we add new elements
            int t = ans.size();
            // Do it in reverse order
            for (int j = t-1; j >= 0; j--)
                ans.push_back( ans[j] | ( 1 << i));
        }
        return ans;
        }
};
