// First, read the problem description carefully. At the beginning I thought we just want to find something like 1, 2, 3, 4 rather than in a disordered order.
//
// My naive idea is, so long as we want a O(n) complexity, we cannot sort it because the best sorting algorithm would need a O(n logn) time.
// So, for such a question, we would need more buffer space to save time. One possible and popular way is to use a hash bitmap. If the range of the numbers in $nums$ is small, for instance, the numbers are always from 1 to 100000, then we can just initial a all-zero bitmap and set the corresponding bits of every number in nums to 1, then, we traverse the bitmap and will get the largest consequent sequence.
// However, as there is one test case [2147483646,-2147483647,0,2,2147483644,-2147483645,2147483645], that means, the range is very large and our method cannot work in this way: it would take a very large space and time to traverse all the integers in the range of ints.
// The bitmap code is shown below. The range is -10000 to + (100000-1).

class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        bool hash[200000] = {};
        int base = 100000;
        int n = nums.size();
        for (int i = 0; i < n; i++)
            hash[nums[i] + base] = 1;
        int ans = 0, count = 0;
        int i = 0;
        while (i < base * 2)
        {
            if (hash[i])
            {
                int j = i+1;
                while (j < base*2 && hash[j])
                    j++;
                ans = max(ans, j-i);
                i = j;
            }
            i++;
        }
        return ans;
    }
};

// After a brief thought, I think there is no possible to compress the range of nums. There are cases that we can compress somehow but this one is not.
// So I just searched the solution online, and find a very easy and brief way to improve my method.
// It is NOT necessary to search from the head to tail of the bitmap. In stead of the bitmap, we use a set. We put all the numbers of nums in the set. Next, for each number, we try to find if its left and right neighbors are in the set.
// The worst case would be O(n^2). For instance, if the numbers are [1, 2, 3, 4, 5], for 1 we find if -1, 2, 3, 4, 5 are in the set, and for 2 we find all of 1, 2, 3, 4, 5 again. We waste time here because there are overlapping in the searching. So, just after we find a number in the set, we remove it from the set. Now, after we visit nums[0], for nums[1] to nums[4] they are all not in the set and would be skipped. So we can get a O(n) solution!

class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
    set<int> hash;
    int n = nums.size();
    for (int i = 0; i < n; i++)
        hash.insert(nums[i]);
    int ans = 0, count = 0;
    for (int i = 0; i < n; i++)
        if (hash.find(nums[i]) != hash.end())
        {
            hash.erase(nums[i]);
            int count = 1;
            int l = nums[i] - 1, r = nums[i] + 1;
            while (hash.find( l ) != hash.end() )
            {
                hash.erase(l);
                l--;
                count++;
            }
            while (hash.find( r ) != hash.end() )
            {
                hash.erase(r);
                r++;
                count++;
            }
            ans = max(ans, count);
        }
    return ans;
    }
};
