class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int m = matrix.size();
    if (m == 0)
        return false;
    int n = matrix[0].size();

    int l = 0, r = m - 1;
    int rowNum = -1;
    // if there is only one row, then the initial value of l and r would be both 0
    while (l <= r)
    {
        int mid = (l + r) / 2;
        if (matrix[mid][0] <= target && (mid == m-1 || matrix[mid+1][0] > target) )
        {
            rowNum = mid;
            break;
        }
        // Remember that we use mid here rather than m
        if (matrix[mid][0] < target)
            l = mid+1;
        else
            r = mid - 1;
    }
    if (rowNum < 0)
        return false;
    l = 0;
    // It's not a square, so the r = n-1 rather than m-1
    r = n - 1;
    while (l <= r)
    {
        int mid = (l+r)/2;
        if (matrix[rowNum][mid] == target)
            return true;
        if (matrix[rowNum][mid] > target)
            r = mid - 1;
        else
            l = mid + 1;
    }
    return false;
    }
};
