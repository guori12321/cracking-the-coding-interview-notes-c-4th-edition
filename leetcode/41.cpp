// At the beginning I have no idea about O(n). Enumerating the number from 1 to INT_MAX would take O(n^2) time and sorting would take O(n logn). As this problem asks for O(n) time and constant space, I think we can count the number of ints in a range, say if there are 5 numbers in the range from 1 to 5 then every number in 1 to 5 are in the array, however, as the numbers in the array are not unique (the examples are but the description doesn’t say anything about unique), we cannot do it this way.
// My last idea about O(n) time complexity is that, since the INT_MAX is always fixed, we can search the array range by range: if we have an array of 1000 boolean, then we search 1..1000 first to see if any number fits the range, and then 1001..2000 and so on. So we need to search INT_MAX / 1000 times, and it is indeed a constant. However the constant is too large and I doubt I cannot pass the OJ.
// The key in the answer is to avoid O(n) space complexity, and since we already have the input array, we can use its index for the key in the hash table. That’s it.
class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        int n = nums.size();
        if (n == 0)
            return 1;

        int l = 0, r = n - 1;
        while (l < r)
        {
            while (l < r && nums[l] > 0)
                l++;
            // the last condition is > 0, so here is <= 0. Be careful about == 0.
            while (l < r && nums[r] <= 0)
                r--;
            if (l < r)
            {
                int t = nums[l];
                nums[l] = nums[r];
                nums[r] = t;
                l++;
                r--;
            }
        }
        r = 0;
        while (r < n && nums[r] > 0)
            r++;
        n = r;

        for (int i = 0; i < n; i++)
        {
                // nums[i] may already be revered to negative, so here we judge the absolute value
                if (abs(nums[i]) > 0 && abs(nums[i]) <= n)
                    nums[ abs(nums[i])-1 ] = - abs(nums[ abs(nums[i])-1 ]);
        }
        for (int i = 0; i < n; i++)
            if (nums[i] > 0)
                return i+1;

        return n+1;
    }
};
