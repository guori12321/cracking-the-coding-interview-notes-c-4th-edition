// The math way is too tricky for me...In high school I did practice and tried to remember the formula, but I believe it is useless in real programming. And if necessary we can just google the formula.
class Solution {
public:
    int addDigits(int num) {
        while (num > 9)
        {
            int t = 0;
            while (num != 0)
            {
                t += num % 10;
                num /= 10;
            }
            num = t;
        }
        return num;
    }
};
