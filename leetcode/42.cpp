// As in such a bucket only the shorter line matters, so we first find a not strictly increasing sequence from left to right, and then, computing the amount of water between two items in the increasing sequence, next, we find an strictly increasing sequence from right to left and compute.
// Why such an algorithm don't compute the same water pool twice: in the pool it either has a higher or equally higher left boundary or a higher right boundary, and it's not possible to have a left higher than the right and the righter higher than left at the same time.

class Solution {
public:
    int trap(vector<int>& height) {
    int ans = 0;
    int n = height.size();
    if (n <= 2)
        return 0;

    int l = 0, r = l+1;
    while (l < n-1)
    {
        r = l + 1;
        while (r < n && height[r] < height[l])
            r++;
        if (r >= n)
            break;
        // We skip height[l]
        for (int i = l+1; i < r; i++)
        ans += height[l] - height[i];
        l = r;
    }

    r = n-1;
    while (r > 0)
    {
        l = r - 1;
        // in the l-- loop note that l can be 0, so the condition is l >= 0
        // And we need to deal with height[l] == height[r] once and only once
        while (l >= 0 && height[l] <= height[r])
            l--;
        if (l < 0)
            break;
        for (int i = r-1; i > l; i--)
            ans += height[r] - height[i];
        r = l;
    }
    return ans;
    }
};
