// Stack is a basic way to judge if the parentheses are valid.
// So we maintain the best answer during we judge.
// Be careful about the -1 or not.
class Solution {
public:
    int longestValidParentheses(string s) {
        int n = s.size();
        stack<int> st;
        int start = 0, ans = 0;
        for (int i = 0; i < n; i++)
        {
            if (s[i] == '(')
                st.push(i);
            else
            {
                if (st.empty())
                {
                    // the point start is included in our ans, so here we don't -1
                    ans = max(ans, i - start);
                    start = i+1;
                }
                else
                    st.pop();
            }
        }

        if (st.empty())
            ans = max(ans, n-start);
        else
        {
            // The st.top() is a left parentheses that is NOT included in our answer, so here we need to -1
            ans = max(ans, n-1 - st.top());
            while (st.size() > 1)
            {
                int t = st.top();
                st.pop();
                ans = max(ans, t-st.top()-1);
            }
            ans = max(ans, st.top() - start);
        }
        return ans;
    }
};

