class Solution {
    public:
        int ans = -99999999; // As the result may be negative, here the initial value should be less than 0.
        int getMax(TreeNode* root)
        {
            if (root == NULL)
                return 0;

            int maxl = max( getMax(root->left), 0);
            int maxr = max( getMax(root->right), 0);
            ans = max( ans, max(maxl, maxr)+root->val);
            ans = max( ans, maxl + maxr + root->val);

            return max( maxl, maxr ) + root->val;
        }
        int maxPathSum(TreeNode* root) {
            getMax(root);
            return ans;
        }
};
