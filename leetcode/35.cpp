// Just a little change to the binary search
class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        // r cannot be nums.size()
        int l = 0, r = nums.size() - 1;
        while (l <= r)
        {
            if (l == r)
                if (nums[l] >= target)
                    return l;
                else
                    return l+1;
            int m = (l + r) / 2;
            // We still need to check nums[m] here
            if (nums[m] == target)
                return m;
            if (nums[m] > target)
                r = m - 1;
            else
                l = m + 1;
        }
        // as the middle is closer to l, so if r is smaller than l, then we should return l, because then l is larger than target.
        return l;
    }
};

