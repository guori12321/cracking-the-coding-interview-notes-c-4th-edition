// Such a problem can be solved by both DP or memorized search. However the boundary (+1, -1 or not) needs many attention.

// Reference: http://fisherlei.blogspot.com/2013/11/leetcode-wordbreak-ii-solution.html
// Memorized Search. In fact we don't have to do another DFS to generate the answer. Instead, we can just use more memory to store the answer buffer, because in this problem the memory is not key issue.
class Solution {
public:

    vector<unordered_set<int> > pre;
    bool visited[50000] = {0};
    vector<string> ans;
    vector<string> st;
    unordered_set<int> lens;

    void getAns(string &s, int k)
    {
        for (unordered_set<int>::iterator it = pre[k].begin(); it != pre[k].end(); it++)
        {
            if (*it == 0)
            {
                string t = s.substr(0, k);
                for (int i = st.size()-1; i >= 0; i--)
                    t += " " + st[i];
                ans.push_back(t);
            }
            else
            {
                st.push_back(s.substr(*it, k-*it));
                getAns(s, *it);
                st.pop_back();
            }
        }
    }

    void DFS( string &s, unordered_set<string>& wordDict, int k)
    {
        if (k >= s.size())
            return;

        // We check and set the visited flag at the beginning of DFS
        if (visited[k])
            return;
        visited[k] = true;

        for (unordered_set<int>::iterator it = lens.begin(); it != lens.end(); it++)
        {
            int i = k+*it;
            // To get the position of the end. However this is not necessary and as we reduce i, the boundary in the following codes are to be changed.
            i--;

            // Check the boundary here!!
            if (i >= s.size())
                continue;
            if (wordDict.find( s.substr(k, i-k+1) ) != wordDict.end() )
            {
                // Be careful about i+1 or i
                pre[i+1].insert(k);
                // Don't set the visited flag. It is different from the 8 queues.
                //visited[i] = 1;
                DFS(s, wordDict, i+1);
            }
        }
    }

    vector<string> wordBreak(string s, unordered_set<string>& wordDict) {
        for (int i = 0; i < s.size() + 2; i++)
        {
            unordered_set<int> t;
            pre.push_back(t);
        }
        int n = s.size();
        if (n == 0 || wordDict.size() == 0)
            return ans;

        for (unordered_set<string>::iterator it = wordDict.begin(); it != wordDict.end(); it++)
            lens.insert((*it).size());

        for (unordered_set<int>::iterator it = lens.begin(); it != lens.end(); it++)
        {
            int i = *it;
            // We should check if *it > s.size() here. However I still ACed...
            if (wordDict.find( s.substr(0, i) ) != wordDict.end() )
            {
                pre[i].insert(0);
                // We cannot set visited flag here, because we would check it at the beginning of the DFS, so we set the flag in the DFS
                //visited[i+1] = 1;
                DFS(s, wordDict, i);
            }
        }

        getAns(s, n);
        return ans;
    }
};

// DP + DFS to generate answer
// Reference: http://www.programcreek.com/2014/03/leetcode-word-break-ii-java/
class Solution {
    public:

        vector<unordered_set<int> > pre;
        bool visited[50000] = {0};
        vector<string> ans;
        vector<string> st;
        unordered_set<int> lens;

        void getAns(string &s, int k)
        {
            for (unordered_set<int>::iterator it = pre[k].begin(); it != pre[k].end(); it++)
            {
                if (*it == 0)
                {
                    string t = s.substr(0, k);
                    for (int i = st.size()-1; i >= 0; i--)
                        t += " " + st[i];
                    ans.push_back(t);
                }
                else
                {
                    st.push_back(s.substr(*it, k-*it));
                    getAns(s, *it);
                    st.pop_back();
                }
            }
        }

        vector<string> wordBreak(string s, unordered_set<string>& wordDict) {
            for (int i = 0; i < s.size() + 2; i++)
            {
                unordered_set<int> t;
                pre.push_back(t);
            }

            unordered_set<int> lens;
            for (unordered_set<string>::iterator it = wordDict.begin(); it != wordDict.end(); it++)
                lens.insert((*it).size());

            // As i is the length of the first word, i could be as large as s.size()
            for (int i = 0; i <= s.size(); i++)
            {
                for (unordered_set<int>::iterator it = lens.begin(); it != lens.end(); it++)
                {
                    int len = *it;
                    // In case of i-len == 0, we still need to check if the first word is in the wordDict.
                    if ( (i-len == 0 && wordDict.find(s.substr(i-len, len)) != wordDict.end()) || (i-len > 0 && pre[i-len].size() > 0 && wordDict.find(s.substr(i-len, len)) != wordDict.end() ) )
                    {
                        pre[i].insert(i-len);
                    }
                }
            }

            getAns(s, s.size());
            return ans;
        }
};
