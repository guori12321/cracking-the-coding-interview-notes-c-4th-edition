class Solution {
public:
    int lengthOfLastWord(string s) {
        int n = s.size();
        if (n == 0)
        return 0;

        int r = n-1;
        while (r >= 0 && s[r] == ' ')
        r--;
        if (r < 0)
        return 0;

        int l = r - 1;
        while (l >= 0 && s[l] != ' ')
        l--;
         return r - l;
    }
};
