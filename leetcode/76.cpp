// A basic window sliding algorithm.
class Solution {
public:
    string minWindow(string s, string t) {
        unordered_map<char, int> mt, ms;
        int nt = t.size(), ns = s.size();
        if (s == "" || t == "")
            return "";

        for (int i = 0; i < nt; i++)
            if ( mt.find(t[i]) != mt.end())
                mt[ t[i] ]++;
            else
                mt[ t[i] ] = 1;

        int minLen = 99999, minl = -1, count = 0;
        string ans = "";
        int l = 0, r = 0;
        while (r < ns)
        {

            if (mt.find(s[r]) != mt.end())
            {
                if (ms.find(s[r]) == ms.end() )
                {
                    ms[ s[r] ] = 1;
                    count++; // Remember to add count in this case
                }
                else
                {
                    if (ms[ s[r] ] < mt[ s[r] ])
                        count++;
                    ms[ s[r] ]++;
                }
            }

            if (count == nt)
            {

                while ( ! (ms.find(s[l]) != ms.end() && ms[ s[l] ] == mt[ s[l] ]) )
                {
                    if (ms.find(s[l]) != ms.end() && ms[ s[l] ] > mt[ s[l] ] )
                        ms[ s[l] ]--;
                    l++;
                }

                if (r-l+1 < minLen)
                {
                    minLen = r-l+1;
                    ans = s.substr(l, r-l+1);
                }
                ms[ s[l] ]--;
                l++;
                count--;
            }
            r++;
        }

        return ans;
    }
};
