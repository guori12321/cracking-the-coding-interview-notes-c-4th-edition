// Just do the binary search on the binary tree.
// Note that & has a lower priority than ==, so we need to add parentheses as (m & 1) == 0, otherwise it would be m & (1 == 0).
class Solution {
public:
    int search(TreeNode* root, int m, int k, int dl)
    {
        if (root == NULL)
            return k-1;

        // As the node numbered 01 would be go left (0) then go right (1), here we need to find the corresponding command encoded in the number m
        if ( ((m >> (dl-1 - k)) & 1) == 0)
            return search(root->left, m, k+1, dl);
        return search(root->right, m, k+1, dl);
    }

    int countNodes(TreeNode* root) {
        if (root == NULL)
            return 0;
        TreeNode* lnode = root->left, *rnode = root->right;
        int dl = 1, dr = 1;
        while (lnode != NULL)
        {
            dl++;
            lnode = lnode->left;
        }
        while (rnode != NULL)
        {
            dr++;
            rnode = rnode->right;
        }
        if (dl == dr)
            return pow(double(2), dl) - 1;

        int l = 0, r = 1;
        for (int i = 1; i < dr; i++)
            r |= 1 << i;
        while ( l+1 != r)
        {
            int m = (l+r) / 2;
            // Here we number the leaf nodes from 0. And its number would be the path from root to the node. For instance, node 01 would be the one go left (0) from the root and then go right (1).
            if ( search(root, m, 1, dl) == dl)
                l = m;
            else
                r = m;
        }
        return pow(double(2), dr) - 1 + r;
    }
};
