// This is combination rather than unique permutation! So [1, 2] and [2, 1] are the same!
// So the DFS methods would be different

class Solution {
public:
    vector< vector<int> > ans;
    vector<int> s;
    map<int, int> m;
    int n;

    void DFS(int target, map<int, int>::iterator last, int sum)
    {
        if (target == sum)
        {
            ans.push_back(s);
            return;
        }
        if (target < sum || last == m.end())
            return;

        // Here the variable name is m rather than map, so m.end() rather than map.end()
        map<int, int>::iterator it = last;
        // There is NO it+1, only it++;
        it++;
        // In the for loop we use at least one of last->first, so we need to DFS when we don't use any of last->first
        DFS(target, it, sum);
        for (int i = 0; i < last->second; i++)
        {
            s.push_back(last->first);
            sum += last->first;
            //DFS(target, last+1, sum); is illegal; only last++;
            DFS(target, it, sum);
        }
        for (int i = 0; i < last->second; i++)
            // s.end() rather than s->end() because s is a vector rather than a map
            s.erase(s.end() - 1);
        return;
    }
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
             n = candidates.size();
        for (int i = 0; i < n; i++)
            if (m.find(candidates[i]) == m.end() )
                m[ candidates[i] ] = 1;
            else
                m[ candidates[i] ] = m.find( candidates[i] )->second + 1;
        DFS(target, m.begin(), 0);
        return ans;
    }
};
