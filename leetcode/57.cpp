// A tricky solution. We add the intervals one by one and replace newInterval if necessary.
// Reference: http://www.programcreek.com/2012/12/leetcode-insert-interval/
class Solution {
public:
    vector<Interval> insert(vector<Interval>& intervals, Interval newInterval)     {
        int n = intervals.size();
        vector<Interval> ans;

        for (int i = 0; i < n; i++)
        {
            if ( intervals[i].end < newInterval.start)
                ans.push_back(intervals[i]);
            else if (newInterval.end < intervals[i].start)
            {
                ans.push_back(newInterval);
                newInterval = intervals[i];
            }
            else
            {
                newInterval.start = min(newInterval.start, intervals[i].start);
                newInterval.end = max(newInterval.end, intervals[i].end);
            }
        }
        ans.push_back(newInterval);
        return ans;
    }
};
