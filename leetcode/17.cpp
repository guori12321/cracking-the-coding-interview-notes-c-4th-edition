// Discuss the undefined cases with the interviewer, for instance, what should we return if the input is an empty string? In this case we are expected to return an empty vector, however I returned a vector containing only one empty string, and I got Wrong Answer.
class Solution {
public:
    map<int, string> m;
    vector<char> s;
    vector<string> ans;

    void DFS(string digits, int h)
    {
        int n = digits.size();
        if (h == n)
        {
            if (n == 0)
                return;
            string st = "";
            for (int i = 0; i < h; i++)
                st += s[i];
            ans.push_back(st);
            return;
        }

        string t = m.find(digits[h] - '0')->second;
        int nt = t.size();
        for (int i = 0; i < nt; i++)
        {
            // s[h], rather than s[i]
            s[h] = t[i];
            DFS(digits, h+1);
        }
        return;
    }

    vector<string> letterCombinations(string digits) {
    m[0] = " ";
    m[1] = "";
    m[2] = "abc";
    m[3] = "def";
    m[4] = "ghi";
    m[5] = "jkl";
    m[6] = "mno";
    m[7] = "pqrs";
    m[8] = "tuv";
    m[9] = "wxyz";

    int i = 0;
    while (i < digits.size())
    {
        if (digits[i] == '1')
            digits.erase(i, 1);
        else i++;
    }
    for (int i = 0; i < digits.size(); i++)
        s.push_back('-');
    DFS(digits, 0);

    return ans;
    }
};
