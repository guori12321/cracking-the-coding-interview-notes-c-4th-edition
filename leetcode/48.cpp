// This one is not difficult. Just be clear in your mind.
// Just layer by layer and rotate by rows and columns.
// If the row is fixed, then it is layer, if not then item. If reverted then n-1-layer and n-1-item (starting from the tail rather than head)
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int n = matrix.size();
    if (n == 0)
        return;

    int layerNum = n / 2;
    for (int layer = 0; layer < layerNum; layer++)
    {
        // We don't touch the end of the current row or column. The end is treated as the beginning of the other column or row.
        for (int item = layer; item < n - 1 - layer; item++)
        {
            int t = matrix[layer][item];
            matrix[layer][item] = matrix[n-1 - item][layer];
            matrix[n-1 - item][layer] = matrix[n-1 - layer][ n-1 - item];
            matrix[n-1 - layer][n-1 - item] = matrix[item][n-1-layer];
            matrix[item][n-1-layer] = t;
        }
    }
    return;
    }
};
