// Careful about the boundary all the time!
// Submitted 4 times...
class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int n = nums.size();
        if (n <= 2)
            return n;

        int l = 0, r = 1;
        while (l < n)
        {
            while ( (l < n) && ( l < 2 || (l >= 2 && nums[l] >= nums[l-1] && nums[l] > nums[l-2] )))
                l++;
            if (l == n)
                return n;
            r = max(r, l+1);
            // The current nums[l] is NOT validate, so, we compare nums[r] with nums[l-2] (not nums[l-1] because a new value can be the same as nums[l-1])
            while ( r < n && nums[r] <= nums[l-2])
                r++;
            if (r == n)
                return l;
            nums[l] = nums[r];
            // We need to mark nums[r] as used, because we allow duplicate twice, so when there is a valid candidate which is the same as the previous one, then we don't know if this candidate is already used or it's a new one
            // A test case could be
            // Input: [1,1,1,2]
            // Output: [1,1,2,2]  If we don't mark it as used, then we may use the 2 two times.
            // Expected: [1,1,2]
            nums[r] = -99999;
            l++;
            r++;
        }
        return l;
    }
};
