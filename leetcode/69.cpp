class Solution {
public:
    int mySqrt(int x) {
        if (x <= 0)
            return 0;

        long long l = 1, r = 1;
        while (r*r < x)
        {
            r *= 2;
        }
        while (l < r)
        {
            // if m is int, then m*m is int as well, so it would overflow
            long long m = (l+r) / 2;
            // Note that here we find the largest m that m*m <= x
            if (m*m <= x && (m+1)*(m+1) > x)
                return m;
            if (m*m < x)
                l = m + 1;
            else
                r = m-1;
        }
        // if l == r, then we return r; if r == l-1, then l, or the last m is not satisfied, so r-1 is the correct answer.
        return r;
    }
};
