class Solution {
public:
    // include l, don’t include r
    TreeNode* DFS(ListNode* l, ListNode* r)
    {
        if (l == NULL)
            return NULL;
        int n = 0;
        ListNode* t = l;
        while (t != NULL && t != r)
        {
            n++;
            t = t->next;
        }
        // If t == NULL, then, perhaps the right side is NULL or l is at the right of r. Only the second case should we return NULL.
        if ( (t == NULL && r != NULL) || n == 0)
            return NULL;
        int m = n / 2;
        int vm = 0;
        t = l;
        for (int i = 0; i < m; i++)
            t = t->next;
        vm = t->val;

        TreeNode* root = new TreeNode(vm);
        root->left = DFS(l, t);
        root->right = DFS(t->next, r);
        return root;
    }

    TreeNode* sortedListToBST(ListNode* head) {
        return DFS(head, NULL);
    }
};
