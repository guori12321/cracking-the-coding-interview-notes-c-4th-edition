// At the beginning I thought we need to find the repeating pattern in the decimal pattern, however this is not possible.
// After reading this reference, http://www.programcreek.com/2014/03/leetcode-fraction-to-recurring-decimal-java/  , I found we can maintain a hash table of remainder.
// Lots of corner cases to care about.

class Solution {
public:
    string fractionToDecimal(int numerator, int denominator) {
        string ans = "";

        // Illegal input
        if (denominator == 0)
            return ans;
        if (numerator == 0)
            return "0";

        // INT_MIN / -1 would overflow the int range
        long long num = numerator;
        long long den = denominator;

        // We cannot use num*den to judge if the result is negative because INT_MIN * INT_MIN would overflow even in the long long range
        // And 7 / (-12) in C++ would return 0, but in Python it is -1
        // So the best way is to judge the division
        if (num/den < 0 || den/num < 0)
            ans += "-";

        num = abs(num);
        den = abs(den);

        long long intPart = num / den;
        ans += to_string(intPart);

        long long reminador = num % den;
        if (reminador == 0)
            return ans;

        // In case of no decimal part
        ans += ".";
        unordered_map<long long, int> rSet;
        string floatPart = "";
        int k = 0;
        while (reminador != 0 && rSet.find(reminador) == rSet.end() )
        {
            rSet[reminador] = k;
            k++;
            floatPart += to_string(reminador*10 / den);
            reminador = reminador*10 % den;
        }

        // In case of no repeat pattern in the decimal part
        if (reminador == 0)
            return (ans + floatPart);

        floatPart.insert(rSet[reminador], "(");
        floatPart += ")";
        ans += floatPart;
        return ans;
    }
};
