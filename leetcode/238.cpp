// Reference: http://www.programcreek.com/2014/07/leetcode-product-of-array-except-self-java/
// At first glance I felt very weird. The problem is trivial but the limitation is unreasonable. And the space complexity is also strange: O(1) but the space for answer (O(n)) is not considered here.
//
// In fact, the target of this problem is to apply the divide-and-conquer algorithm. The product without self can be divided into two parts, the sum of the left and of right, and then we can get the sum by multiple the two parts.
// But still this problem is too tricky.
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int n = nums.size();
        vector<int> ans(n, 1);

        // Note that there is i-- rather than i++
        for (int i = n-2; i >= 0; i--)
        {
            ans[i] = nums[i+1] * ans[i+1];
        }

        int t = nums[0];
        for (int i = 1; i < n; i++)
        {
            ans[i] *= t;
            t *= nums[i];
        }

        return ans;
    }
};
