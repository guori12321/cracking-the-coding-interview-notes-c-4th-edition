// My first idea is to enumerate the breaking point and then we will have the same previous problem, however enumeration takes a long time.
// Next idea is to optimize the enumeration by memorizing the initial states, however, it is not necessary or helpful as in this problem the computing doesn't overlap: it is more like a binary search than a DP and the adjacent searches doesn't share the same parts.
// So, my last idea is, the difference between this problem and the previous one is just cycle, so, we focus on the first and last item in the nums. There are just two cases, if we keep the first item then we cannot keep the last and the cycle becomes a line again, or the other way we keep the last and lost the first.
class Solution {
public:
    int DFS(vector<int>& nums, int l, int r)
    {
        if (l > r)
            return 0;
        if (l == r)
            return nums[l];
        if (r - l == 1)
            return max(nums[l], nums[r]);
        int m = (l+r) / 2;

        return max( DFS(nums, l, m-1) + DFS(nums, m+1, r), nums[m] + DFS(nums, l, m-2) + DFS(nums, m+2, r) );
    }

    int rob(vector<int>& nums) {
        int n = nums.size();
        if (n == 0)
            return 0;

        return max( nums[0] + DFS(nums, 2, n-2), DFS(nums, 1, n-1) );
    }
};
