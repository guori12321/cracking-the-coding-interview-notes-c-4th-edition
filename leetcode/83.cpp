// A simple one, but still need to be careful.

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        ListNode* t = head;
        while (t != NULL && t->next != NULL)
        {
            // As there may be multiple duplicate numbers, we should use a while rather than an if here.
            // We still need to check if t->next is NULL or not.
            while ( t->next != NULL && t->next->val == t->val)
            {
                // tt is the pointer so remember to declear * here.
                ListNode* tt = t->next;
                t->next = t->next->next;
                delete tt;
            }
            t = t->next;
        }
        return head;
    }
};
