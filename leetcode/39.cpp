// As the problem description changes, the codes would change correspondingly.
class Solution {
public:
    vector< vector<int> > ans;
    int hash[1000] = {0};

    void DFS(vector<int> & candidates, int target, int d, int sum)
    {
        int n = candidates.size();
        if (sum > target)
            return;
        if (sum == target)
        {
            vector<int> t;
            for (int i = 0; i < d; i++)
                if (hash[i] > 0)
                    for (int j = 0; j < hash[i]; j++)
            t.push_back(candidates[i]);
            ans.push_back(t);
            return;
        }
        // If we put this if statement in front of the sum check statement, then, the last item in candidates won't be used because at that time d == n
        // When d == n and we don't meet the target, then we should break, so d >= n rather than d > n.
        if (d >= n)
            return;

        // Here as we want to reach target, so the condition should be <= rather than <
        for (int i = 0; candidates[d] * i + sum <= target; i++)
        {
            hash[d] = i;
            DFS(candidates, target, d+1, sum + candidates[d] * i);
        }
        return;
    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        // Sort the input vector!!! Don't assume that it is given in order
        sort(candidates.begin(), candidates.end());
        DFS(candidates, target, 0, 0);
        return ans;
    }
};
