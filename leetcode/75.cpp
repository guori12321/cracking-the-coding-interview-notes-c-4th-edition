class Solution {
public:
    void sortColors(vector<int>& nums) {
        int n = nums.size();
        int count[3] = {0};
        for (int i = 0; i < n; i++)
            count[ nums[i] ]++;
        int t = 0;
        for (int i = 0; i < 3; i++)
        for (int j = 0; j < count[i]; j++)
        {
            nums[t] = i;
            t++;
        }

        return;
    }
};
