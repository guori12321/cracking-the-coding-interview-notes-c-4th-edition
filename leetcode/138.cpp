class Solution {
public:
    RandomListNode *copyRandomList(RandomListNode *head) {
        if (head == NULL)
            return NULL;

        unordered_map<RandomListNode*, int> m1;
        unordered_map<int, RandomListNode*> m2;

        RandomListNode * t = head;
        int n = 0;
        while (t != NULL)
        {
            m1[t] = n;
            n++;
            t = t->next;
        }

        RandomListNode *ans = new RandomListNode(head->label);
        RandomListNode *t1 = head, *t2 = ans;

        while (t1->next != NULL)
        {
            t2->next = new RandomListNode(t1->next->label);
            t1 = t1->next;
            t2 = t2->next;
        }

        t2 = ans;
        n = 0;
        while (t2 != NULL)
        {
            m2[n] = t2;
            n++;
            t2 = t2->next;
        }

        t1 = head; t2 = ans;
        while (t1 != NULL)
        {
            if (t1->random != NULL)
            {
                // NOT m1.find(t1)->second;
                n = m1.find(t1->random)->second;
                t2->random = m2.find(n)->second;
            }
            t1 = t1->next;
            t2 = t2->next;
        }

        return ans;
    }
};
