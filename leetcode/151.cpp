// This problem is easy, so we need to program in a short time.
// However there are still lots of corner cases to care about.

// Reference: http://blog.csdn.net/kenden23/article/details/20701069
// We can record the middle words.
class Solution {
public:
    void reverseWords(string &s) {
        int n = s.size();
        string ans = "";

        int i = n - 1;
        while (i >= 0)
        {
            while (i >= 0 && s[i] == ' ')
                i--;
            // We have to check if i < 0 here as the previous sentence is a while loop
            if (i < 0)
                break;
            string t;
            while (i >= 0 && s[i] != ' ')
            {
                t = s[i] + t;
                i--;
            }
            // It is not easy to judge if we are adding the last word into ans, so we judge if ans is empty or not. In the other words, we are adding " " + t rather than t + " "
            if (ans.size() == 0)
                ans = t;
            else
                ans = ans + " " + t;
        }
        s = ans;
        return;
    }
};

// We can also use two points to locate the words
class Solution {
public:
    void reverseWords(string &s) {
        string ans = "";

        int last = s.size() - 1, start = s.size() - 1;

        while (last >= 0 && s[last] == ' ')
            last--;
        // If all the chars are space
        if (last < 0)
        {
            s = ans;
            return;
        }
        // Remove the spaces at the left side, otherwise we don't know which word is the left-most one and if we need to add space in front of the word
        int leftSpace = 0;
        while (s[leftSpace] == ' ')
            leftSpace++;
        s = s.substr(leftSpace, s.size()-leftSpace);

        while (last >= 0)
        {
            // start is the first space at the right side of last
            start = last;
            while (start >= 0 && s[start] != ' ')
                start--;
            {
                if (ans != "")
                    ans = ans + " " + s.substr(start+1, last - start);
                // No spaces if there is only one word
                else
                    ans = s.substr(start+1, last - start);
                last = start - 1;
            }
            while (last >= 0 && s[last] == ' ')
                last--;
        }
        s = ans;
        return;
    }
};
