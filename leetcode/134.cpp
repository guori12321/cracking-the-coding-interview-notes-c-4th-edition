// Let's try greedy algorithm first.
// Is it possible that we find the maximal gas[i] and then start from it? Not really, because there may be more gas before that maximal gas[i].
// So, it is possible that we find a station that we cannot pass  with its own gas (i.e. cost[i] > gas[i]), and then, we try to find the next closet station that cost[i] <= gas[i] and then start?
// I think it works, but there is one test case:
//
//Input: [1,2,3,3], [2,1,5,1]
//Output: -1
//Expected: 3
//
// So, there are two stations that we cannot pass with the gas at that station only: the first one and third one.
// We cannot get passed from the second station, however we can from the forth one.
//
// Let's update our algorithm a little: find the station which lacks gas the most (where cost[i]-gas[i] is largest), and then, start from its next closest station where there are enough gas.

class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int n = gas.size();
        for (int i = 0; i < n; i++)
        {
            gas.push_back( gas[i] );
            cost.push_back( cost[i] );
        }

        // We cannot claim i and j inside the loop as we always do.
        int maxDelta = -1;
        int i, j, k = -1;
        for (i = 0; i < n; i++)
        {
            if (cost[i] - gas[i] > maxDelta)
            {
                maxDelta = cost[i] - gas[i];
                k = i;
            }
        }

        if (k == -1)
            return 0;

        i = k;
        for (j = i+1; j < i+1+n; j++)
            if (cost[j] <= gas[j])
                break;
        if (j == i+1+n)
            return -1;

        int tot = 0;
        for (i = j; i < j+n; i++)
        {
            tot += gas[i];
            tot -= cost[i];
            if (tot < 0)
                return -1;
        }
        // As we copied the cycle, we should return j%n rather than j
        return j % n;
    }
};
