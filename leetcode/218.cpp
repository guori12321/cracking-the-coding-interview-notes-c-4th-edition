// Reference: http://www.cnblogs.com/easonliu/p/4531020.html
//
// Interval questions always sort the x coordinates first, and then merge or do something.
// Here we sort the x coordinates as well. First we add the boundary points of the buildings into a vector t, and then sort the points.
// Next, we scan from left to right and add the boundary points into a max heap (here we use multiset instead) to get the current highest height.
// More details are given in the reference. Note that in case of the same height (if we have two buildings stand exactly next to each other and share the same height), we have to add the new height first and then delete the old height, otherwise if we delete first then we would get a lower point.
class Solution {
public:
    vector<pair<int, int>> getSkyline(vector<vector<int>>& buildings) {
        vector<pair<int, int> > ans, t;
        int n = buildings.size();

        for (int i = 0; i < n; i++)
        {
            t.push_back( make_pair(buildings[i][0], -buildings[i][2]) );
            t.push_back( make_pair( buildings[i][1], buildings[i][2]) );
        }
        sort(t.begin(), t.end());

        multiset<int> m;
        m.insert(0);

        int pre = 0, cur = 0;
        for (int i = 0; i < t.size(); i++)
        {
            if (t[i].second < 0)
                m.insert(-t[i].second);
            else
                // Delete one height here. m.earse(t[i].second) would delete all the same height.
                m.erase( m.find(t[i].second) );
            cur = *m.rbegin();
            if (cur != pre)
            {
                pre = cur;
                ans.push_back( make_pair( t[i].first, cur) );
            }
        }
        return ans;
    }
};
