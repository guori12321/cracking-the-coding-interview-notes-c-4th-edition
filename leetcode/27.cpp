class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
    int n = nums.size();
    int l = 0, r = n - 1;

    while (l < r)
    {
        while ( l < n && nums[l] != val)
        l++;
        while (r > 0 && nums[r] == val)
        r--;
        if (l < r)
        {
            nums[l] = nums[r];
            nums[r] = val;
            l++;
            r--;
        }
    }
    // Maybe the while stops when l == r, in this case, we have no idea about nums[l]
    if (l == n || (l < n && nums[l] == val))
        return l;
    return l+1;
    }
};

