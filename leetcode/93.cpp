class Solution {
    public:

        vector<string> ans;
        void search(string a, string b, int k)
        {
            if (b.size() < 4-k)
                return;
            //cout << a <<endl;
            if (k == 3)
            {
                // The first digit of IP number cannot be 0, however 0 is okay
                if (b.size() > 1 && b[0] == '0')
                    return;

                // Remember to check if b is empty string
                if (b.size() > 0 && b.size() <= 3 && stoi(b)<=255)
                    ans.push_back(a+"." + b);
                return;
            }

            for (int i = 0; i < 3; i++)
            {
                // Note that 0 is okay, 01 is not
                if (i > 0 && b[0] == '0')
                    continue;
                if (b.size() > i && stoi(b.substr(0, i+1)) <= 255)
                {
                    if (a.size() > 0)
                        search(a+"."+b.substr(0, i+1), b.substr(i+1, b.size()-(i+1)), k+1) ;
                    else
                        search(b.substr(0, i+1), b.substr(i+1, b.size()-(i+1)), k+1) ;
                }
            }
        }

        vector<string> restoreIpAddresses(string s) {
            search("", s, 0);
            return ans;
        }


};
