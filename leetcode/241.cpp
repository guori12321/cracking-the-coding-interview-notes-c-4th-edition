// At the beginning I thought we could use a stack (in fact the parentheses problems always do) and recursive in two branches when we get a new number, one is to push the new element into the stack and then go on, one is to pop the operation and another previous element, conduct the operation and push the result back, and then go on.
// However, even in the "2-1" case this algorithm won't work. When we get 1, we may go on and then calculate 2-1, or we may conduct 2-1 and then go on, and we reach the end of the string and then get 1 again. So the result would be two 1 rather than just one.
//
// Reference: https://leetcode.com/discuss/55255/clean-ac-c-solution-with-explanation
// And after a glance of the reference, I found this is just a divide and conquer problem, and then finish the codes and AC very quickly.
//
// So, the lesson from this problem is: Think Before Code!
class Solution {
public:
    vector<int> ans;
    stack<string> st;

    map<pair<int, int>, vector<int> > m;

    void DFS(string& input, int l, int r)
    {
        if (m.find(make_pair(l, r) ) != m.end())
            return;

        bool flag = true;
        vector<int> t;
        for (int i = l+1; i <= r-1; i++)
        {
            if (input[i] == '+' || input[i] == '-' || input[i] == '*')
            {
                flag = false;
                DFS(input, l, i-1);
                DFS(input, i+1, r);
                vector<int> left = m[make_pair(l, i-1)];
                vector<int> right = m[make_pair(i+1, r)];

                for (vector<int>::iterator iti = left.begin(); iti != left.end(); iti++ )
                    for (vector<int>::iterator itj = right.begin(); itj != right.end(); itj++)
                    {
                        if (input[i] == '+')
                            t.push_back(*iti + *itj);
                        else if (input[i] == '-')
                            t.push_back(*iti - *itj);
                        else if (input[i] == '*')
                            t.push_back(*iti * *itj);
                    }
            }
        }
        if (flag)
            // vector<int>(n) would be a vector of n elements of value 0 rather than a element of value n.
            m[make_pair(l, r)] = vector<int>(1, stoi(input.substr(l, r-l+1)));
        else
            m[make_pair(l, r)] = t;
        return;
    }

    vector<int> diffWaysToCompute(string input) {
        if (input.size() == 0)
            return ans;

        DFS(input, 0, input.size()-1);
        return m[make_pair(0, input.size()-1)];
    }
};
