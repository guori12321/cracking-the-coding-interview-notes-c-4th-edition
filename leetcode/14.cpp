// Be careful!
class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
    string ans = "";
         int n = strs.size();

    if (n == 0)
        return ans;

    for (int i = 0; i < strs[0].size(); i++)
    {
        for (int j = 0; j < n; j++)
        {
            // Here as i is the length, so strs[j][i] rather than strs[i][j]
            // Always check the length first
            if ( !(strs[j].size() >= i &&  strs[j][i] == strs[0][i]) )
                return ans;
        }
        ans += strs[0][i];
    }
    return strs[0];
    }
};
