// The basic idea is easy: just to avoid the overlap computing
class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int n = nums.size();
        if (n == 0)
            return 0;

        int ans = 0;
        long long sum = 0;
        for (int i = 0; i < n; i++)
        {
            sum += nums[i];
            ans++;
            if (sum >= s)
                break;
        }

        if (sum < s)
            return 0;

        for (int i = 1; i < n; i++)
        {
            int r = min(n, i+ans-1);
            sum -= nums[i-1];
            if (r < n)
                sum += nums[r];
            else
                r = n-1;

            // Special judge: if we include the right-most element
            if (sum >= s)
                ans = min(ans, r+1-i);

            // And if we don't include
            while (sum - nums[r] >= s && r >= i)
            {
                sum -= nums[r];
                r--;
                ans = min(ans, r-i+1);
            }
        }

        return ans;
    }
};
