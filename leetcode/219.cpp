// The simple way is to do a brute force check, which takes O(n*k) time.
// My next idea is to sort, and then check the original distance of adjacent two numbers if they are the same. The sorting takes O(n logn), even worse.
//
// For such a problem, we always want to do it just once. And hashtable or map or set are always used to reduce the time by using more memory.
// So, just memorize the last place those numbers show, and check if the distance between the current number and the last one is larger than k.
class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
        int n = nums.size();
        map<int, int> last;

        for (int i = 0; i < n; i++)
        if (last.find( nums[i] ) == last.end() )
            last[ nums[i] ] = i;
        else
            if ( i - last.find( nums[i] )->second <= k)
                return true;
            else
                last[ nums[i] ] = i;
        return false;
    }
};
