class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        vector<vector<int>> ans;
        vector<int> t;
        for (int i = 0; i < n; i++)
            t.push_back(0);
        for (int i = 0; i < n; i++)
            ans.push_back(t);

        int x = 1;
        // A little trick here, to avoid treat odd and even n separately.
        int rowNum = (n+1) / 2;
        for (int row = 0; row < rowNum; row++)
        {
            for (int item = row; item < n - row; item++)
                ans[row][item] = x++;
            // If n is odd, then, the center grid may be overwritten in the later for loop
            if (x >= n*n)
                break;
            for (int item = row+1; item < n-row-1; item++)
                ans[item][n-1-row] = x++;
            for (int item = row; item < n - row; item++)
                ans[n-1-row][n-1-item] = x++;
            for (int item = row+1; item < n-row-1; item++)
                ans[n-1-item][row] = x++;
            }
        return ans;
    }
};
