// The idea is easy, however lots of details are to be considered when coding

class Solution {
public:
    void deepCopy(TreeNode* target, TreeNode* source, int offset)
    {
        if (source == NULL)
            return;
        target->val = source->val + offset;
        if (source->left != NULL)
        {
            target->left = new TreeNode(-1);
            deepCopy(target->left, source->left, offset);
        }
        if (source->right != NULL)
        {
            target->right = new TreeNode(-1);
            deepCopy(target->right, source->right, offset);
        }
    }

    vector<TreeNode*> generateTrees(int n) {
        vector< vector<TreeNode*> > ans;
        vector<TreeNode*> t0, t1;
        t0.push_back(NULL);
        TreeNode* tt = new TreeNode(1);
        t1.push_back(tt);
        ans.push_back(t0);
        ans.push_back(t1);

        for (int i = 2; i <= n; i++)
        {
            vector<TreeNode*> vt;
            ans.push_back(vt);
            for (int j = 0; j <= i-1; j++)
            {

                for (int l = 0; l < ans[j].size(); l++)
                {
                    for (int r = 0; r < ans[ i-1-j ].size(); r++)
                    {
                        // We need to claim root in the most inner loop, because tree is not vector and they copy by address rather than value, so we should not overlap any trees in this problem
                        TreeNode* root = new TreeNode(j+1);
                        // Here we need to check if the left is NULL. In this case there is no good method to skip this check
                        if (ans[j][l] != NULL)
                        {
                            root->left = new TreeNode(-1);
                            deepCopy(root->left, ans[j][l], 0);
                        }
                        // Here is not ans[j][r], an error that I made when submitted
                        if (ans[i-1-j][r] != NULL)
                        {
                            root->right = new TreeNode(-1);
                            deepCopy(root->right, ans[i-1-j][r], j+1);
                        }
                        ans[i].push_back(root);
                    }
                }
            }
        }

        // Here is not ans as the other problems. Another error I made when submitted.
        return ans[n];
    }
};
