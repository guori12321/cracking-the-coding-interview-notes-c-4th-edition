class Solution {
public:
    // don't name it like isAlpha, because we might assume it is boolean and use it wrongly later
    int getAlpha(char c)
    {
        if (c >= 'a' && c <= 'z')
            return c - 'a';
        if (c >= 'A' && c <= 'Z')
            return c - 'A';
        if (c >= '0' && c <= '9')
            return c + 300; // we map the 0..9 to a different domain from chars

        return -1;
    }

    bool isPalindrome(string s) {
        int n = s.size();
        if (n <= 1)
            return true;
        int l = 0, r = n-1;
        while (l < r)
        {
            while (l < r && getAlpha(s[l]) == -1)
                l++;
            while (l < r && getAlpha(s[r]) == -1)
                r--;
            if (l < r && getAlpha(s[l]) != getAlpha(s[r]) )
                return false;
            l++;
            r--;
        }
        return true;
    }
};
