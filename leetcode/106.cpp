class Solution {
public:
    TreeNode* DFS( vector<int>& inorder, vector<int> & postorder, int il, int ir, int pl, int pr)
    {
        if (pl > pr)
            return NULL;
        if (pl == pr)
        {
            TreeNode* root = new TreeNode(postorder[pl]);
            return root;
        }

        // We want to use the value of i later, so we cannot claim it in the loop
        int i;
        for (i = il; i <= ir; i++)
            if (inorder[i] == postorder[pr])
            {
                break;
            }
        TreeNode* root = new TreeNode(postorder[pr]);
        root->left = DFS(inorder, postorder, il, i-1, pl, pl + (i-1-il));
        root->right = DFS(inorder, postorder, i+1, ir, pl + (i-1-il) + 1, pr-1);
        return root;
    }

    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        return DFS(inorder, postorder, 0, inorder.size() - 1, 0, postorder.size() - 1);
    }
};
