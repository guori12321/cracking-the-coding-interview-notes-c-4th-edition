// DFS and BFS are the basic operation in tree, so just think if we can modify the two methods to solve this problem.
// Recursively
class Solution {
public:
    bool check(TreeNode* l, TreeNode* r)
    {
        if (l == NULL && r == NULL)
            return true;
        if (l == NULL || r == NULL)
            return false;
        if (l->val != r->val)
            return false;
        return check(l->left, r->right) && check(l->right, r->left);
    }
    bool isSymmetric(TreeNode* root) {
    if (root == NULL)
        return true;
    return check(root->left, root->right);
    }
};

// Iteratively: just do the stack operation manually
class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        stack<TreeNode*> l, r;
        if (root == NULL)
            return true;
        l.push(root->left);
        r.push(root->right);

        while (! l.empty() )
        {
            TreeNode* lt = l.top();
            l.pop();
            TreeNode* rt = r.top();
            r.pop();
            if (lt == NULL && rt == NULL)
                continue;
            if (lt == NULL || rt == NULL)
                return false;
            if (lt->val != rt->val)
                return false;
            l.push(lt->left);
            l.push(lt->right);
            r.push(rt->right);
            r.push(rt->left);
        }
        return true;
    }
};
