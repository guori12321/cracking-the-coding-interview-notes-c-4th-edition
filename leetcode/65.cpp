// Note 6e6.5 is NOT validate, but 6.5e6 is.
class Solution {
public:
    bool isNoENumber(string s, bool point)
    {
        int n = s.size();
        if (n == 0)
            return false;
        bool hasNum = false;

        int i = 0;
        if (s[0] == '+' || s[0] == '-')
            i++;
        while (i < n)
        {
            if (s[i] == '.')
            {
                if (point == false)
            point = true;
                else return false;
            }
            else if (s[i] < '0' || s[i] > '9')
                return false;
            else
                hasNum = true;
            i++;
        }
        return hasNum;
    }
    bool isNumber(string s) {
        int n = s.size();
        if (n == 0)
        return false;

        int l = 0, r = n-1;
        while (l < n && s[l] == ' ')
        l++;
        while (r >= l && s[r] == ' ')
            r--;
        s = s.substr(l, r-l+1);
        n = s.size();
        if (n == 0)
            return false;

        if (isNoENumber(s, 0))
            return true;
        for (int i = 0; i < n; i++)
            if (s[i] == 'E' || s[i] == 'e')
                return isNoENumber(s.substr(0, i), 0) && isNoENumber(s.substr(i+1, n-(i+1)), 1);
        return false;
    }
};
