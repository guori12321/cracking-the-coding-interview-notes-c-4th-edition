// The problem description is ambiguous, and I think that's why the AC rate is so low for such a easy problem. The successive elements means the location in the sorted order is successive rather than in the original order.
// As the problem asks for a linear complexity in both time and space, the bucket sorting is the best policy here.

class Solution {
public:
    int maximumGap(vector<int>& nums) {
    set<int> s;
    int n = nums.size();
    if (n < 2)
        return 0;

    for (int i = 0; i < n; i++)
        s.insert(nums[i]);

    int ans = 0;

    // No s.begin() + 1 here: + is not support in iterator operation.
    for (set<int>::iterator it = s.begin(); it != s.end(); it++)
    {
        if (it == s.begin())
            continue;
        set<int>::iterator last = it;
        last--;

        // find the value of set<int>::iterator   : *it
        ans = max(ans, *it - *last);
    }

    return ans;
    }
};
