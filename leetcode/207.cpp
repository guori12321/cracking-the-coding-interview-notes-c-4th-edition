class Solution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        int n = numCourses;
        int done = 0;

        int pre[100000] = {0};
        bool visited[100000] = {0};
        for (int i = 0; i < prerequisites.size(); i++)
        {
            pre[ prerequisites[i].second ]++;
        }

        int i = 0;
        while (i < n)
        {
            if (pre[i] == 0 && visited[i] == false)
            {
                visited[i] = true;
                done++;
                int j = 0;
                while ( j < prerequisites.size() )
                {
                    if (prerequisites[j].first == i)
                    {
                        pre[ prerequisites[j].second ]--;
                        //Erase takes a long time, so just check the entire prerequisites every time.
                        //prerequisites.erase(prerequisites.begin() + j);
                    }
                    //else
                        j++;
                }
                i = 0;
            }
            else i++;
        }
        if (done == numCourses)
            return true;
        return false;
    }
};

