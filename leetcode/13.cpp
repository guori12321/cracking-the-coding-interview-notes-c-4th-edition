// Knowledge to Roman Number is necessary
// A good reference is http://fisherlei.blogspot.com/2012/12/leetcode-roman-to-integer.html
class Solution {
public:

    inline int c2i(char c) {
      switch(c) {
        case 'I': return 1;
        case 'V': return 5;
        case 'X': return 10;
        case 'L': return 50;
        case 'C': return 100;
        case 'D': return 500;
        case 'M': return 1000;
         default: return 0;
       }
     }

    int romanToInt(string s) {
        int n = s.size();
        int last = 99999;
        int t = 0;
        int ans = 0;
        int current = 0;
        for (int i = 0; i < n; i++)
        {
            // Note to use c2i() value rather than s[i] directly in the if condition
            // A trick here is to delete 2*c2i(s[i-1]) so that we can avoid a flag to judge if we have added the previous number
            if (i > 0 && c2i(s[i]) > c2i(s[i-1]))
                ans += (c2i(s[i]) - 2*c2i(s[i-1]) );
            else
                ans += c2i(s[i]);
        }
        return ans;
    }
};
