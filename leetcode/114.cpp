// Problems like this can always be solved by DFS, or BFS

// DFS:
class Solution {
public:
    TreeNode* now;
    void DFS(TreeNode* root)
    {
        if (root == NULL)
            return;
        TreeNode* tr = root->right;
        now->right = root;
        now = now->right;
        DFS(root->left);
        // Just remember to set the left pointer to NULL. The right child would be overwritten, but not the left one.
        root->left = NULL;
        DFS(tr);
        return;
    }
    void flatten(TreeNode* root) {
        now = new TreeNode(-1);
        DFS(root);
        //We cannot delete now node, because after the DFS it would be the last one rather than the first node in the linked list.
        //delete now;
        return;
    }
};

// BFS: I don't think this problem is suitable for BFS because BFS searchs layer by layer.
