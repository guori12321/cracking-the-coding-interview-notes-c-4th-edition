// Annoying...
class Solution {
public:
vector<string> fullJustify(vector<string>& words, int maxWidth)
{
    vector<string> ans;
    int n = words.size();
    if (n == 0 || maxWidth == 0)
    {
        ans.push_back("");
        return ans;
    }

    int l = 0, r = 0;
    int tot = 0;
    while (r < n)
    {
        if (tot + (r-l) + words[r].size() <= maxWidth)
        {
            tot += words[r].size();
        }
        else
        {
            string t;
            if (l == r-1)
                t += words[l] + string( maxWidth - words[l].size(), ' ');
            else
            {
                int totSpace = maxWidth - tot;
                int aveSpace = totSpace / (r-1-l);
                for (int i = l; i < r-1; i++)
                {
                    t += words[i] + string(aveSpace, ' ');
                    totSpace -= aveSpace;
                    if (totSpace - aveSpace*(r-1-(i+1)) > 0)
                    {
                        t += " ";
                        totSpace--;
                    }
                }
                t += words[r-1];
            }

            ans.push_back(t);
            l = r;
            // Note that here we've get the first word, so the initial tot value is the size of the word
            tot = words[l].size();
        }
        r++;
    }

    string t;
    int len = 0;
    for (int i = l; i < r-1; i++)
    {
        t += words[i] + ' ';
        len += words[i].size() + 1;
    }
    t += words[n-1];
    len += words[n-1].size();
    t += string(maxWidth-len, ' ');
    ans.push_back(t);

    return ans;
}




};

