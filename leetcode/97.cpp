// At the beginning I don't know how to do this: searching is certainly not possible and I don't know how to search.
// However, except searching the recursive algorithm could be easy and simple to implement, and as the recursive has lots of overlap computing, so the DP is the best way to do this.

// So, search, recursive, and then DP, they are the most popular methods when dealing with such problem!

// A very good reference: http://www.geeksforgeeks.org/check-whether-a-given-string-is-an-interleaving-of-two-other-given-strings-set-2/
// As the length of current s3 == s1 + s2, we can reduce the dimension of length of s2 because we can get that from s3 - s1.
// So we don't need to copy the array as I did. It could be much faster, and the code could be briefer.
class Solution {
    public:
        bool isInterleave(string s1, string s2, string s3) {
            int n1 = s1.size(), n2 = s2.size(), n3 = s3.size();

            // In case of "", "", "" we should return true
            /*if (n1 == 0 || n2 == 0 || n3 == 0)
              return false;*/

            if (n3 != n1+n2)
                return false;

            if (n1 == 0)
                return s2 == s3;
            if (n2 == 0)
                return s1 == s3;

            // We can reduce the memory to two dimensions.
            bool f1[1000][1000] = {0}, f2[1000][1000] = {0};
            f1[0][0] = 1;

            for (int i = 0; i < n3; i++)
            {
                memset(f2, 0, sizeof(f2));
                for (int j = 0; j < min(i+1, n1); j++)
                    if (s3[i] == s1[j])
                        // Note here is |= rather than = because there may be multiple way to make up the string and we are only trying one method
                        f2[j+1][(i+1)-(j+1)] |= f1[j][(i+1)-(j+1)];
                // We cannot say we cannot make up the string because we failed this one method
                //else
                //    f[i+1][j+1][(i+1)-(j+1)] = 0;
                for (int j = 0; j < min(i+1, n2); j++)
                    if (s3[i] == s2[j])
                        f2[(i+1)-(j+1)][j+1] |= f1[(i+1)-(j+1)][j];
                memcpy(f1, f2, sizeof(f2));

            }
            return f1[n1][n2];
        }
};
