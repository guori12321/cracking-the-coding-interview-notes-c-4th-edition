class Solution {
public:
    bool ans = false;
    void DFS(TreeNode* root, int sum, int count)
    {
        if (ans == true)
            return;
        if (root == NULL)
            return;
        if (root->left == NULL && root->right == NULL)
        {
            if (count + root->val == sum)
                ans = true;
            return;
        }
        DFS(root->left, sum, count + root->val);
        DFS(root->right, sum, count + root->val);
        return;
    }
    bool hasPathSum(TreeNode* root, int sum) {
        DFS(root, sum, 0);
        return ans;
    }
};
