// Convert the problem to be two sum
class Solution {
public:
    vector< vector<int> > threeSum(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        vector< vector<int> > ans;

        int n = nums.size();
        for (int i = 0; i < n-2; i++)
        {
            // To skip the same triple and speed up the algorithm, otherwise we would TLE
            if (i > 0 && nums[i] == nums[i-1])
                continue;
            int l = i+1;
            int r = n-1;
            while (l < r)
            {
                if (nums[l] + nums[r] == 0 - nums[i])
                {
                    if (ans.size() == 0 || (ans.size() > 0 && (ans[ans.size()-1][0] != nums[i] || ans[ans.size()-1][1] != nums[l]) || ans[ans.size()-1][2] != nums[r]))
                    {
                        vector<int> t;
                        t.push_back(nums[i]);
                        t.push_back(nums[l]);
                        t.push_back(nums[r]);
                        ans.push_back(t);
                    }
                    l++;
                    r--;
                }
                else if (nums[l] + nums[r] < 0 - nums[i])
                    l++;
                else
                    r--;
            }
        }
        return ans;
    }
};
