// As we are allowed to use O(h) space, so we can claim a stack to store the ancestor of the current node.
// A trick here is, when we meet a node with a left child, how can we judge if we have visited this left child before? We can keep a visit hashmap, but it would use O(n) memory (larger than our O(h) limit), or we can modify the BST (e.g. set the value of visited nodes to INT_MIN), but I don't want to do the modification.
// Instead, as this is a BST, and we are visiting in a increasing order, a int current showing the largest number we've ever visited would help. If the value of the left child is smaller than the current largest number visited, that means we have visited the left child before and we can skip it.
// However, this assume that the value of the left child cannot be the same as the parent, otherwise we would skip the left child as visited.
// Here we assume that the numbers that is equal to the parent is the right child.
class BSTIterator {
public:
    // stack is the type name, in the later code I'm always confusing and use it as a variable name
    stack<TreeNode*> s;
    int current;

    BSTIterator(TreeNode *root) {
        s.push(root);
        current = -9999999;
    }

    /** @return whether we have a next smallest number */
    bool hasNext() {
        // We have to judge if the top is NULL. The root may be NULL after all.
        while (! s.empty() && s.top() == NULL)
            s.pop();
        if (! s.empty())
                return true;
        return false;
    }

    /** @return the next smallest number */
    int next() {
        // Here we need to judge if s.top()->val <= current. If we just judge the < , then, we will visit the same left child again and again because we don't increase the current value.
        while (!s.empty() && (s.top() == NULL || (s.top() != NULL && s.top()->val <= current) ) )
            s.pop();
        TreeNode* t = s.top();
        // A typical grammar typo is to compare t->left <= current rather than t->left->val <= current
        if (t->left == NULL || (t->left != NULL && t->left->val <= current))
        {
            s.pop();
            if (t->right != NULL)
                s.push(t->right);
            current = t->val;
            return t->val;
        }
        else
        {
            s.push(t->left);
            return next();
        }
    }
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */
