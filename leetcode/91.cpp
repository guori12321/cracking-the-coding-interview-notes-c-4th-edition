// Note that '0' cannot be decoded
class Solution {
public:
    int numDecodings(string s) {
        int f[10000] = {0};
        int n = s.size();
        if (n == 0)
            return 0;

        f[0] = int(s[0] != '0');
        if (n == 1)
            return f[0];

        f[1] = f[0] * int(s[1] != '0') +  int(s[0] != '0' && stoi(s.substr(0, 2)) <= 26);

        for (int i = 2; i < n; i++)
        {
            f[i] = f[i-1]*int(s[i] != '0') + f[i-2] * int(s[i-1] != '0' && stoi( s.substr(i-1, 2)) <= 26 );
        }
        return f[n-1];
    }
};
