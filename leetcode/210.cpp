// A typical topology sorting.
class Solution {
public:
    vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {
        vector<int> ans;

        if (numCourses == 0)
            return ans;

        vector<vector<int> > post(numCourses);
        vector<int> preNum(numCourses, 0);
        int totPre = prerequisites.size();

        for (int i = 0; i < totPre; i++)
        {
            pair<int, int> p = prerequisites[i];
            // The second one is the prerequisite, which is different from general description
            post[ p.second ].push_back(p.first);
            preNum[p.first]++;
        }

        int unDone = numCourses;
        // Here we use the number of undone courses as the condition in the while loop. If we use the number of unsatisfied prerequisites we would need to deal with all the remaining courses that has not been added into ans outside the loop.
        while (unDone > 0)
        {
            bool flag = true;
            for (int i = 0; i < numCourses; i++)
                if (preNum[i] == 0)
                {
                    flag = false;
                    preNum[i] = -1;
                    ans.push_back(i);
                    unDone--;
                    for (int j = 0; j < post[i].size(); j++)
                        preNum[ post[i][j] ]--;
                }
            // Here we return the empty array and note that at this time the ans array may not be empty
            if (flag)
                return vector<int>(0);
        }

        return ans;
    }
};
