// At the beginning I thought there might be many cases: the two triangles may overlap at the four corners, or inside, or one side...
// But, as this is a 2D problem, how about the 1D problem? We can get the length of the overlap edge, and then, get the overlap area.
// And, the 1D problem is easy: only 3 cases!
class Solution {
public:
    vector<int> overlap( int A, int B, int C, int D)
    {
        vector<int> ans;
        ans.push_back(0);
        ans.push_back(0);

        if (A > C)
        {
            int t = A;
            A = C;
            C = t;
            t = B;
            B = D;
            D = t;
        }
        if (C > B)
            return ans;
        ans[0] = C;
        ans[1] = min(B, D);
        return ans;
    }
    int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        long long area = (C-A)*(D-B) + (G-E)*(H-F);
        vector<int> o1 = overlap(A, C, E, G);
        vector<int> o2 = overlap(B, D, F, H);
        return area - (o1[1] - o1[0]) * (o2[1] - o2[0]);
    }
};
