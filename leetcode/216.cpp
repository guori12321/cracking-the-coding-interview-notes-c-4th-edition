class Solution {
public:
    bool visited[10];
    vector< vector<int> > ans;
    void DFS(int d, int k, int n, int last, int sum)
    {
        if (last > 9)
            return;
        if (sum + last > n)
            return;
        // Note that we can use 1 to 9, rather than 0 to 9, so here we hash 1..9 to 0..8
        visited[last - 1] = true;
        // here is k-1 because we will use last, and last would be the dth number
        if (d == k-1 && sum + last == n)
        {
            vector<int> t;
            for (int i = 0; i < last; i++)
                if (visited[i])
                // Because the offset in visited, here is i+1
                    t.push_back(i+1);
            ans.push_back(t);
            return;
        }
        DFS(d+1, k, n, last + 1, sum + last);
        visited[last - 1] = false;
        // Note that here we didn't use the number last, so, the depth is still d rather than d+1
        DFS(d, k, n, last + 1, sum);
        return;
    }

    vector<vector<int>> combinationSum3(int k, int n) {
        DFS(0, k, n, 1, 0);
        return ans;
    }
};

