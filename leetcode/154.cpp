class Solution {
public:
    int findLocalMin( vector<int> & nums, int l, int r)
    {
        int n = nums.size();
        if (l > r || l > n - 1 || r < 0)
            return 999999;
        // Note that as we don't deal with nums[l] == nums[r] in the later codes, we need to judge if l == r, otherwise there would be infinite loop
        if (l == r)
            return nums[l];

        if (nums[l] < nums[r])
            return nums[l];
        // If the right side is smaller, we cannot say the right side is the smallest, for instance, in [5, 1, 3] 3 is smaller than 5 but 1 is the smallest
        //if (nums[r] < nums[l])
        //  return nums[r];
        int m = (l + r) / 2;
        return min( findLocalMin(nums, l, m), findLocalMin(nums, m+1, r) );
    }

    int findMin(vector<int>& nums) {
        return findLocalMin(nums, 0, nums.size()-1);
    }
};
