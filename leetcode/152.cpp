// It is not necessary to DP...It's just greedy algorithm
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int n = nums.size();
        if (n == 0)
            return 0;

        int lastMax = nums[0], lastMin = nums[0], ans = nums[0];
        for (int i = 1; i < n; i++)
        {
            // As lastMax is updated when we update lastMin, we need to store the lastMax value to a buffer otherwise the lastMin will be deal with the new lastMax
            int tMax = lastMax;
            lastMax = max(nums[i], max( nums[i]*lastMax, nums[i]*lastMin ) );
            lastMin = min(nums[i], min( nums[i]*tMax, nums[i]*lastMin) );
            ans = max(ans, lastMax);
        }
        return ans;
    }
};
