// Write pseudo code first to explain the main idea of the program, run an example, and then writing
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        ListNode* vh = new ListNode(-99999);
        vh->next = head;

        stack<ListNode*> s;
        ListNode* p1 = vh, *p2 = vh;

        for (int i = 0; i < m-1; i++)
            p1 = p1->next;
        p2 = p1;

        for (int i = m; i <= n; i++)
        {
            p2 = p2->next;
            s.push(p2);
        }
        p2 = p2->next;
        while (! s.empty() )
        {
            p1->next = s.top();
            s.pop();
            p1 = p1->next;
        }
        p1->next = p2;

        return vh->next;
    }
};
