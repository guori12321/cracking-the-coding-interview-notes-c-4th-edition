// This is NOT a math problem. It is a basic SEARCH problem.
class Solution {
public:
    vector<string> ans;
    string s = "";
    void DFS(int l, int r, int n)
    {
        if (l == n && r == n)
        {
            ans.push_back(s);
            return;
        }

        if (l < n)
        {
            s[l+r] = '(';
            DFS(l+1, r, n);
        }

        if (r < l)
        {
            s[l+r] = ')';
            DFS(l, r+1, n);
        }
        return;
    }
    vector<string> generateParenthesis(int n) {
        for (int i = 0; i < n*2; i++)
            s += " ";
        DFS(0, 0, n);
        return ans;
    }
};
