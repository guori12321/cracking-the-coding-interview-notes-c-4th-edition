// At the beginning I thought we can just compare every two numbers, and compare the first digits.
// For instance, 91 is larger than 832, because 9 is larger than 8.
// But the tricky part is if one number is the prefix of another. Just as 12 is the prefix of 121, so, should we think 12 is larger or 121 is larger?
// One straight idea would be we treat 12 as 120 and then compare it with 121.So we think 121 is larger than 12 (120), so we would return 12112, but it is smaller than 12 121.
// Then, I think we can solve this by comparing the circular version of the prefix. In the above example, we can treat 12 as 121 (we fill 1, 2, 1, 2...until we get enough digits to compare it with the other number). But here 12 (121) and 121 would be the same. In this case I think we can treat the short one (12) as the largest. So we would still return 12 121, still the correct answer~
//
// But there is one count example (and only one): 8308 and 830. If we think 830 is larger and return 830 8308 then we get the wrong answer: 8308 830 would be larger! And if we think 830 is smaller we would make mistake in the 12 and 121 example.
//
// So I think the above algorithm would not work. A easier and briefer way would be to just attach the two numbers, and then judge which way is better: to attach them directly or swap first and then attach?
// In the above example, we just compare 8308 830 and 830 8308. As the 8308 830 is larger, we think 8308 is larger. And it works for the 12 121 example! And the code is briefer.

class Solution {
public:
    int getNumDigits(int x)
    {
        int t = 0;
        while (x > 0)
        {
            x /= 10;
            t++;
        }
        return max(1, t);
    }

	bool isLarger(int ox, int oy)
	{
	    long long x = ox, y = oy;
	    int dx = getNumDigits(x);
	    int dy = getNumDigits(y);
	    for (int i = 0; i < dy; i++)
	        x *= 10;
	    x += oy;
	    for (int i = 0; i < dx; i++)
	        y *= 10;
	    y += ox;

	    if (x >= y)
	        return false;
	    else return true;
	}

    string largestNumber(vector<int>& nums) {
    	string ans = "";
    	int n = nums.size();

    	for (int i = 0; i < n; i++)
    	{
    		for (int j = i+1; j < n; j++)
    			if (isLarger(nums[i], nums[j]) )
    			{
    				int t = nums[i];
    				nums[i] = nums[j];
    				nums[j] = t;
    			}
    		if (ans.size() > 0 || nums[i] > 0)
                ans += to_string(nums[i]);
    	}

    	if (ans == "")
    		return "0";
    	else
    		return ans;
    }
};

