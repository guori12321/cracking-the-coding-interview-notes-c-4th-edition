class Solution {
public:
    vector< vector<int> > ans;
    vector<int> path;

    void DFS(TreeNode* root, int count, int sum)
    {
        if (root == NULL)
                return;
        path.push_back(root->val);
        count += root->val;
        if (root->left == NULL && root->right == NULL && count == sum)
            ans.push_back(path);
        else
        {
            DFS(root->left, count, sum);
            DFS(root->right, count, sum);
        }
        path.erase(path.end() - 1);
        return;
    }

    vector<vector<int>> pathSum(TreeNode* root, int sum) {
        DFS(root, 0, sum);
        return ans;
    }
};
