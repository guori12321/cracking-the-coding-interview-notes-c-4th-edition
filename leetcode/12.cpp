// A little trick: as we need to treat numbers like 900, 500, or 4 specially, we can add them to the number list and enumerate them.
class Solution {
public:
    string intToRoman(int num) {
        string ans = "";
        int nums[13] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        string symbols[13] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        while (num > 0)
        {
            for (int i = 0; i < 13; i++)
                if (num / nums[i] > 0)
                {
                    ans += symbols[i];
                    num -= nums[i];
                    // Remember to break here
                    break;
                }
        }
        return ans;
    }
};
