/* Note that the input may be empty!
 *
 */
class Solution {
public:
    int maxProfit(vector<int>& prices) {
    // Special treat to the empty input
    if (prices.size() == 0)
        return 0;
            int ans = 0;
    int p = prices[0];
    for (int i = 1; i < prices.size(); i++)
    {
        if (prices[i] < prices[i-1])
        {
        ans += prices[i-1] - p;
        p = prices[i];
        }
    }
    ans += prices[ prices.size() - 1] - p;
    return max(ans, 0);
    }
};


