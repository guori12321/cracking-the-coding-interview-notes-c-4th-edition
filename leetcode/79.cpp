class Solution {
public:
    bool visited[1000][1000];
    int dir[4][2] = { {-1, 0}, {1, 0}, {0, 1}, {0, -1} };
    bool search(vector<vector<char>>& board, string word, int x, int y)
    {
        if (word == "")
            return true;
        for (int i = 0; i < 4; i++)
        {
            int nx = x + dir[i][0];
            int ny = y + dir[i][1];
            if (nx >= 0 && nx < board.size() && ny >= 0 && ny < board[0].size() && visited[nx][ny] == 0 && board[nx][ny] == word[0])
            {
                visited[nx][ny] = 1;
                if (search(board, word.substr(1, word.size()-1), nx, ny) )
                    return true;
                visited[nx][ny] = 0;
            }
        }
        return false;
    }

    bool exist(vector<vector<char>>& board, string word) {
        bool ans = false;
        int m = board.size();
        if (m == 0)
            return false;
        int n = board[0].size();
        if (n == 0)
            return false;
        if (word == "")
            return true;
        memset(visited, 0, sizeof(visited) );

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
            {
                if (board[i][j] == word[0])
                {
                    visited[i][j] = 1;
                    if ( search(board, word.substr(1, word.size()-1), i, j) )
                        return true;
                    visited[i][j] = 0;
                }
            }

        return false;
    }
};
