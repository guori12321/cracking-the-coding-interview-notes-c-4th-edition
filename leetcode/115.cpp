// The problem description is very confusing. As the first reference said, "Clearly, the 'distinct' here mean different operation combination, not the final string of subsequence. Otherwise, the result is always 0 or 1."
//
// This is a very classical DP problem. And we can optimize the space because only the last row is used during the DP computing.
//
// Reference: http://www.programcreek.com/2013/01/leetcode-distinct-subsequences-total-java/
//
// The reference for space optimization: http://www.cs.cmu.edu/~yandongl/distinctseq.html
class Solution {
public:
    int numDistinct(string s, string t) {
        int f[10000] = {0};
        int m = s.size();
        int n = t.size();

        f[0] = 1;
        for (int i = 1; i <= m; i++)
        {
            int last = 1, tlast = 1;
            for (int j = 1; j <= n; j++)
            {
            	tlast = f[j];
                if (s[i-1] == t[j-1])
            	    f[j] += last;
                last = tlast;
            }
        }

	    return f[n];
    }
};
