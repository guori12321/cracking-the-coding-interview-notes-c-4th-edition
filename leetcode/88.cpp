class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        int p1 = m-1, p2 = n-1;
        int p = m+n-1;
        while (p >=0)
        {
            // Just to check if p1 or p2 is smaller than zero.
            // Note here as we compare p1 and p2, we need to assure that p1 and p2 are both larger or equal to 0
            if ( (p2 < 0) || (p1>= 0 &&p2 >= 0 && nums1[p1] > nums2[p2]) )
            {
                nums1[p] = nums1[p1];
                p1--;
                p--;
            }
            else{
                nums1[p] = nums2[p2];
                p2--;
                p--;
            }
        }
        return;
    }
};

