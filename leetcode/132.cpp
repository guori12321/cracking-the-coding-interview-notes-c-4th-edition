// Reference: http://fisherlei.blogspot.com/2013/03/leetcode-palindrome-partitioning-ii.html
// My intuitive idea is to use a 2D DP. However as the 2D DP here we have three level of for loop, the time would be O(n^3). As this is a string problem we can fix the end of all the substring to be the end of the string, so we can get the 1D DP one (In this problem we don't care how we split words in the middle of the string, however some other problems does and in that case we can only 2D DP).
// Note that in the 1D DP we still need O(n^2) memory to store if s[i..j] is a palindrome, and yes we can maintain this boolean array while we do the DP.

// 1D DP
class Solution {
    public:
        int minCut(string s) {
            int n = s.size();
            if (n <= 1)
                return 0;
            if (n == 2)
            {
                if (s[0] == s[1])
                    return 0;
                else
                    return 1;
            }

            int f[500][500] = {0};
            for (int i = 0; i < n; i++)
                f[i][i] = 0;
            for (int i = 0; i < n-1; i++)
            {
                if (s[i] == s[i+1])
                    f[i][i+1] = 0;
                else
                    f[i][i+1] = 1;
            }
            for (int i = 2; i < n; i++)
                for (int j = 0; j < n - i; j++)
                {
                    if (s[j] == s[j+i-1] && f[j-1][j+i-1-1] == 0)
                        f[j][j+i-1] = 0;
                    else
                    {
                        int t = 99999;
                        for (int k = j; k < j+i-1; k++)
                            t = min(t, f[j][k] + f[k+1][j+i-1]);
                        f[j][j+i-1] = t;
                    }
                }
            return f[0][n-1];
        }
};

// 2D DP
class Solution {
    public:
        int minCut(string s) {
            int n = s.size();
            if (n <= 1)
                return 0;
            if (n == 2)
            {
                if (s[0] == s[1])
                    return 0;
                else
                    return 1;
            }

            int f[10000] = {0};
            bool hash[2000][2000] = {0};

            for (int i = 0; i < n; i++)
            {
                f[i] = 999999;
                hash[i][i] = true;
            }

            for (int i = 0; i < n-1; i++)
            {
                if (s[i] == s[i+1])
                    hash[i][i+1] = true;
            }

            f[n-1] = 0;
            for (int i = n-2; i >= 0; i--)
            {
                if (s[i] == s[n-1] && (hash[i][n-1] || hash[i+1][n-2]) )
                {
                    f[i] = 0;
                    hash[i][n-1] = 1;
                }
                for (int j = i; j < n-1; j++)
                {
                    if (s[i] == s[j] && (hash[i][j] || hash[i+1][j-1]))
                    {
                        hash[i][j] = 1;
                        f[i] = min(f[i], 1+f[j+1]);
                    }
                }
            }
            return f[0];
        }
};
