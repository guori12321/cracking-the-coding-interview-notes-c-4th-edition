class Solution {
public:
    vector<int> spiralOrder(vector<vector<int> >& matrix) {
        vector<int> ans;
        if (matrix.size() == 0)
            return ans;
        // Special case of only one row or one column
        if (matrix[0].size() == 0)
            return ans;
        int m = matrix.size();
        int n = matrix[0].size();

        if (m == 1)
           return matrix[0];
        if (n == 1)
        {
            for (int i = 0; i < m; i++)
                ans.push_back(matrix[i][0]);
            return ans;
        }

        for (int i = 0; i < ( min(m,n)+1)/2; i++)
        {
            for (int column = i; column < n-i; column++)
                ans.push_back( matrix[i][column]);

            for (int row = i+1; row < m-i-1; row++)
                ans.push_back( matrix[row][n-1-i] );

            // Note that there may be only one column left
            for (int column = n-i-1; (m-i-1>i) && column >= i; column--)
                ans.push_back( matrix[m-1-i][column]);

            // Or only one row left; Note that if you miss the i<n-1-i condition you can still AC on Leetcode because lack of test cases.
            for (int row = m-i-1-1; i < n-1-i && row > i; row--)
                ans.push_back( matrix[row][i] );
        }
        return ans;
    }
};
