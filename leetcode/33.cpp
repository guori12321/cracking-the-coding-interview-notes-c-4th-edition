// Related to 81
// This is just a classical binary search question.
// However it involves 4 branches rather than just 2
// The good news is that the 4 branches are just at the same level so one if and 3 else is enough

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int n = nums.size();
        if (n == 0)
            return -1;

        int l = 0, r = n-1;
        while (l <= r)
        {
            int m = (l+r) / 2;
            if (nums[m] == target)
                return m;

            // Numbers in the left part is increasing and the target is in this area
            // Note that target may be at nums[l], so just target >= nums[l]
            if (nums[l] < nums[m] && target >= nums[l] && target <= nums[m])
                r = m - 1;
            else
            // Numbers in the right part is increasing and the target is in this area
            // Note that target may be at nums[l], so just target >= nums[l]
                if (nums[m] < nums[r] && target >= nums[m] && target <= nums[r] )
                    l = m + 1;
            else
            // Numbers in the left part is divided into two increasing parts and the target is in this area
                if (nums[l] > nums[m] && (target >= nums[l] || target < nums[m]) )
                   r = m - 1;
            // Numbers in the right part is divided into two increasing parts and the target is in this area
             else
                   l = m + 1;
        }
        return -1;
    }
};
