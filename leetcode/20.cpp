class Solution {
public:
    bool isValid(string s) {
        stack<char> st;
        map<char, char> m;
        m[')'] = '(';
        m[']'] = '[';
        m['}'] = '{';
        set<char> left;
        left.insert('(');
        left.insert('[');
        left.insert('{');

        int n = s.size();
        for (int i = 0; i < n; i++)
        {
            if (left.find(s[i]) != left.end())
            {
                st.push(s[i]);
            }
            else
                if (st.empty())
                    return false;
                else
                {
                    char t = st.top();
                    st.pop();
                    // Check here: weather m uses the left brackets or right ones as keys.
                    // The find() in map is as follows:
                    // if (m.find(t)->second != s[i])
                    if (m.find(s[i])->second != t)
                        return false;
                }
        }

        // Remember to check the remaining items
        if (st.empty())
            return true;
        return false;
    }
};
