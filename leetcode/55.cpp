// Watch out the boundary
class Solution {
public:
    bool canJump(vector<int>& nums) {
    int n = nums.size();
    if (n == 0)
        return true;
    // This problem asks the last index, rather than go out of the range, so, if the vector is [0] we need to return true.
    //if (nums[0] == 0)
    //  return false;

    int l = 0, r = nums[0];
    // We should return true if we can reach the last index, so here is n-1
    while (r < n-1)
    {
        // if l == r, then, only if nums[l] == 0 should we return false, i.e. next time l++ and then l would be larger than r.
        if (l > r)
            return false;
        r = max(r, l + nums[l]);
        l++;
    }
    return true;
    }
};
