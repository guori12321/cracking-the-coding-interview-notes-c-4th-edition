// The most obvious and easiest way
class Solution {
public:
    int reverse(int x) {
        if (x == INT_MIN)
            return 0;
        bool negative = false;
        if (x < 0)
        {
            negative = true;
            x = -x;
        }

        long long t = 0;
        long long mask = 1;
        while (x / mask > 0)
            mask *= 10;

        while (x > 0)
        {
            mask /= 10;
            t += (x % 10) * mask;
            x /= 10;
        }

        if (t > INT_MAX || t < INT_MIN)
            return 0;
        if (negative)
            return -t;
        return t;
    }
};

// We can easily do it by converting to string
class Solution {
public:
    void rev(string& s, int l, int r)
    {
        for (int i = l; i <= (l+r)/2; i++)
        {
            char t = s[i];
            s[i] = s[r-(i-l)];
            s[r-(i-l)] = t;
        }
        return;
    }
    int reverse(int x) {
        string s = to_string(x);
        if (s[0] == '-')
            rev(s, 1, s.size() - 1);
        else
            rev(s, 0, s.size() - 1);
 long long t = 0;
 // The input range for s is int
    try{
        t = stoi(s);
    }
    catch (exception &e)
    {
        return 0;
    }
    if (t > INT_MAX || t < INT_MIN)
        return 0;
    return t;
    }
};

// Sometimes I just cannot come up with the easiest way, and try a very very hard method:  to get the left and right digit, and then swap them as we did in an array.
class Solution {
public:
 int reverse(int x) {
    // Because -INT_MIN == -INT_MIN, it's the only special case
    if (x == INT_MIN)
        return 0;
    if ( (x >= 0 && x < 10) || (x < 0 && x > -10) )
        return x;
        bool negative = false;
    if (x < 0)
    {
        negative = true;
        x = -x;
    }
    long long xx = x;
    int n = 1;
    long long t = 10;
    while (xx / t > 0)
    {
        n++;
        t *= 10;
    }

    t /= 10;
    long long lowt = 10;
    for (int i = 0; i < n / 2; i++)
    {
        long long left = xx / t % 10;
        long long right = xx % lowt / (lowt/10);
        xx -= left*t + right*(lowt/10);
        xx += right * t + left*(lowt / 10);

        t /= 10;
        lowt *= 10;
    }

    if (xx > INT_MAX)
        return 0;
    if (xx < INT_MIN)
        return 0;

    if (negative)
    return -xx;
    return xx;
}
};
