class Solution {
public:
    bool hasLength(ListNode* root, int k)
    {
        ListNode* t = root;
        for (int i = 0; i < k && root != NULL; i++)
            root = root->next;
        if (root == NULL)
return false;
        return true;
    }
    ListNode* reverseKGroup(ListNode* head, int k) {
        if (k == 1)
            return head;
        ListNode *vh = new ListNode(-99999);
        vh->next = head;
        ListNode* p1 = vh;
        while ( hasLength(p1, k) )
        {
            // It's not necessary to move p2 inside the for loop, for movement p2->next to p1->next also moves p2 forward
            ListNode *p2 = p1->next;
            // To reverse k nodes, we just move k-1 times
            for (int i = 0; i < k-1; i++)
            {
                ListNode *t1 = p1->next;
                p1->next = p2->next;
                ListNode *t2 = p2->next->next;
                p2->next->next = t1;
                p2->next = t2;
            }
            p1 = p2;
        }
        return vh->next;
    }
};
