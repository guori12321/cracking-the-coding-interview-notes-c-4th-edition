class Solution {
public:
    vector<int> st;
    vector<string> ans;
    void DFS(TreeNode* root)
    {
        if (root == NULL)
            return;
        if (root->left == NULL && root->right == NULL)
        {
            string t = "";
            for (int i = 0; i < st.size(); i++)
                t += to_string(st[i]) + "->";
            t += to_string(root->val);
            ans.push_back(t);
            return;
        }

        st.push_back(root->val);
        DFS(root->left);
        DFS(root->right);
        st.pop_back();

        return;
    }

    vector<string> binaryTreePaths(TreeNode* root) {
        if (root == NULL)
            return ans;
        DFS(root);
        return ans;
    }
};
