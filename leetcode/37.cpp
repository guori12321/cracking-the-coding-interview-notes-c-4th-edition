// Though this is a basic searching, be careful when deal with the state recovery.

class Solution {
public:
struct point
{
    int x;
    int y;
    point(int a, int b) :x(a), y(b) {};
};

vector<point> todo;
bool finish = false;
int m, n;

bool check(vector<vector<char> >& board, int d, char c)
{
    int x = todo[d].x, y = todo[d].y;
    bool mask[10] = {0};
    mask[ c - '0' ] = true;
    for (int i = 0; i < m; i++)
    {
        if (i != x && board[i][y] != '.')
            if (mask[board[i][y] - '0'] == true)
                return false;
            else
                mask[board[i][y] - '0'] = true;
    }

    for (int i = 0; i < 10; i++)
        mask[i] = false;
    mask[ c - '0' ] = true;
    for (int i = 0; i < n; i++)
    {
        if (i != y && board[x][i] != '.')
            if (mask[board[x][i] - '0'] == true)
                return false;
            else
                mask[board[x][i] - '0'] = true;
    }

    for (int i = 0; i < 10; i++)
        mask[i] = false;
    mask[ c - '0' ] = true;
    // Here we need to time 3!!!
    for (int i = 3*(x/3); i < 3*(x/3) +3; i++)
        for (int j = 3*(y/3); j < 3*(y/3) +3; j++)
        {
            if ( (i != x || j != y) && board[i][j] != '.')
                if (mask[ board[i][j] - '0' ] == true)
                    return false;
                else
                    mask[ board[i][j] - '0' ] = true;
        }

    return true;
}

void DFS(vector<vector<char> >& board, int d)
{
    if (finish)
        return;
    if ( d == todo.size() )
    {
        finish = true;
        return;
    }

    for (int i = 1; i <= 9; i++)
    {
        if ( check(board, d, i+'0'))
        {
            board[todo[d].x][todo[d].y] = i + '0';
            DFS(board, d+1);
            //Here we need to judge before we recover the state. If we don't recover, we'll be influenced with the marks when we go back searching; if we always recover then we would recover to the original state after we get the correct answer.
            if (!finish)
                board[todo[d].x][todo[d].y] = '.';
        }
    }
    return;
}

void solveSudoku(vector<vector<char> >& board) {
    m = board.size();
    if (m == 0)
        return;
    n = board[0].size();
    if (n == 0)
        return;

    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            if (board[i][j] == '.')
                todo.push_back(point(i, j));

    DFS(board, 0);

    return;
}
};
