// Just hash it~
class Solution {
public:
    bool isAnagram(string s, string t) {
        unordered_map<char, int> m;

        if (s.size() != t.size())
            return false;
        int n = s.size();
        for (int i = 0; i < n; i++)
        {
            if (m.find(s[i]) == m.end())
                m[s[i]] = 1;
            else
                m[ s[i] ]++;

            if (m.find(t[i]) == m.end())
                m[ t[i] ] = -1;
            else
                m[ t[i] ]--;
        }

        for (unordered_map<char, int>::iterator it = m.begin(); it != m.end(); it++)
        {
            if (it->second != 0)
                return false;
        }
        return true;
    }
};
