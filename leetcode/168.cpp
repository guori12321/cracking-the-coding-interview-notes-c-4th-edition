// Be careful that 1 is A, rather than 0
// So we need to map it by using n-1
class Solution {
public:
    string convertToTitle(int n) {
    string ans;

    while (n != 0)
    {
        ans = char(((n-1) % 26) + 'A') + ans;
        n = (n-1) / 26;
    }
    return ans;
    }
};

