// Two stacks. One for the value, one for the minimal value.
class MinStack {
public:
    stack<int> st;
    stack<int> minSt;
    MinStack()
    {
        minSt.push(INT_MAX);
    }
    void push(int x) {
        st.push(x);
        // Here we push x when it is the same as the minSt.top(), otherwise there may be several same minimal value that we don't know if we should pop it out or not.
        if (x <= minSt.top())
            minSt.push(x);
    }

    void pop() {
        int t = st.top();
    st.pop();
    if (minSt.top() == t)
        minSt.pop();
    }

    int top() {
        return st.top();
    }

    int getMin() {
        return minSt.top();
    }
};
