// Reference: http://fisherlei.blogspot.com/2012/12/leetcode-largest-rectangle-in-histogram.html
// At the beginning I thought we could enumerate the first and last index to compute the area of the rectangle, and we still need to enumerate the internal items to find the smallest one inside, which would take O(n^3) time. However, we can only enumerate the first and last index and maintain a current minimal height when we enumerate the last index, so we just need O(n^2) time.
// My other thought is to enumerate the minimal middle one, and try to find its left and right boundary, which is O(n^2).
// As the reference says, we can prune the above algorithm by only enumerating left boundary when the right one is in a peak: we can always do better and keep going right until we reach a peak. This is still O(n^2).
// So, there are lots of way to enumerate: enumerate the left/right boundary or minimal item. Some of them are better than others and can be pruned easily. If you have trouble enumerating, just think if there are other possible ways.

// The best solution here is to use a stack. Though this seems tricky, I’m sure this is a classical method (everything in leetcode is…), and I’ve met it somewhere else. The meaning of the stack is to “merge” the previous bars: if we keep the current bar then all the previous bars that are larger than the current one can be treated as the same high as the current one!

class Solution {
public:
    int largestRectangleArea(vector<int>& height) {
        height.push_back(0);
        int n = height.size();

        stack<int> s;
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            if (!s.empty() && height[i] <= height[ s.top()])
            {
                int r = i-1;
                while( !s.empty() && height[s.top()] >= height[i])
                {
                    int h = height[ s.top() ];
                    s.pop();
                    int l = 0;
                    if (!s.empty())
                        l = s.top() + 1;
                    ans = max(ans, (r-l+1)*h );
                }
            }
            // And the last step is to push i
            s.push(i);
        }

        return ans;
    }
};
