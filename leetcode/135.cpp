// Reference: http://yucoding.blogspot.com/2014/02/leetcode-question-candy.html , however this reference doesn't say how to proof the correctness of this method
//
// My first insight is to scan only once, however there may lead to negative values and I don't know how to fix it. My second idea is to sort and then we start from the low rating to high rating, but the sorting takes at least O(n logn) time. My third thought is to scan twice, one from left to right one the other way around, but I'm now sure if this works.
//
// In fact, scan twice (one from left to right and one right to left) works. Assume that the ratings are heights of mountains, that means sometimes the mountains may go higher and sometimes go lower.
// We can safely assume that the number of candy at the bottom of the mountains would be the least 1 because it doesn't need to be higher.
// And then, let's consider the two sides of the mountain. Assume that the left side of the mountain is going higher and right side go lower. The left side of the mountain is not related to other mountains or the right side of the mountain: they are separated by the top or bottom. So, when we scan from left to right, we can solve the number of candies on the left side, and when we go from right to left we deal with the right side. So the both side would be done. A little problem occurs where the two sides meet: the top of the mountain. We can just take the necessary maximal value to guarantee the top is toper than its left and right neighbors.
// Now, all done!
class Solution {
    public:
        int candy(vector<int>& ratings) {
            int n = ratings.size();
            vector<int> l(n, 1), r(n, 1);

            for (int i = 1; i < n; i++)
                if (ratings[i-1] < ratings[i])
                    l[i] = l[i-1] +1;
            for (int i = n-2; i >= 0; i--)
                if (ratings[i] > ratings[i+1])
                    r[i] = r[i+1] + 1;

            int ans = 0;
            for (int i = 0; i < n; i++)
                ans += max(l[i], r[i]);
            return ans;
        }
};
