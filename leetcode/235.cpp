/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* ans;
    // In this function we assume that p->val < q->val. However this is not guaranteed in the problem description, so we need to check if it is so in the main function.
    bool check(TreeNode* root, TreeNode* p, TreeNode* q, int l, int r)
    {
        if (root == NULL)
            return false;
        if (p->val > l && p->val <= root->val && q->val > l && q->val <= root->val)
            return true;
        if (p->val > l && p->val <= root->val && q->val >= root->val && q->val < r)
            return true;
        if (p->val >= root->val && p->val < r && q->val >= root->val && q->val < r)
            return true;
        return false;
    }

    void find(TreeNode* root, TreeNode*p, TreeNode* q, int l, int r)
    {
        if (root == NULL)
            return;
        if (check(root, p, q, l, r) )
        {
            if ( !( check(root->left, p, q, l, root->val) || check(root->right, p, q, root->val, r) ) )
            {
                ans = root;
                return;
            }
            else
            {
                find(root->left, p, q, l, root->val);
                find(root->right, p, q, root->val, r);
            }
        }

        return;
    }

    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        ans = NULL;
        if (p->val > q->val)
        {
            TreeNode* t = p;
            p = q;
            q = t;
        }
        find(root, p, q, -99999, 999999);
        return ans;
    }
};
