// At the beginning I focused on the tree structure, and I compare the root with its left and right child. However there is a problem: if the value of root is smaller than its left child or larger than its right child, you won’t know if the root is in the wrong place or the left/right child or the root and one of the children are in the wrong place.
// As the O(n) space complexity method, the correct answer also focus on the inorder searching. And then, everything is clear.
// The key here is to discuss O(n) space complexity method thoroughly first, and then discuss a better way.

class Solution {
public:
    TreeNode* prev = NULL, *curr = NULL, *last = NULL;
    TreeNode* p = NULL;

    // p is the previous node in the inorder traversing.
    // We need a global p and there is no better way to get the previous node in the inorder traversing.
    void DFS(TreeNode* root)
    {
        if (root == NULL)
            return;

        DFS(root->left);

        if (p != NULL && root->val < p->val)
            if (curr == NULL)
            {
                prev = p;
                curr = root;
            }
            else
                last = root;
        // root is visited and we set it to be the previous node in the inorder traversing.
        p = root;
        DFS(root->right);
        return;
    }

    void recoverTree(TreeNode* root) {
        p = NULL;
        DFS(root);
        if (last != NULL)
        {
            int t = prev->val;
            prev->val = last->val;
            last->val = t;
        }
        else
        {
            int t = prev->val;
            prev->val = curr->val;
            curr->val = t;
        }

        return;
    }
};
