// If the given array is sorted, then this method would be more clear.
// For the 3 sum closest, first we enumerate the first item, and then, try to find the other two items in a linear time search.
// The time complexity therefore is O(n^2).
class Solution {
public:
    int find(vector<int>& nums, int left, int target)
    {
        int l = left, r = nums.size() - 1;
        int ans = 9999999;
        while (l < r)
        {
            int t = nums[l] + nums[r];
            if ( abs(t - target) < abs( ans - target) )
                ans = t;
            if (t < target)
                l++;
            else r--;
        }
        return ans;
    }

    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int n = nums.size();
        int ans = 9999999;
        for (int i = 0; i < n-2; i++)
        {
            int t = nums[i] +  find(nums, i+1, target - nums[i]);
            if ( abs(target - t) < abs( target - ans) )
                ans = t;
        }
        return ans;
    }
};
