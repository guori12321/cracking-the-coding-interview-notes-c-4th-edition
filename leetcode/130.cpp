// Note: DFS would Run Time Error, and I doubt that it is because the stack would overflow.
// This BFS works.
class Solution {
    public:
        void solve(vector<vector<char>>& board) {
            queue<pair<int, int> > q;
            int dir[4][2] = { {0, 1}, {0, -1}, {-1, 0}, {1, 0} };
            int m = 0, n = 0;

            m = board.size();
            if (m == 0)
                return;
            n = board[0].size();
            if (n == 0)
                return;

            for (int i = 0; i < m; i++)
            {
                q.push(make_pair(i, 0) );
                q.push(make_pair(i, n-1) );
            }
            for (int j = 0; j < n; j++)
            {
                q.push(make_pair(0, j) );
                q.push(make_pair(m-1, j) );
            }

            while (!q.empty())
            {
                pair<int, int> p = q.front();
                q.pop();

                if (p.first >= 0 && p.first < m && p.second >= 0 && p.second < n
                        && board[p.first][p.second] == 'O')
                {
                    board[p.first][p.second] = '-';
                    for (int i = 0; i < 4; i++)
                        q.push(make_pair( p.first + dir[i][0], p.second+dir[i][1]) );
                }
            }

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    if (board[i][j] == 'O')
                        board[i][j] = 'X';
                    else
                        if (board[i][j] == '-')
                            board[i][j] = 'O';
            return;
        }
};


