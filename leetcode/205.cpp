// Note that ab and aa are NOT isomorphic.
// So we can add a visited bool array to judge if the t[i] is already mapped.
class Solution {
public:
    bool isIsomorphic(string s, string t) {
    bool visited[256] = {0};
        map<char, char> m;

    int n = s.size();
    for (int i = 0; i < n; i++)
    {
        // We judge if t[i] is mapped rather than s[i].
        if ( m.find(s[i]) == m.end() && visited[ t[i] ] == false )
        {
            m[ s[i] ] = t[i];
            visited[ t[i] ] = true;
        }
        else
            if ( m.find( s[i] ) == m.end() || ( m.find( s[i] ) != m.end() && m[ s[i] ] != t[i]) )
                return false;
    }

    return true;
    }
};
