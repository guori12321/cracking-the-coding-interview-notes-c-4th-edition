// For subset, a quick method is to use a bitmap and enumerate all ints from 0 to 2^n - 1.

class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> ans;
        int n = nums.size();
        // The problem description didn't say that nums is ordered (in fact, the test cases are NOT). So sort it in advance.
        sort(nums.begin(), nums.end());

        for (int i = 0; i < (1 << n); i++)
        {
            vector<int> t;
            for (int j = 0; j < n; j++)
            {
                if ( (i >> j) & 1 == 1)
                    t.push_back(nums[j]);
            }
            ans.push_back(t);
        }
        return ans;
    }
};

// A more general way is to do a DFS
class Solution {
public:
    bool hash[1000] = {0};
    vector< vector<int> > ans;

    void DFS(vector<int> & nums, int k, int n)
    {
        if (k == n)
        {
            vector<int> t;
            for (int i = 0; i < n; i++)
                if (hash[i])
                    t.push_back(nums[i]);
            ans.push_back(t);
            return;
        }
        hash[k] = true;
        DFS(nums, k+1, n);
        hash[k] = false;
        DFS(nums, k+1, n);
        return;
    }

    vector<vector<int>> subsets(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        DFS(nums, 0, nums.size());
        return ans;
    }
};
