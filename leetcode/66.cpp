// I don't know if we can use the reverse() in this case, because the difficulty seems to be "the most significant digit is at the head of the list", and if we use a linked list rather than a vector, then we cannot reverse it easily.

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
    reverse(digits.begin(), digits.end() );
        int carry = 0;
    int n = digits.size();
    digits[0]++;
    for (int i = 0; i < n; i++)
    {
        digits[i] += carry;
        carry = digits[i] / 10;
        digits[i] %= 10;
        if (carry == 0)
            break;
    }
    if (carry != 0)
        digits.push_back(1);
    reverse(digits.begin(), digits.end() );
    return digits;
    }
};
