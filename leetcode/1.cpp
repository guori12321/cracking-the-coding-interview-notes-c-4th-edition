/* We cannot use the same number twice, so just check if the $target - nums[i]$ is $nums[i]$ itself.
 */


class Solution {
public:
    vector<int> ans;
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int, int> loc;
        for (int i = 0; i < nums.size(); i++)
            loc[ nums[i] ] = i;
        for (int i = 0; i < nums.size(); i++)
            if ( loc.find( target - nums[i]) != loc.end() )
            {
                ans.push_back(i+1);
                ans.push_back( loc[ target-nums[i] ] + 1);
                return ans;
            }
        return ans;
    }
};
