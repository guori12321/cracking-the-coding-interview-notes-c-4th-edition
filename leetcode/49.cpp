// The problem description is ambiguous. We are expected to return all the strings that have at least one anagrams.
// For anagram, we can always sort them first.
class Solution {
public:
    vector<string> anagrams(vector<string>& strs) {
map<string, vector<string> > m;
int n = strs.size();

for (int i = 0; i < n; i++)
{
    string t = strs[i];
    sort(t.begin(), t.end() );
    m[t].push_back(strs[i]);
}

vector<string> ans, emptyVector;
for (map<string, vector<string> >::iterator it = m.begin(); it != m.end(); it++)
    if (it->second.size() > 1 )
        for (int i = 0; i < it->second.size(); i++)
            ans.push_back(it->second[i]);
if (ans.size() <= 1)
    return emptyVector;
return ans;
    }
};

// Use a hashmap to judge the anagram would TEL
class Solution {
public:
    vector<string> anagrams(vector<string>& strs) {
        map< map<char, int>, vector<string> > m;
        int n = strs.size();
        vector<string> ans, emptyVector;

        for (int i = 0; i < n; i++)
        {
                if (strs[i] == "")
                    continue;
            map<char, int> t;
            for (int j = 0; j < strs[i].size(); j++)
                t[ strs[i][j] ]++;
            m[t].push_back(strs[i]);
        }

        for (map< map<char, int>, vector<string> > ::iterator it = m.begin(); it != m.end(); it++)
    if (it->second.size() > ans.size() )
    ans = it->second;

        if (ans.size() == 1)
            return emptyVector;
        return ans;
    }
};
