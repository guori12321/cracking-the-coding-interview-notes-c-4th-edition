#include <iostream>
using namespace std;

int N = 52;

int main()
{
    int result[52] = {0};
    for (int i = 0; i < N; i++)
        result[i] = i;
    for (int i = 0; i < N; i++)
    {
        int r = rand() % (N-i);
        int t = result[i+r];
        result[i+r] = result[i];
        result[i] = t;
    }

    for (int i = 0; i < N; i++)
        cout << result[i] << '\t';
    cout << endl;
    return 0;
}
