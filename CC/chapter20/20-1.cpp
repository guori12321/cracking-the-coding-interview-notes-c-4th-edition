#include <iostream>
using namespace std;

int addDIY(int a, int b)
{
    if (b == 0)
        return a;

    return addDIY(a^b, (a&b) << 1);
}

int main()
{
    cout << addDIY(20, 7) << endl;
    return 0;
}
