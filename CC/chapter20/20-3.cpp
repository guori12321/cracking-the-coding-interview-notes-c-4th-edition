#include <iostream>
using namespace std;

int main()
{
    const int N = 20;
    int M = 5;
    int arr[N];
    for (int i = 0; i < N; i++)
        arr[i] = i;

    for (int i = 0; i < M; i++)
    {
        int r = rand() % (N-i);
        int t = arr[r + i];
        arr[r + i] = arr[i];
        arr[i] = t;
    }
    for (int i = 0; i < M; i++)
        cout << arr[i] << '\t';
    cout << endl;
    return 0;
}
