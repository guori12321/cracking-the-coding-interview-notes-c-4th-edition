#include <iostream>
using namespace std;

int main()
{
    int N = 26;
    int ans = 0;
    //The initial value is 5
    for (int i = 5; N/i > 0; i *= 5)
    {
        ans += N / i;
    }

    cout << ans << endl;

    return 0;
}
