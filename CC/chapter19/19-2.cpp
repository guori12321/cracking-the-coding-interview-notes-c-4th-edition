#include <iostream>
using namespace std;

int cheese[3][3] =
{
    {1, 0, 0},
    {0, 1, 0},
    {1, 1, 0}
};

bool check()
{
    for (int i = 0; i < 3; i++)
    {
        if (cheese[i][0] && cheese[i][1] && cheese[i][2])
            return true;
        if (cheese[0][i] && cheese[1][i] && cheese[2][i])
            return true;
    }
    if ( (cheese[0][0] && cheese[1][1] && cheese[2][2]) || (cheese[0][2] && cheese[1][1] && cheese[2][0]) )
        return true;
    return false;
}

int main()
{
    cout << check() << endl;
    return 0;
}
