#include <iostream>
using namespace std;

int main()
{
    string s = "RGGB", t = "YRGB";
    int N = 4;
    int mask = 0;
    for (int i = 0; i < N; i++)
        mask |= ( 1 << (s[i] - 'A') );

    int hit = 0, phit = 0;
    for (int i = 0; i < N; i++)
    {
        if (s[i] == t[i])
            hit++;
        else
            if ( mask & ( 1 << (t[i] - 'A') ))
                phit++;
}
    cout << hit << endl << phit;
    return 0;
}
