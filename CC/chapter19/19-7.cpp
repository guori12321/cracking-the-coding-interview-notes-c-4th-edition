#include <iostream>
using namespace std;
int N = 6;
int s[6] = {2, -8, 3, -2, 4, -10};
int f = -999999;
int ans = 0;

int main()
{
    for (int i = 0; i < N; i++)
    {
        f = max(f + s[i], s[i]);
        ans = max(ans, f);
    }

    cout << ans << endl;
    return 0;
}
