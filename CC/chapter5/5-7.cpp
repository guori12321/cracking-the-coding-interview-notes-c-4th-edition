#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> s;
    s.push_back(4);
    s.push_back(1);
    s.push_back(0);
    s.push_back(3);
    s.push_back(5);
    s.push_back(7);
    s.push_back(6);
    s.push_back(2);
    //ans = 2;

    int ans = 0;
    vector<int> odd, even;
    int bit = 0;

    while ( s.size() > 0)
    {
        odd.clear();
        even.clear();

        for (int i = 0; i < s.size(); i++)
            if ( (s[i] >> bit) & 1)
                odd.push_back(s[i]);
            else
                even.push_back(s[i]);

        if (even.size() <= odd.size())
        {
        ans |= 0 << bit;
        s = even;
        }
        else
        {
            ans |= 1 << bit;
            s =odd;
        }
        bit++;
    }
    cout << ans << endl;

    return 0;
}
