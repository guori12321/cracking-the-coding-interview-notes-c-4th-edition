#include <iostream>
using namespace std;

int main()
{
    string s = "9.25";
    string intPart = s.substr(0, s.find('.'));
    string decPart = s.substr(s.find('.'), s.size() - s.find('.'));

    string bintPart = "";
    int t = stoi(intPart);
    while (t != 0)
    {
        bintPart = to_string(t % 2) + bintPart;
        t /= 2;
    }

    string bdecPart = ".";
    double f = stod(decPart);
    while ( f > 0)
    {
        f  *= 2;
        bdecPart += to_string(int(f) % 2);
        if (int(f) % 2)
            f -= int(f) % 2;
    }
    cout << bintPart + bdecPart << endl;

    return 0;
}
