//Input: 31, 14
//Output: 2

#include <iostream>
using namespace std;

int main()
{
    int a = 31, b = 14;
    int ans = 0;
    for (int i = 0; i < 31; i++)
    {
        if ( ((a >> i) & 1) != ((b>>i) & 1))
            ans++;
    }

    cout << ans;
    return 0;
}
