#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    unsigned int s = 156;
    unsigned int large, small;
    unsigned mask = 0;

    int last1 = 0;
    //A while loop here can be briefer
    for (int i = 0; i < 32; i++)
        if ( ((s >> i) & 1) == 1 )
        {
            last1 = i;
            break;
        }

    large = s - (1 << last1);

    for (int i = last1 + 1; i < 32; i++)
        if (((s >> i) & 1) == 0)
        {
            large |= (1 << i);
            int count = 0;
            for (int j = 0; j < i; j++)
                if ( ((large >> j) & 1) == 1)
                    count++;
            large = (large >> i) << i;
            large |= (unsigned int)( (1 << count) - 1);
            break;
        }
    bitset<8> x(large);
    bitset<8> y(163);
    cout << "Large: \t" << x << endl;
    cout << "163:   \t" << y << endl;


    int last0 = 0;
    for (int i = 0; i < 32; i++)
    if ( ((s >> i) & 1) == 0 )
    {
        last0 = i;
        break;
    }

    small = s | (1 << last0);

    for (int i = last0 + 1; i < 32; i++)
        if (((s >> i) & 1) == 1)
        {
            small -= 1 << i;
            int count = 0;
            for (int j = 0; j < i; j++)
                if ( ((small >> j) & 1) == 1)
                    count++;
            small = (small >> i) << i;
            small |= (unsigned int)( ((1 << count) - 1) << (i - count) );
            break;
        }
    cout << "Small: " << small << endl;

    return 0;
}
