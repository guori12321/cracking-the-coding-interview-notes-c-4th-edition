#include <iostream>
using namespace std;

int setBetween(int n, int m, int i, int j)
{
    unsigned int t = m;
    unsigned int mask = 0xffffffff;
    mask = mask >> (j-i);
    m &= mask;
    m = m << i;
    return n | m;
}

int main()
{
    cout << setBetween(0b10000000000, 0b10101, 2, 6) << endl;
    return 0;
}
