# 5.1
Basic one. Remember that `m << i` won't change the value of `m`. Do `m = m << i` instead.

# 5.2
A few popular methods about string or C-style string:
* Convert string to int : `stoi()`.
* Convert string to double : `stod()`.
* Convert C-style string (`char *`) to int : `atoi()`.
* Convert C-style string (`char *`) to double : `atod()`.
* Convert int to string: `std::to_string(int)`.

To convert the decimal part, remember to double the float first (in the int part we mod the int part first rather than divide it), and then see if it is larger than 1.

# 5.3
Write brace when ever possible:
```
    if (((s >> i) & 1) == 0)
```

To find the next largest number (the smallest one that is larger than the target one), the answer mentions to turn off one 0 and one 1.
I would express it as move a right-most 1 to the next possible position where there is 0.
However, remember to shift the right part: we can make it smaller.

For instance, if we want to find the next largest number of xxxx**0**11**1**00, we move the right-most 1 to the next left position, and it would be xxxx**1**11**0**00.
Next, we move all the 1 in the right of the new 1 xxxx1**11**000 to the right, so the result can be smaller.

And you need to be careful about the operation.
The answer is more brief than my code. But to understand and write the comfortable code matters more.

There is [a runnable codes for reference](http://www.geeksforgeeks.org/next-higher-number-with-same-number-of-set-bits/).

# 5.4

I'm so happy that I don't need to code for this one~

# 5.5
We can use `xor` in this case, or just write our own code to check how many bits are different.

# 5.6
An application to mask.
I hate tricky questions...

# 5.7
Very tricky and classic one.

Don't be afraid of using more space.
If I have the buffer, then I can do it.

The basic idea is to judge the answer bit by bit.
And **remove**!
I forgot to remove the impossible items, so I failed to do it at the beginning.

Another topic is the running time. It seems to be some kind of log, because we split the buffer into two parts. However it is not the case.
It's `n + 1/2n + 1/4n +... = 2n = n`.
