#include <iostream>
using namespace std;

bool check(string a, string b)
{
    if (a.size() != b.size())
        return (a > b);
    int stacka[26], stackb[26];
    memset(stacka, 0, sizeof(int) * 26);
    memset(stackb, 0, sizeof(int) * 26);
    for (int i = 0; i <a.size(); i++)
    {
        stacka[a[i]]++;
        stackb[b[i]]++;
    }
    for (int i = 0; i < 26; i++)
    {
        if (stacka[i] > stackb[i])
            return true;
    }
    return false;
}

int main()
{
    string stack[5] = {"asdf", "abcde", "fdsa", "zzzzz", "xxx"};
    const int N = 5;
    for (int i = 0; i < N; i++)
        for (int j = i+1; j < N; j++)
            if (check(stack[i], stack[j]))
            {
                string t = stack[i];
                stack[i] = stack[j];
                stack[j] = t;
            }

    for (int i = 0; i < N; i++)
    {
        cout << stack[i] << endl;
    }
    return 0;
}
