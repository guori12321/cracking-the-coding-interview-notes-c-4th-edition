#include <iostream>
using namespace std;

const int M = 5, N = 5;
int ansl = -1;
int ansr = -1;
int stack[M][N] = {
{1,  20, 30, 40, 50},
{11, 21, 35, 41, 52},
{12, 22, 37, 42, 53},
{13, 23, 38, 43, 55},
{14, 24, 39, 44, 100},
};
int target = 22;

//We can do it as the normal binary search: if the middle one in the matrix is larger than the matrix, then we can eliminate the left-up part, if smaller then the right-down part.
//Note the remaining part can be divided into two parts, and the two parts should contain the remaining part of the middle column and middle row
//As we eliminate about 1/4 of the matrix, the time complexity is $log_4^(MN)$
void search(int l0, int r0, int l1, int r1)
{

    if (ansl != -1)
        return;
    //Never compare l0 with r0
    //if (l0 > r0 || l1 > r1)
    //    return;
    if (l0 > l1 || r0 > r1)
        return;
    if (l0 < 0 || l1 < 0 || l0 >= M || l1 >= M || r0 < 0 || r1 < 0 || r0 >= N || r1 >= N)
        return;

    //cout << endl << endl << "Matrix\n";
    //for (int i = l0; i <= l1; i++)
    //{
    //    for (int j = r0; j <= r1; j++)
    //        cout << stack[i][j] << '\t';
    //    cout << endl;
    //}
    //cout << endl;


    int lm = (l0+l1) / 2;
    int rm = (r0+r1) / 2;
    //cout << "M " << stack[lm][rm] << endl;
    //getchar();
    if (stack[lm][rm] == target)
    {
        ansl = lm;
        ansr = rm;
        return;
    }

    if ( (l0 == l1) && (r0 == r1) )
        return;
    //cout << l0 << '\t' << r0 << '\t' << l1 << '\t' << r1 << '\t' << endl;

    if (target < stack[lm][rm])
    {
        search(l0, r0, l1, rm-1);
        search(l0, rm, lm-1, r1);
    }
    else
    {
        //Be  careful here, if you write l1 as l0, then it takes a long time to debug
        search(l0, rm+1, l1, r1);
        search(lm+1, r0, l1, rm);
    }

    return;
}

void easySearch(int l0, int r0, int l1, int r1)
{
    int m = l0, n = r1;
    while (m <= l1 && n >= r0)
    {
        if (stack[m][n] == target)
        {
            ansl = m;
            ansr = n;
            return;
        }
        else
            if (stack[m][n] < target)
                m++;
            else
                n--;
    }
    return;
}

int main()
{
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
            cout << stack[i][j] << '\t';
        cout << endl;
    }

    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            ansl = -1;
            ansr = -1;
            target = stack[i][j];
            target = 22;
            //search(0, 0, M-1, N-1);
            easySearch(0, 0, M-1, N-1);

            if (ansl == -1)
                cout << "Error" << endl;
            else
                cout << "OK" << endl;
        }
    }
    return 0;
}
