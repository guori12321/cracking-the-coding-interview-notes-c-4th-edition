#include <iostream>
using namespace std;

int main()
{
    int a[10] = {1, 1, 5, 7, 9};
    int b[5] = {2, 2, 6, 8, 10};
    int m = 4, n = 4;
    int last = 9;

    //attention to m > 0 or m >= 0
    while (m >= 0 && n >= 0)
    {
        if (a[m] > a[n])
        {
            a[last] = a[m];
            //careful about m-- first or last
            m--;
            last--;
        }
        else
        {
            a[last] = b[n];
            n--;
            last--;
        }
    }
    //In fact, we don't need to copy the remaining part of a
    for (int i = m; i > 0; i--)
    {
        a[last] = a[i];
        last--;
    }
    for (int i = n; i > 0; i--)
    {
        a[last] = b[i];
        last--;
    }

    for (int i = 0; i < 10; i++)
        cout << a[i] << '\t';
    return 0;
}
