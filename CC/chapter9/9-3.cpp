#include <iostream>
using namespace std;

const int N = 12;
int stack[N] = {15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14};
int ans = -1;
int target = 5;

void search(int l, int r) // Remember to add int here
{
    if (l > r)
        return;

    if ( (stack[l] < stack[r]) && (stack[r] < target))
        return;
    if ( (stack[l] > stack[r]) && (target > stack[l]))
        return;
    int m = (l+r) / 2;
    if (stack[m] == target)
    {
        ans = m;
        return;
    }

    if (target < stack[m])
        search(l, m-1);
    if (stack[m] < target)
        search(m+1, r);

    return;
}

int main()
{
    search(0, N-1);
    cout << ans << endl;
}
