#include <iostream>
using namespace std;

int N = 13;
string stack[13] = {"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", ""};
string target = "ballas";
int ans = -1;

void search(int l, int r)
{
    if (l > r)
        return;
    while (stack[l] == "" && l < r)
        l++;
    while (stack[r] == "" && l < r)
        r--;

    int m = (l+r) / 2;
    //Note "" is smaller than any other strings
    while (stack[m] == "" && m > l)
        m--;
    if (stack[m] == target)
    {
        ans = m;
        return;
    }

    if (target < stack[m])
        search(l, m-1);
    if (stack[m] < target)
        search(m+1, r);

    return;
}

int main()
{
    cout << ("" < "aa") << endl;
    search(0, N-1);
    cout << ans << endl;
    return 0;
}
