//(65, 100) (70, 150) (56, 90) (75, 190) (60, 95) (68, 110)
#include <iostream>
using namespace std;

int main()
{
int h[6] = {65, 70, 56, 75, 60, 68};
int w[6] = {100, 150, 90, 190, 95, 110};

for (int i = 0; i < 6; i++)
    for (int j = i+1; j < 6; j++)
        if (h[i] > h[j] || ( (h[i] == h[j]) && (w[i] > w[j])) )
        {
            int t = h[i];
            h[i] = h[j];
            h[j] = t;
            t = w[i];
            w[i] = w[j];
            w[j] = t;
        }

    int d[6] = {0};
    d[0] = 1;
    for (int i = 1; i < 6; i++)
        for (int j = 0; j < i; j++)
            if (w[j] < w[i])
                d[i] = max(d[i], 1 + d[j]);
    int ans = -1;
    for (int i = 0; i < 6; i++)
        ans = max(ans, d[i]);
    cout << ans <<  endl;

    return 0;
}
