#include <iostream>
using namespace std;

//int buffer[300];
//int stackPointer[3] = {0, 0, 0};

/*
void push(int stackNum, int item)
{
    if (stackPointer[stackNum] >= 100)
    {
        cout << stackNum << "  Full!" << endl;
        return;
    }
    stackPointer[stackNum]++;
    buffer[100*stackNum + stackPointer[stackNum] ] = item;
    return;
}

int pop(int stackNum)
{
    int result = buffer[ 100*stackNum + stackPointer[stackNum] ];
    stackPointer[stackNum]--;
    return result;
}

int peek(int stackNum)
{
    return buffer[100*stackNum + stackPointer[stackNum] ];
}

int main()
{
    push(0, 1);
    push(0, 2);
    push(0, 3);
    push(0, 4);
    push(0, 5);
    cout << pop(0) << endl;
    cout << pop(0) << endl;
    cout << pop(0) << endl;
    cout << pop(0) << endl;
    cout << pop(0) << endl;

    push(1, 1);
    push(1, 2);
    push(1, 3);
    push(1, 4);
    push(1, 5);
    cout << pop(1) << endl;
    cout << pop(1) << endl;
    cout << pop(1) << endl;
    cout << pop(1) << endl;
    cout << pop(1) << endl;

    push(2, 2);
    push(2, 2);
    push(2, 3);
    push(2, 4);
    push(2, 5);
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    return 0;
}

*/
int buffer[300];
int stackPointer[3] = {-1, -1, -1};
int prevNode[300];
bool used[100] = {0};

void push(int stackNum, int item)
{
    int i;
    for (i = 0; i < 100; i++)
        if (! used[i])
            break;
    buffer[i] = item;
    prevNode[i] = stackPointer[stackNum];
    stackPointer[stackNum] = i;
    used[i] = true;
}

int pop(int stackNum)
{
    int ans = buffer[ stackPointer[stackNum] ];
    used[ stackPointer[stackNum] ] = false;
    stackPointer[stackNum] = prevNode[ stackPointer[stackNum] ] ;
    return ans;
}

int peek(int stackNum)
{
	return buffer[ stackPointer[stackNum] ];
}

bool isEmpty(int stackNum)
{
	return stackPointer[stackNum] < 0;
}

int main()
{
    push(0, 1);
    push(0, 2);
    push(0, 3);
    push(0, 4);
    push(0, 5);
    cout << pop(0) << endl;
    cout << pop(0) << endl;
    cout << pop(0) << endl;
    cout << pop(0) << endl;
    cout << pop(0) << endl;

    push(1, 1);
    push(1, 2);
    push(1, 3);
    push(1, 4);
    push(1, 5);
    cout << pop(1) << endl;
    cout << pop(1) << endl;
    cout << pop(1) << endl;
    cout << pop(1) << endl;
    cout << pop(1) << endl;

    push(2, 2);
    push(2, 2);
    push(2, 3);
    push(2, 4);
    push(2, 5);
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    cout << pop(2) << endl;
    return 0;
}

