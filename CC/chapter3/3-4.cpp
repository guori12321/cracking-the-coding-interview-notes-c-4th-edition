#include <iostream>
#include <fstream>
using namespace std;

int n;
int stack[3][100];
int sn[3];

void move(int n, int l, int r) //Move the top n plates from the l stack to the r stack
{
    if (n < 0)
        return;
    if (n == 1)
    {
        stack[r][ sn[r] ] = stack[l][ sn[l]-1 ];
sn[r]++;
        sn[l]--;
        cout <<  endl << "stack:\n";
        cout << "stack 0: ";
        for (int i = 0; i < sn[0]; i++)
            cout << stack[0][i] << "\t";
        cout << endl;
        cout << "stack 1: ";
        for (int i = 0; i < sn[1]; i++)
            cout << stack[1][i] << "\t";
        cout << endl;
        cout << "stack 2: ";
        for (int i = 0; i < sn[2]; i++)
            cout << stack[2][i] << "\t";
        cout << endl;
}
else
{
    move(n-1, l, (0+1+2) - l - r); //move the top n-1 plates to the third stack
    move(1, l, r);
    move(n-1, (0+1+2) - l - r, r);
}
}

int main()
{
    fstream fin("3-4.in");

    while (fin >> n)
    {
        cout << n << endl;
        memset(stack, 0, sizeof(int)*3*100);
        for (int i = 0; i < n; i++)
            stack[0][i] = n-1 - i;
        sn[0] = n;
        sn[1] = 0;
        sn[2] = 0;

        move(n, 0, 2);
    }
}
