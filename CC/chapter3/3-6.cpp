#include <iostream>
#include <stack>
using namespace std;

stack<int> s, r;

void sort()
{
    r.push(-999999);
    while (! s.empty() )
        if ( s.top() >= r.top() )
        {
            r.push( s.top() );
            s.pop();
        }
        else
        {
            int t = s.top();
            s.pop();
            while ( t < r.top() )
            {
                s.push( r.top() );
                r.pop();
            }
            r.push(t);
        }

    while (! r.empty() )
    {
        s.push(r.top());
        r.pop();
    }

    s.pop(); // remove the -9999999 guard
    return;
}

int main()
{

    s.push(1);
    s.push(3);
    s.push(7);
    s.push(2);
    s.push(4);
    s.push(9);

    sort();
    while (! s.empty())
    {
        cout << s.top() << "\t";
        s.pop();
    }
    cout << endl;

    return 0;
}
