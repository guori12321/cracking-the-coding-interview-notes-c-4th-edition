#3.1
Just to practice the stack codes.
As an addition to the answer, we can list all the free slices in a free list, or use a bitmap to search for the free slices.

#3.2
A stack recording the minimal values is necessary.
However we can skip the duplicated minimal values to save space (however there may be the same integers in the minimal stack for there may be the same values in the original stack).

# 3.3
There are multiple ways.
I assume that the substack can be not full, because moving all the remaining items takes a long long time.
However, when you want to pop the stack, perhaps the current and the several previous stacks are all empty, so just keep looking, until you find one that is not empty.

# 3.4
Just be clear about the procedure.

# 3.5
Initial the in-class variables in the constructor.
And remember to add `public :` and the last `;`.
```
    public: MyQueue()
    {
        n1 = 0;
        n2 = 0;
    };
```

# 3.6
A stack and a int buffer are necessary.
Ask that during the interview.

Try to use the `stack` STL.
Note that the `stack.pop()` function in C++ returns void.
So just use `stack.top()` first and then pop.


