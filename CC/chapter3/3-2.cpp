#include <iostream>
using namespace std;

int stack[100] = {0}, minStack[100] = {99999};
int stackPointer = 0, minStackPointer = 0;

void push(int item)
{
    stack[stackPointer] = item;
    stackPointer++;
    if (item <= minStack[ minStackPointer ])
    {
        minStackPointer++;
        minStack[ minStackPointer ] = item;
    }
    return;
}

int pop()
{
    //decrease the pointer first, because the pointer points to a new unused space.
    stackPointer--;
    int ans = stack[ stackPointer ];
    if (ans <= minStack[ minStackPointer ])
    {
        minStackPointer--;
    }
    return ans;
}

int min()
{
    return minStack[ minStackPointer ];
}

int main()
{
    push(4);
    cout << min() << endl;
    push(2);
    cout << min() << endl;
    push(3);
    cout << min() << endl;
    push(1);
    cout << min() << endl;
    push(5);
    cout << min() << endl;

    cout << endl;
    pop();
    cout << min() << endl;
    pop();
    cout << min() << endl;
    pop();
    cout << min() << endl;
    pop();
    cout << min() << endl;

}
