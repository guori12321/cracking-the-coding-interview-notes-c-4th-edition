#include <iostream>
using namespace std;

const int stackSize = 5;
int stack[10][stackSize];
int stackPointer[10] = {0};
int stackNum = 0;

void push(int item)
{
    if (stackPointer[ stackNum ] >= stackSize)
    {
        stackNum++;
    }
    stack[stackNum][stackPointer[stackNum]] = item;
    stackPointer[stackNum]++;
}

int pop()
{
    while (stackPointer[stackNum] == 0)
        stackNum--;
    stackPointer[stackNum] --;
    return stack[stackNum][stackPointer[stackNum]];
}

int subPop(int sNum)
{
    stackPointer[ sNum ]--;
    return stack[sNum][stackPointer[ sNum ]];

}

int main()
{
    push(1);
    push(2);
    push(3);
    push(4);
    push(5);
    push(6);

    cout << subPop(0) << endl;
    cout << pop() << endl;
    cout << pop() << endl;
    cout << pop() << endl;
    cout << pop() << endl;
    cout << pop() << endl;

    push(7);
    push(8);
    push(9);
    push(10);
    push(11);
    push(12);


    cout << pop() << endl;
    cout << pop() << endl;
    cout << pop() << endl;
    cout << pop() << endl;
    cout << pop() << endl;

    return 0;
}
