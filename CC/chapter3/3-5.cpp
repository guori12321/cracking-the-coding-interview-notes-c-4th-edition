#include <iostream>
#include <fstream>
using namespace std;

class MyQueue
{
    public: MyQueue()
    {
        n1 = 0;
        n2 = 0;
    };

    int stack1[100];
    int stack2[100];
    int n1, n2;

    public:
    void push(int item)
    {
        stack1[n1] = item;
        n1++;
    }

    int pop()
    {
        if (n2 == 0)
        {
            for (int i = n1 -1; i >= 0; i--)
            {
                stack2[n2] = stack1[i];
                n2++;
            }
            n1 = 0;
        }
        n2--;
        return stack2[n2];
    }
};

int main()
{
    MyQueue m;

    m.push(0);
    m.push(1);
    m.push(2);

    cout << m.pop() << endl;
    cout << m.pop() << endl;

    m.push(3);
    m.push(4);
    m.push(5);

    cout << m.pop() << endl;
    cout << m.pop() << endl;
    cout << m.pop() << endl;

    return 0;
}
