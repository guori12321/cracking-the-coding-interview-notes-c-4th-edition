#include <iostream>
using namespace std;

int n = 5;
int s[5] = {1, 2, 3, 4, 5};
bool stack[5];

void getSubset(int n)
{
    if (n == 5)
    {
        for (int i = 0; i < n; i++)
        {
            if (stack[i])
                cout << s[i] << '\t';
        }
        cout << endl;
        return;
    }

    stack[n] = 0;
    getSubset(n+1);
    stack[n] = 1;
    getSubset(n+1);
    return;
}

int main()
{
    getSubset(0);
    return 0;
}
