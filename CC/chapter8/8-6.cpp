#include <iostream>
#include <fstream>
using namespace std;

int M = 5, N = 5;
int matrix[100][100];
bool visited[100][100];

void search(int m, int n)
{
    if (m < 0 || m > M || n < 0 || n > N)
        return;
    if (visited[m][n])
        return;
    visited[m][n] = true;
    if (matrix[m][n] == 0)
        return;
    matrix[m][n] = 2;
    search(m+1, n);
    search(m-1, n);
    search(m, n+1);
    search(m, n-1);
}

int main()
{
    ifstream fin("8-6.in");
    for (int i = 0; i < M; i++)
        for (int j = 0; j < N; j++)
            fin >> matrix[i][j];

    search(2, 2);

    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
            cout << matrix[i][j] << '\t';
        cout << endl;
    }
    return 0;
}
