#include <iostream>
using namespace std;

int getTotal(int n, int size)
{
    if (n <= 0)
        return 0;
    int ans = 0;
    if (size == 1)
        return 1;

    int nextSize = 0;
    if (size == 25)
        nextSize = 10;
    if (size == 10)
        nextSize = 5;
    if (size == 5)
        nextSize = 1;
    if (size == 1)
        nextSize = 1;

    for (int i = 0; i * size <= n; i++)
        ans += getTotal(n - i * size, nextSize);
    return ans;
}

int main()
{
    cout << getTotal(100, 25) << endl;
    return 0;
}
