#include <iostream>
using namespace std;

int N = 3;
char stack[10];

void getParentheses(int l, int r, int n)
{
    //set the edge check
    if (l < 0 || r < 0)
        return;
    if (n == N * 2)
    {
        for (int i = 0; i < N*2; i++)
            cout << stack[i];
        cout << endl;
    }
    stack[n] = '(';
    getParentheses(l-1, r+1, n+1);
    if (r > 0)
    {
        stack[n] = ')';
        getParentheses(l, r-1, n+1);
    }
    return;
}

int main()
{
    getParentheses(N,0,0);
    return 0;
}
