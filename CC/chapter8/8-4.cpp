#include <iostream>
using namespace std;

string s = "abcde";
char stack[5];
bool visited[5] = {0};

void getPermutation(int n)
{
    if (n == 5)
    {
        for (int i = 0; i < 5; i++)
            cout << stack[i];
        cout << endl;
        return;
    }

    for (int i = 0; i < 5; i++)
    {
        if (!visited[i])
        {
            visited[i] = true;
            //stack[n], instead of stack[i]
            stack[n] = s[i];
            getPermutation(n+1);
            visited[i] = false;
        }
    }
    return;
}

int main()
{
    getPermutation(0);

    return 0;
}
