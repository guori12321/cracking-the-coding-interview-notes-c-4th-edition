#include <iostream>
using namespace std;

const int N = 8;

int stack[N];
int tot = 0;

bool check(int t, int n)
{
    for (int i = 0; i < n; i++)
        if (t == stack[i] || abs(stack[i] - t) == (n - i) )
            return false;
    return true;
}

void DFS(int n)
{
    if (n == 8)
    {
        tot++;
        for (int i = 0; i < N; i++)
            cout << stack[i] << '\t';
        cout << endl;
    }

    for (int i = 0; i < N; i++)
        if (check(i, n))
        {
            stack[n] = i;
            DFS(n+1);
        }
    return;
}

int main()
{
    DFS(0);
    cout << tot << endl;
    return 0;
}
