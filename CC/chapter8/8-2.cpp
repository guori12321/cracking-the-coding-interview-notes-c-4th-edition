#include <iostream>
using namespace std;

int recursive(int m, int n)
{
    if ( m < 0 || n < 0)
        return 0;
    if (m == 0 && n == 0)
        return 1;
    return recursive(m - 1, n) + recursive(m, n-1);
}

int matrix[100][100];
void iterative(int m, int n)
{
    memset(matrix, 0, 100*100*sizeof(int));
    matrix[0][0] = 1;
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
        {
            //Skip the initial state
            if (i == 0 && j == 0)
                continue;
            int t0 = 0, t1 = 0;
            if (i > 0) t0 = matrix[i-1][j];
            if (j > 0) t1 = matrix[i][j-1];
            matrix[i][j] = t0 + t1;
        }

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
            cout << matrix[i][j] << '\t';
        cout << endl;
}
    return;
}

int main()
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
            cout << recursive(i, j) << '\t';
        cout << endl;
    }

    cout << endl;
    iterative(5, 5);
    return 0;
}
