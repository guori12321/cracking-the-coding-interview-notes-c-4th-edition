#include <iostream>
using namespace std;

int getRecursive(int n)
{
    if (n == 0)
        return 0;
    if (n == 1 || n == 2)
        return 1;

    int t =  getRecursive(n-2) + getRecursive(n-1);
    return t;
}

int getIterative(int n)
{
    int t0 = 0;
    int t1 = 1;
    for (int i = 0; i < n; i++)
    {
        int t = t0 + t1;
        t0 = t1;
        t1 = t;
    }
    cout << t0 << endl;
    return 0;
}

int main()
{
    cout << getRecursive(0) << endl;
    cout << getRecursive(1) << endl;
    cout << getRecursive(2) << endl;
    cout << getRecursive(3) << endl;
    cout << getRecursive(4) << endl;
    cout << getRecursive(5) << endl;
    cout << getRecursive(6) << endl;
    cout << getRecursive(7) << endl;
    cout << getRecursive(8) << endl;
    cout << endl;

    getIterative(0);
    getIterative(1);
    getIterative(2);
    getIterative(3);
    getIterative(4);
    getIterative(5);
    getIterative(6);
    getIterative(7);
    getIterative(8);
    cout << endl;

    return 0;
}
