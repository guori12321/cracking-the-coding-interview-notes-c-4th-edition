# 4.1
It's **NOT** the same to the [leetcode 'Balanced Binary Tree' question](https://leetcode.com/problems/balanced-binary-tree/): in Leetcode the definition is `the height of any two subtrees`, in CC is `the height of any two leaf nodes`.

Also, as the Leetcode uses class, so don't use global variables, just in-class variables.

Note:
If the left and right subtree are balanced tree, the entire tree may not be a balanced tree;

# 4.2
Just to practice how to write DFS and BFS.

# 4.3
Just to practice divide-and-conquer and in-order visit.

# 4.4
The trick is to record the level of the nodes when we do BFS.
We can add the level information into every state in the BFS queue, but a better way is to add a entire level into the queue, record the queue size *n*, and then pop *n* times from the queue and add their children nodes.
Those *n* newly poped nodes would be in the same level, and after they are poped out, the queue would be occupied by their direct children only.

Another tricky way is to use the linked list of the last level to get the next level, just as the answer does.

And in the while loop, remember to set `t=t->next`.

# 4.5
There is [a very good reference](http://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/).

There are two methods, one is to use the parent link, another is to search from the root.
The second method is based on the assumption 'binary search tree', that means the value of the successor node is the least value who is larger than the target value.
And note that in the second method if there is no right child of the target node then we want to find the first ancestor whose left branch contains our target, so only update the answer variable when we go left.

# 4.6
Just to check if we can arrive the two nodes from the current node, its left child and right child. Then we return the current node or continue on the left or right ones.

# 4.7
There is [a reference with test cases](http://www.geeksforgeeks.org/check-if-a-binary-tree-is-subtree-of-another-binary-tree/).

The assumption that one tree is very large while another is small means we cannot just get the inorder and preorder traversals of the two trees. So we need to write codes to check the trees.

# 4.8

Don't code too much details during the interview: it's not the OI or ACM game, it's about mind and communication.
And code the simple way, you have 45 minutes after all.
Don't consider space complicity when not necessary.
If it is the key, the interviewer will ask.

During the interview, try to simplify the question if necessary.
For this specific question, all modified questions can be solved by the corresponding modification in the codes.

We do a DFS. If it is from the root to the leaves, we check sum only when we reach a leaf.
If from root to any leaf, then we check at any point.

The next is the key trick here: if from any point (not necessarily root) to any other point, then we check the sum of the last parts of the buffer.

As we need to enumerate all the last parts of the buffer, a buffer instead of a current sum is a MUST.
Previously I don't know how to do it because I want to save space and use a current sum (an int) rather than a buffer vector. However, the answer in the book uses lots of buffer! So, only save space when necessary, and if the interviewer is interested in that point, he would ask!
