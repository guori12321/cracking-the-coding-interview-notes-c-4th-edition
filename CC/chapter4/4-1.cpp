#include <iostream>
#include <fstream>
using namespace std;

int maxh = -999999;
int minh = 999999;

struct TreeNode
{
    int value;
    TreeNode * left, *right;
};

void getHeight(TreeNode* r, int h)
{
    if (r == NULL)
    {
        if (maxh < h)
            maxh = h;
        if (minh > h)
            minh = h;
        return;
    }
    getHeight(r->left, h+1);
    getHeight(r->right, h+1);
    return;
}

bool isBalanced(TreeNode* root) {
    getHeight(root, 0);
    if ( (maxh - minh) < 2)
        return true;
    return false;
}

int main()
{
    TreeNode *root = new TreeNode;
    root->value = 0;
    root->left = new TreeNode;
    root->right = NULL;

    root->left->value = 1;
    root->left->left = NULL;
    root->left->right = NULL;

    cout << isBalanced(root) << endl;
    cout << maxh << endl << minh << endl;
    if ( isBalanced(root))
    {
        cout << "True" << endl;
        return 0;
    }
    cout << "False" << endl;

    return 0;
}
