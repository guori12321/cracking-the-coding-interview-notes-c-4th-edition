#include <iostream>
#include <fstream>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *left, *right;
};

struct ListNode{
    TreeNode* val;
    ListNode *next;
};

int s[100];
ListNode *list[100];
int totlel = 0;

TreeNode* getTree(int l, int r)
{
    if (l > r)
        return NULL;
    TreeNode* root = new TreeNode;
    int m = (l+r) / 2;
    root->val = s[m];
    root->left = getTree(l, m-1);
    root->right = getTree(m+1, r);
    return root;
}

void inOrder(TreeNode *root)
{
    if (root == NULL)
        return;
    inOrder(root->left);
    cout << root->val << '\t';
    inOrder(root->right);

    return;
}

void getLinkedList(TreeNode *root)
{
    list[0] = new ListNode;
    list[0]->val = root;
    list[0]->next = NULL;
    totlel = 1;

    int lastSum = 1;
    while (lastSum > 0)
    {
        lastSum = 0;
        ListNode* t = list[ totlel - 1];
        list[ totlel ] = NULL;
        ListNode * tt = NULL;

        while (t != NULL)
        {
            if (t->val->left != NULL)
            {
                if (list[totlel] == NULL)
                {
                    list[totlel] = new ListNode;
                    tt = list[totlel];
                    tt->val = t->val->left;
                    tt->next = NULL;
                }
                else
                {
                    tt->next = new ListNode;
                    tt = tt->next;
                    tt->val = t->val->left;
                    tt->next = NULL;
                }
                lastSum++;
            }

            if (t->val->right != NULL)
            {
                if (list[totlel] == NULL)
                {
                    list[totlel] = new ListNode;
                    tt = list[totlel];
                    tt->val = t->val->right;
                    tt->next = NULL;
                }
                else
                {
                    tt->next = new ListNode;
                    tt = tt->next;
                    tt->val = t->val->right;
                    tt->next = NULL;
                }

                lastSum++;
            }
            t = t->next;
        }
        totlel++;
    }
}

int main()
{
    for (int i = 0; i < 10; i++)
        s[i] = i;
    TreeNode* root = getTree(0, 9);
    inOrder(root);
    cout << endl;

    getLinkedList(root);
    for (int i = 0; i < 5; i++)
    {
        cout << "Layer: " << i << endl;
        if (list[i] != NULL)
        {
            ListNode* t = list[i];
            while (t != NULL)
            {
                cout << t->val->val << '\t';
                t = t->next;
            }
            cout << endl;
        }
    }
    return 0;
}

