#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <stack>
using namespace std;

struct Node
{
public:
    vector<int> next;
};

Node graph[100];
int visited[100];

void dfs(int t)
{
    if (! visited[t])
    {
        visited[t] = true;
        for (int i = 0; i < graph[t].next.size(); i++)
            dfs(graph[t].next[i]);
    }
}

int main()
{
    memset(visited, 0, 100);
    graph[0].next.push_back(1);
    graph[0].next.push_back(2);
    graph[2].next.push_back(1);
    graph[2].next.push_back(5);
    graph[5].next.push_back(8);

    queue<int> q;
    q.push(0);
    while (!q.empty())
    {
        int t = q.front();
        q.pop();

        if (visited[t] == false)
        {
            visited[t] = true;
            for (int i = 0; i < graph[t].next.size(); i++)
                q.push(graph[t].next[i]);
        }
    }

    memset(visited, 0, 100);
    dfs(0);

    if (visited[8])
        cout << "True\n";
    else
        cout << "False\n";

    return 0;
}
