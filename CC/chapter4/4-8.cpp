#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
using namespace std;

/* A binary tree node has data, pointer to left child
   and a pointer to right child */
struct node
{
    int data;
    struct node* left;
    struct node* right;
    struct node* parent;
};

struct node * minValue(struct node* node);

struct node * inOrderSuccessor(struct node *root, struct node *n)
{
  // step 1 of the above algorithm
  if( n->right != NULL )
    return minValue(n->right);

  // step 2 of the above algorithm
  struct node *p = n->parent;
  while(p != NULL && n == p->right)
  {
     n = p;
     p = p->parent;
  }
  return p;
}

/* Given a non-empty binary search tree, return the minimum data
    value found in that tree. Note that the entire tree does not need
    to be searched. */
struct node * minValue(struct node* node) {
  struct node* current = node;

  /* loop down to find the leftmost leaf */
  while (current->left != NULL) {
    current = current->left;
  }
  return current;
}

/* Helper function that allocates a new node with the given data and
    NULL left and right pointers. */
struct node* newNode(int data)
{
  struct node* node = (struct node*)
                       malloc(sizeof(struct node));
  node->data   = data;
  node->left   = NULL;
  node->right  = NULL;
  node->parent = NULL;

  return(node);
}

/* Give a binary search tree and a number, inserts a new node with
    the given number in the correct place in the tree. Returns the new
    root pointer which the caller should then use (the standard trick to
    avoid using reference parameters). */
struct node* insert(struct node* node, int data)
{
  /* 1. If the tree is empty, return a new,
      single node */
  if (node == NULL)
    return(newNode(data));
  else
  {
    struct node *temp;

    /* 2. Otherwise, recur down the tree */
    if (data <= node->data)
    {
         temp = insert(node->left, data);
         node->left  = temp;
         temp->parent= node;
    }
    else
    {
        temp = insert(node->right, data);
        node->right = temp;
        temp->parent = node;
    }

    /* return the (unchanged) node pointer */
    return node;
  }
}

vector<int> buffer;

void getSum(struct node* root, int sum)
{
    if (root == NULL)
        return;
    buffer.push_back(root->data);

    //root is a leaf node
    //if (root->left == NULL && root->right == NULL)
    //{
        int t = 0;
        for (int i = buffer.size() - 1; i >= 0; i--)
        {
            t += buffer[i];
            if (t == sum)
            {
                cout << endl;
                for (int j = i; j < buffer.size(); j++)
                {
                    cout << buffer[j] << '\t';
                }
                cout << endl;
            }
        }
        /*
        for (int i = 0; i < buffer.size(); i++)
        {
            //cout << buffer[i] << '\t';
            t += buffer[i];
        }
        //cout << t << endl;
        if (t == sum)
        {
            cout << endl;
            for (int i = 0; i < buffer.size(); i++)
            {
                cout << buffer[i] << '\t';
            }
            cout << endl;
        }
        */
    //}

    getSum(root->left, sum);
    getSum(root->right, sum);
    buffer.erase(buffer.end() - 1);
}

/* Driver program to test above functions*/
int main()
{
  struct node* root = NULL, *temp, *succ, *min;

  //creating the tree given in the above diagram
  root = insert(root, 20);
  root = insert(root, 8);
  root = insert(root, 22);
  root = insert(root, 4);
  root = insert(root, 12);
  root = insert(root, 10);
  root = insert(root, 14);
  temp = root->left->right->right;

  getSum(root, 12);

  /*
  succ =  inOrderSuccessor(root, temp);
  if(succ !=  NULL)
    printf("\n Inorder Successor of %d is %d ", temp->data, succ->data);
  else
    printf("\n Inorder Successor doesn't exit");
  */
  return 0;
}
