#include <iostream>
#include <fstream>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *left, *right;
};

int s[100];

TreeNode* getTree(int l, int r)
{
    if (l > r)
        return NULL;
    TreeNode* root = new TreeNode;
    int m = (l+r) / 2;
    root->val = s[m];
    root->left = getTree(l, m-1);
    root->right = getTree(m+1, r);
    return root;
}

void inOrder(TreeNode *root)
{
    if (root == NULL)
        return;
    inOrder(root->left);
    cout << root->val << '\t';
    inOrder(root->right);

    return;
}

int main()
{
    for (int i = 0; i < 10; i++)
        s[i] = i;
    TreeNode* root = getTree(0, 9);
    inOrder(root);
    cout << endl;

    return 0;
}
