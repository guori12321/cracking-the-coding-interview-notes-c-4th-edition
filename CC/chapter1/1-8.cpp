#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ifstream fin("1-8.in");
    string s, t;
    while (fin >> s >> t)
    {
        cout << endl;
        cout << s << endl;
        cout << t << endl;
        cout << endl;
        if (s.size() != t.size())
        {
            cout << "False!" << endl;
            continue;
        }
        string str = t + t;
        if (str.find(s) != string::npos)
            cout << "True!" << endl;
        else
            cout << "False!" << endl;
    }
}
