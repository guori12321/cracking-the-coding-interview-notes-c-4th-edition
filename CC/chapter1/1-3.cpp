#include <iostream>
#include <fstream>
using namespace std;

int main0()
{
    ifstream fin("1-3.in");
    string s;

    while (fin >> s)
    {
        cout << s << endl;

        for (int i = 0; i < s.size(); i++)
        {
            int j = i+1;
            while (j < s.size())
            {
                if (s[i] == s[j])
                    s.erase(s.begin() + j);
                else
                    j++;
            }
        }
        cout << s << endl;
    }

    return 0;
}


int main1()
{
    ifstream fin("1-3.in");
    string str;

    while (fin >> str)
    {
        char* s = new char[str.size() + 1];
        copy(str.begin(), str.end(), s);
        s[str.size()] = '\0';
        printf("%s\n", s);
        int tail = 0;
        for (int i = 0; i < strlen(s); i++)
        {
            int j;
            for (j = 0; j < tail; j++)
            {
                if (s[j] == s[i])
                    break;
            }
            if (j == tail)
            {
                s[tail] = s[i];
                tail++;
            }
        }
        s[tail] = '\0';
        printf("%s\n\n", s);
    }

    return 0;
}

int main()
{
    ifstream fin("1-3.in");
    string str;

    while (fin >> str)
    {
        char* s = new char[str.size() + 1];
        copy(str.begin(), str.end(), s);
        s[str.size()] = '\0';
        printf("%s\n", s);

        bool hash[20];
        memset(hash, 0, 20);
        int tail = 0;
        for (int i = 0; i < strlen(s); i++)
        {
            if ( !(hash[ s[i] - '0' ]) )
            {
                s[tail] = s[i];
                tail++;
                hash[ s[i] - '0' ] = 1;
            }
        }
        s[tail] = '\0';
        printf("%s\n\n", s);
    }

    return 0;
}
