#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ifstream fin("1-2.in");
    string str;
    while (fin >> str)
    {
        char* s = new char[100];
        copy(str.begin(), str.end(), s);
        s[str.size()] = '\0';
        printf("%s\n", s);

        int n = strlen(s);
        for (int i = 0; i < n/2; i++)
        {
            char t = s[i];
            s[i] = s[n-1-i];
            s[n-1-i] = t;
        }

        printf("%s\n\n", s);
    }

    return 0;
}



