# 1.1

One confusing part here is the meaning of the `unique chars`.
Previously I assume that `all the unique chars` means we have every chars in the alphabet.
However the answer assume that it means every char in the string is different.
Perhaps I'm influenced by the directly Chinese translation in my mind.

# 1.2

Note that the `C style string` or `char *` in C.
It's better to use `strcpy()` to initialize it.
And when you want to convert a C++ string to a C char * string, you can use the following `std::copy()` funciton (more detail in the [Stackoverflow related question](http://stackoverflow.com/questions/347949/how-to-convert-a-stdstring-to-const-char-or-char) ):
```
    char* s = new char[100];
    copy(str.begin(), str.end(), s);
    s[str.size()] = '\0';
```

# 1.3

Note that you cannot just look forward because the length of the string will change during the `for (int j)` loop:
```
    for (int i = 0; i < s.size(); i++)
        for (int j = i+1; j < s.size(); j++)
        {
            if (s[i] == s[j])
                s.erase(s.begin() + j);
        }
```

If you want to run it on the test case `1111111`, you will get `111` rather than `1` instead.

So let's use the following code instead:
```
    for (int i = 0; i < s.size(); i++)
    {
        int j = i+1;
        while (j < s.size())
        {
            if (s[i] == s[j])
                s.erase(s.begin() + j);
            else
                j++;
        }
    }
```
In this case `j` will grow only the current char is not duplicated with the previous ones.

Or we can just follow the answer in the book.
```
    int tail = 0;
    for (int i = 0; i < strlen(s); i++)
    {
        int j;
        for (j = 0; j < tail; j++)
        {
            if (s[j] == s[i])
                break;
        }
        if (j == tail)
        {
            s[tail] = s[i];
            tail++;
        }
    }
    s[tail] = '\0';
    printf("%s\n\n", s);
```

# 1.4

Just count them.
And you can just use one hash table to count.

One trick is to judge weather they have the same length first, and then, when counting the second string, if the counted number is smaller than 0, then we can just return false. And we don't need to see if all the counted number is 0 or not, because otherwise there would be at least one negative number in the hash table.

# 1.5

Just use the C++ string function
```
    s.replace(i, 1, "\%20");
```
It's okay if we don't convert the '%', but I suggest to do that because if the '%' is followed by chars like 'd' or 'f' there would be bugs.

Note that the `replace()` may be O(n) because we need to copy chars. Another way is to do it as the answer in the book, and pay attention that we do it backward to avoid overwriting.

# 1.6

A very popular but tricky one.
I suggest to practice it at least 3 times.
The idea is easy, however, programming is not.
You need to have a very clear thought and be aware of the meaning of all the variables all the time.

# 1.7

Be careful about the `memset()`: the length parameter should match the variable type, otherwise where may involve weird bugs.

# 1.8
Read the example carefully! It's not a traditional rotation.
And remember to check if the two string has the same length.
