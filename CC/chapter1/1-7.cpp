#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ifstream fin("1-7.in");
    int m, n;
    while (fin >> m >> n)
    {
        int matrix[100][100];

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
            {
                fin >> matrix[i][j];
            }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
                cout << matrix[i][j] << '\t';
            cout << endl;
        }
        cout << endl;

        bool column[100], row[100];
        memset(column, 0, sizeof(bool) * 100);
        memset(row, 0, sizeof(bool) * 100);

        cout << endl << "Column" << endl;
        for (int i = 0; i < m; i++)
            cout << column[i] << '\t';
        cout << endl << "Row" << endl;
        for (int j = 0; j < n; j++)
            cout << row[j] << '\t';
        cout << endl << endl;

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
                cout << matrix[i][j] << '\t';
            cout << endl;
        }
        cout << endl;

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (matrix[i][j] == 0)
                {
                    column[i] = 1;
                    row[j] = 1;
                    cout << matrix[i][j] << "Found 0:\t" <<  i << '\t' << j << endl;
                }

        cout << endl << "Column" << endl;
        for (int i = 0; i < m; i++)
            cout << column[i] << '\t';
        cout << endl << "Row" << endl;
        for (int j = 0; j < n; j++)
            cout << row[j] << '\t';
        cout << endl << endl;

        for (int i = 0; i < m; i++)
            if (column[i] == 1)
                for (int j = 0; j < n; j++)
                    matrix[i][j] = 0;
        for (int j = 0; j < n; j++)
            if (row[j] == 1)
                for (int i = 0; i < m; i++)
                    matrix[i][j] = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
                cout << matrix[i][j] << '\t';
            cout << endl;
        }
    }

    return 0;
}
