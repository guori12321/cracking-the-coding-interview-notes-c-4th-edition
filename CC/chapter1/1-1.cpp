#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ifstream fin("1_1.in");
    string s;
    while (fin >> s)
    {
        cout << s << endl;
        bool bucket[26]; //We assume that the string only contains lower letter
        memset(bucket, 0, sizeof(bool)*26);
        bool flag = true;
        for (int i = 0; i < s.size(); i++)
        {
            if (bucket[s[i] - 'a'] == 0)
                bucket[s[i] - 'a'] = 1;
            else
            {
                flag = false;
                break;
            }
        }

        if (flag)
            cout << "True\n\n";
        else
            cout << "False\n\n";
    }

    return 0;
}
