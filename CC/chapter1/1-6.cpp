#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ifstream fin("1-6.in");
 	int n;
	while (fin >> n)
	{
		int matrix[100][100];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				fin >> matrix[i][j];

		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
				cout << matrix[i][j] << '\t';
			cout << endl;
		}

		int layer = n / 2;
		for (int i = 0; i < layer; i++)
		{
			//store up side to temp
			int temp[100];
			for (int j = i; j < n - i; j++)
				temp[j] = matrix[i][j];
			//replace up side with left side
			for (int j = i; j < n - i; j++)
				matrix[i][j] = matrix[n-1-j][i];
			//replace left side with down side
			for (int j = i; j < n - i; j++)
				matrix[j][i] = matrix[n-1-i][j];
			//replace down side with right side
			for (int j = i; j < n - i; j++)
				matrix[n-1-i][j] = matrix[n-1-j][n-1-i];
			//replace right side with temp
			for (int j = i; j < n - i; j++)
				matrix[j][n-1-i] = temp[j];
		}

        cout << endl;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
				cout << matrix[i][j] << '\t';
			cout << endl;
		}

	}
return 0;
}
