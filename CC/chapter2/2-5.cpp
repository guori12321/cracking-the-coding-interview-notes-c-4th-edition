#include <iostream>
#include <fstream>
#include <set>
using namespace std;

struct ListNode
{
	int value;
	ListNode* next;
};

int main0() //Many unnecessary judgements
{
    ifstream fin("2-5.in");
    int n;
    ListNode *head, *t;

    while (fin >> n)
    {
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
        }
        else
        {
            set<int> hash;
            head = new ListNode;
            fin >> head->value;
            hash.insert(head->value);
            head->next = NULL;
            t = head;
            for (int i = 1; i < n; i++)
            {
                t->next = new ListNode;
                int temp;
                fin >> temp;
                if (hash.find(temp) != hash.end())
                {
                    ListNode *tt = head;
                    while (tt != NULL && tt->value != temp)
                        tt = tt->next;
                    t->next = tt;
                    //cout << "Find!\n";
                    //cout << tt->value << endl;
                    //cout << tt->next->value << endl;
                }
                else
                {
                    t = t->next;
                    hash.insert(temp);
                    t->value = temp;
                    t->next = NULL;
                }
            }
        }

        //Special judge on the head->next
        if (head->next == NULL)
        {
            cout << "False!\n";
            continue;
        }
        ListNode* t1 = head->next, *t2 = head->next->next;
        while (t1 != NULL && t2 != NULL && t2->next != NULL && t1 != t2)
        {
            t1 = t1->next;
            t2 = t2->next->next;
        }

        if (t1 == t2)
        {
            cout << "True!" << endl;

            t1 = head;
            while (t1 != t2)
            {
                t1 = t1->next;
                t2 = t2->next;
            }

            cout << t1->value << endl;
        }
        else
            cout << "False!\n";
    }

    return 0;
}

int main() //Many unnecessary judgements
{
    ifstream fin("2-5.in");
    int n;
    ListNode *head, *t;

    while (fin >> n)
    {
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
        }
        else
        {
            set<int> hash;
            head = new ListNode;
            fin >> head->value;
            hash.insert(head->value);
            head->next = NULL;
            t = head;
            for (int i = 1; i < n; i++)
            {
                t->next = new ListNode;
                int temp;
                fin >> temp;
                if (hash.find(temp) != hash.end())
                {
                    ListNode *tt = head;
                    while (tt != NULL && tt->value != temp)
                        tt = tt->next;
                    t->next = tt;
                    //cout << "Find!\n";
                    //cout << tt->value << endl;
                    //cout << tt->next->value << endl;
                }
                else
                {
                    t = t->next;
                    hash.insert(temp);
                    t->value = temp;
                    t->next = NULL;
                }
            }
        }

        //Special judge on the head->next
        if (head == NULL || head->next == NULL)
        {
            cout << "False!\n\n";
            continue;
        }

        ListNode* t1 = head, *t2 = head;
        while(t2 != NULL && t2->next != NULL)
        //At the beginning t1 == t2 == head, so we need to judge them before or use the break inside the loop
        //while(t1 != t2 && t2 != NULL && t2->next != NULL)
        //It's not necessary to check if t1 is NULL because t2 goes faster.
        ////while (t1 != NULL && t2 != NULL && t2->next != NULL && t1 != t2)
        {
            t1 = t1->next;
            t2 = t2->next->next;
            if (t1 == t2)
                break;
        }
        if (t2 == NULL || t2->next == NULL)
        {
            cout << "False!\n";
            continue;
        }

        t1 = head;
        while (t1 != t2)
        {
            t1 = t1->next;
            t2 = t2->next;
        }
        cout << "True!\n" << t1->value << endl << endl;
    }

    return 0;
}
