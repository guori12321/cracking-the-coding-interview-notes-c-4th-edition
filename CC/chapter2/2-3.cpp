#include <iostream>
#include <fstream>
#include <set>
using namespace std;

struct ListNode
{
	int value;
	ListNode* next;
};

//This is to delete when the value is given, rather than the node
int main0()
{
    ifstream fin("2-3.in");
    int n;
    while (fin >> n)
    {
        ListNode* head;
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
            continue;
        }

        head = new ListNode;
        fin >> head->value;
        head->next = NULL;
        ListNode* t = head;
        for (int i = 1; i < n; i++)
        {
            t->next = new ListNode;
            t = t->next;
            fin >> t->value;
            t->next = NULL;
        }

        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        int target;
        fin >> target;
        cout << target << endl;

        //Remember to judge the head
        if (head->value == target)
            head = head->next;
        else
        {
            t = head;
            while (t != NULL && t->next != NULL)
            {
                if (t->next->value == target)
                {
                    t->next = t->next->next;
                    break;
                }
                //I always forget this one sentence
                t = t->next;
            }
        }

        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl << endl ;
    }

    return 0;
}

int main()
{
    ifstream fin("2-3.in");
    int n;
    while (fin >> n)
    {
        ListNode* head;
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
            continue;
        }

        head = new ListNode;
        fin >> head->value;
        head->next = NULL;
        ListNode* t = head;
        for (int i = 1; i < n; i++)
        {
            t->next = new ListNode;
            t = t->next;
            fin >> t->value;
            t->next = NULL;
        }

        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        int target;
        fin >> target;
        cout << target << endl;

        if (head == NULL || head->next == NULL)
            cout << "False!\n";
        else
        {
            head->value = head->next->value;
            head->next = head->next->next;
        }

        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl << endl ;
    }

    return 0;
}

