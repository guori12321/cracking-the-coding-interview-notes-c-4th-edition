# Chapter 2

# 2.1

To use the `set` in C++:
```
if ( hash.find(t->next->value) != hash.end())
```

As we try to delete some nodes, here we need to record the previous node as the answer does.
I prefer to judge the value of `t->next` instead.

Another topic is to use no buffer space. So we need to maintain one `done` edge, and before this edge the delete is done and there is no duplicate nodes.
So when we want to extend the `done` edge, we need to check all the nodes to see if the current node is duplicated with the previous ones.
In my codes, there are two `while` loops. And they should all check if the `done` pointer and its next node is NULL or not.

# 2.2
I misunderstood the question again...The nth to the last node means the position rather than the value.

So there are two ways, recursively and iterate.

## Recursively
Remember to set a bool flag, otherwise we will print the answer many times (every node at head of it would call the function and print it, or you can set a cache to remember the result of the recursive function).

## Iteration

Just use two pointers.

# 2.3
In the example the node 'c' is given (rather than the value 'c').

# 2.4
The iteration will be very long and many details to deal with.

Note that the input should not be NULL. It should at least contain a node with value 0.

# 2.5
We all want to make the codes more brief. However some brief codes may bring bugs.
Make sure to check the empty list and the list with only 1 and 2 nodes.
