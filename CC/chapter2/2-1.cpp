#include <iostream>
#include <fstream>
#include <set>
using namespace std;

struct ListNode
{
	int value;
	ListNode* next;
};

int main0()
{
	ifstream fin("2-1.in");
	int n;
	while (fin >> n)
	{
		ListNode* head;
		if (n == 0)
		{
			head = NULL;
			cout << "NULL List \n";
			continue;
		}

		head = new ListNode;
		fin >> head->value;
		head->next = NULL;
		ListNode* t = head;
		for (int i = 1; i < n; i++)
		{
			t->next = new ListNode;
			t = t->next;
			fin >> t->value;
			t->next = NULL;
		}

		t = head;
		while (t != NULL)
        {
			cout << t->value << "\t";
            t = t->next;
        }
		cout << endl;

		t = head;
		set<int> hash;
		while (t != NULL && t->next != NULL)
		{
			hash.insert(t->value);
			if ( hash.find(t->next->value) != hash.end())
				t->next = t->next->next;
			else
				t = t->next;
		}

		t = head;
		while (t != NULL)
        {
			cout << t->value << "\t";
            t = t->next;
        }
		cout << endl << endl;
	}
    return 0;
}

int main()
{
    ifstream fin("2-1.in");
    int n;
    while (fin >> n)
    {
        ListNode* head;
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
            continue;
        }

        head = new ListNode;
        fin >> head->value;
        head->next = NULL;
        ListNode* t = head;
        for (int i = 1; i < n; i++)
        {
            t->next = new ListNode;
            t = t->next;
            fin >> t->value;
            t->next = NULL;
        }

        t = head;
        while (t != NULL)
                {
            cout << t->value << "\t";
                    t = t->next;
                }
        cout << endl;

        ListNode* done = head;

        while (done != NULL && done->next != NULL)
        {
            ListNode* t = head;
            while (t != done->next && done->next != NULL)
            {
                if (done->next->value == t->value)
                {
                    done->next = done->next->next;
                    t = head;
                }
                else
                    t = t->next;
            }
            done = done->next;
        }
        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl << endl;

    }
    return 0;
}
