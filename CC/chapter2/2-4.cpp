#include <iostream>
#include <fstream>
#include <set>
using namespace std;

struct ListNode
{
	int value;
	ListNode* next;
};

int main0()
{
    ifstream fin("2-4.in");
    int n;
    ListNode *h1, *h2, *t;

    while (fin >> n)
    {
        if (n == 0)
        {
            h1 = NULL;
            cout << "NULL List \n";
        }
        else
        {
            h1 = new ListNode;
            fin >> h1->value;
            h1->next = NULL;
            ListNode* t = h1;
            for (int i = 1; i < n; i++)
            {
                t->next = new ListNode;
                t = t->next;
                fin >> t->value;
                t->next = NULL;
            }
        }

        fin >> n;
        if (n == 0)
        {
            h2 = NULL;
            cout << "NULL List \n";
        }
        else
        {
            h2 = new ListNode;
            fin >> h2->value;
            h2->next = NULL;
            ListNode* t = h2;
            for (int i = 1; i < n; i++)
            {
                t->next = new ListNode;
                t = t->next;
                fin >> t->value;
                t->next = NULL;
            }
        }
        t = h1;
        cout << "List 1 \t\t";
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        t = h2;
        cout << "List 2 \t\t";
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        int carry = 0;
        ListNode* result = NULL, *tr = NULL;
        ListNode* t1 = h1, *t2 = h2;
        int v1, v2;
        while (! (t1 == NULL && t2 == NULL))
        {
            if (t1 != NULL)
            {
                v1 = t1->value;
                t1 = t1->next;
            }
            else
                v1 = 0;
            if (t2 != NULL)
            {
                v2 = t2->value;
                t2 = t2->next;
            }
            else
                v2 = 0;

            if (tr == NULL)
            {
                result = new ListNode;
                tr = result;
            }
            else
            {
                tr->next = new ListNode;
                tr = tr->next;
            }

            tr->value = (v1 + v2 + carry) % 10;
            carry = (v1 + v2 + carry) / 10;
            tr->next = NULL;
        }
        if (result == NULL)
        {
            result = new ListNode;
            result->value = 0;
            result->next = NULL;
        }
        if (carry != 0)
        {
            result->next = new ListNode;
            result->value = carry;
            result->next = NULL;
        }

        cout << "Result\t\t";
        t = result;
        while (t != NULL)
        {
            cout << t->value << '\t';
            t = t->next;
        }
        cout << endl;
    }

    return 0;
}

ListNode* addList(ListNode* h1, ListNode* h2, int carry)
{
    if (h1 == NULL && h2 == NULL)
        return NULL;
    int v1 = 0, v2 = 0;
    ListNode* next1 = NULL, *next2 = NULL;
    if (h1 != NULL)
    {
        v1 = h1->value;
        next1 = h1->next;
    }
    if (h2 != NULL)
    {
        v2 = h2->value;
        next2 = h2->next;
    }

    ListNode *result = new ListNode;
    result->value = (v1+v2+carry) % 10;
    carry = (v1+v2+carry) / 10;
    result->next = addList(next1, next2, carry);

    return result;
}

int main()
{
    ifstream fin("2-4.in");
    int n;
    ListNode *h1, *h2, *t;

    while (fin >> n)
    {
        if (n == 0)
        {
            h1 = NULL;
            cout << "NULL List \n";
        }
        else
        {
            h1 = new ListNode;
            fin >> h1->value;
            h1->next = NULL;
            ListNode* t = h1;
            for (int i = 1; i < n; i++)
            {
                t->next = new ListNode;
                t = t->next;
                fin >> t->value;
                t->next = NULL;
            }
        }

        fin >> n;
        if (n == 0)
        {
            h2 = NULL;
            cout << "NULL List \n";
        }
        else
        {
            h2 = new ListNode;
            fin >> h2->value;
            h2->next = NULL;
            ListNode* t = h2;
            for (int i = 1; i < n; i++)
            {
                t->next = new ListNode;
                t = t->next;
                fin >> t->value;
                t->next = NULL;
            }
        }
        t = h1;
        cout << "List 1 \t\t";
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        t = h2;
        cout << "List 2 \t\t";
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        ListNode* result = addList(h1, h2, 0);

        cout << "Result\t\t";
        t = result;
        while (t != NULL)
        {
            cout << t->value << '\t';
            t = t->next;
        }
        cout << endl;
    }

    return 0;
}

