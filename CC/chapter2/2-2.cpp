#include <iostream>
#include <fstream>
#include <set>
using namespace std;

struct ListNode
{
	int value;
	ListNode* next;
};

//Find the nth least number
int main0()
{
    ifstream fin("2-2.in");
    int n;
    while (fin >> n)
    {
        ListNode* head;
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
            continue;
        }

        head = new ListNode;
        fin >> head->value;
        head->next = NULL;
        ListNode* t = head;
        for (int i = 1; i < n; i++)
        {
            t->next = new ListNode;
            t = t->next;
            fin >> t->value;
            t->next = NULL;
        }

        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        fin >> n;
        cout << n << endl;
        int max = 999999;

        for (int i = 0; i < n; i++)
        {
            int ans = -999999;
            t = head;
            while (t != NULL)
            {
                if (t->value < max && t->value > ans)
                    ans = t->value;
                t = t->next;
            }
            max = ans;
        }
        cout << max << endl << endl;

    }
    return 0;
}

bool flag = true;
int depth(ListNode *t, int n)
{
    if (t == NULL)
        return 0;

    if (1 + depth(t->next, n) == n && flag)
    {
        cout << t->value << endl << endl;
        flag = false;
    }
    return (1 + depth(t->next, n));
}

int main1()
{
    ifstream fin("2-2.in");
    int n;
    while (fin >> n)
    {
        ListNode* head;
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
            continue;
        }

        head = new ListNode;
        fin >> head->value;
        head->next = NULL;
        ListNode* t = head;
        for (int i = 1; i < n; i++)
        {
            t->next = new ListNode;
            t = t->next;
            fin >> t->value;
            t->next = NULL;
        }

        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        fin >> n;
        cout << n << endl;
        flag = true;
        depth(head, n);
    }
    return 0;
}

int main()
{
    ifstream fin("2-2.in");
    int n;
    while (fin >> n)
    {
        ListNode* head;
        if (n == 0)
        {
            head = NULL;
            cout << "NULL List \n";
            continue;
        }

        head = new ListNode;
        fin >> head->value;
        head->next = NULL;
        ListNode* t = head;
        for (int i = 1; i < n; i++)
        {
            t->next = new ListNode;
            t = t->next;
            fin >> t->value;
            t->next = NULL;
        }

        t = head;
        while (t != NULL)
        {
            cout << t->value << "\t";
            t = t->next;
        }
        cout << endl ;

        fin >> n;
        cout << n << endl;
        ListNode *t1 = head, *t2 = head;

        for (int i = 0; i < n; i++)
        {
            if (t2 == NULL)
            {
                cout << "NULL!\n\n";
                continue;
            }
            t2 = t2->next;
        }
        while (t2 != NULL)
        {
            t1 = t1->next;
            t2 = t2->next;
        }
        cout << t1->value << endl << endl;
    }
    return 0;
}
